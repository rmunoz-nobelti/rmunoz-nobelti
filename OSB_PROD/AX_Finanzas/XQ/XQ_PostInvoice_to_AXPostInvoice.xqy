xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://tempuri.org";
(:: import schema at "../XSD/XMLSchema_2036091149.xsd" ::)

declare namespace dyn = "http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application";

declare variable $GTDCust as element() (:: schema-element(ns1:GTDCustInvoicePostServicePostInvoiceRequest) ::) external;

declare function local:func($GTDCust as element() (:: schema-element(ns1:GTDCustInvoicePostServicePostInvoiceRequest) ::)) as element() (:: schema-element(ns1:GTDCustInvoicePostServicePostInvoiceRequest) ::) {
    <ns1:GTDCustInvoicePostServicePostInvoiceRequest>
        {
            if ($GTDCust/ns1:_header)
            then 
                <ns1:_header>
                    {
                        if ($GTDCust/ns1:_header/dyn:Attention)
                        then <dyn:Attention>{fn:data($GTDCust/ns1:_header/dyn:Attention)}</dyn:Attention>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:BranchId)
                        then <dyn:BranchId>{fn:data($GTDCust/ns1:_header/dyn:BranchId)}</dyn:BranchId>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:CodCallejero)
                        then <dyn:CodCallejero>{fn:data($GTDCust/ns1:_header/dyn:CodCallejero)}</dyn:CodCallejero>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:CurrencyCode)
                        then <dyn:CurrencyCode>{fn:data($GTDCust/ns1:_header/dyn:CurrencyCode)}</dyn:CurrencyCode>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:CustAccount)
                        then <dyn:CustAccount>{fn:data($GTDCust/ns1:_header/dyn:CustAccount)}</dyn:CustAccount>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:DataArea)
                        then <dyn:DataArea>{fn:data($GTDCust/ns1:_header/dyn:DataArea)}</dyn:DataArea>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:DefaultDim_Zona)
                        then <dyn:DefaultDim_Zona>{fn:data($GTDCust/ns1:_header/dyn:DefaultDim_Zona)}</dyn:DefaultDim_Zona>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:InvoiceDate)
                        then <dyn:DocumentDate>{fn:data($GTDCust/ns1:_header/dyn:InvoiceDate)}</dyn:DocumentDate>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:Downpayment)
                        then <dyn:Downpayment>{fn:data($GTDCust/ns1:_header/dyn:Downpayment)}</dyn:Downpayment>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:DteTemplateId)
                        then <dyn:DteTemplateId>{fn:data($GTDCust/ns1:_header/dyn:DteTemplateId)}</dyn:DteTemplateId>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:DteVersionId)
                        then <dyn:DteVersionId>{fn:data($GTDCust/ns1:_header/dyn:DteVersionId)}</dyn:DteVersionId>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:DueDate)
                        then <dyn:DueDate>{fn:data($GTDCust/ns1:_header/dyn:DueDate)}</dyn:DueDate>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:EndingPeriod)
                        then <dyn:EndingPeriod>{fn:data($GTDCust/ns1:_header/dyn:EndingPeriod)}</dyn:EndingPeriod>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:GL)
                        then <dyn:GL>{fn:data($GTDCust/ns1:_header/dyn:GL)}</dyn:GL>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:GUF)
                        then <dyn:GUF>{fn:data($GTDCust/ns1:_header/dyn:GUF)}</dyn:GUF>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:GULT)
                        then <dyn:GULT>{fn:data($GTDCust/ns1:_header/dyn:GULT)}</dyn:GULT>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:InvoiceAccount)
                        then <dyn:InvoiceAccount>{fn:data($GTDCust/ns1:_header/dyn:InvoiceAccount)}</dyn:InvoiceAccount>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:InvoiceDate)
                        then <dyn:InvoiceDate>{fn:data($GTDCust/ns1:_header/dyn:InvoiceDate)}</dyn:InvoiceDate>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:InvoiceId)
                        then <dyn:InvoiceId>{fn:data($GTDCust/ns1:_header/dyn:InvoiceId)}</dyn:InvoiceId>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:IssuePoint)
                        then <dyn:IssuePoint>{fn:data($GTDCust/ns1:_header/dyn:IssuePoint)}</dyn:IssuePoint>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:Lines)
                        then 
                            <dyn:Lines>
                                {
                                    for $GTDCustInvoiceLineContract in $GTDCust/ns1:_header/dyn:Lines/dyn:GTDCustInvoiceLineContract
                                    return 
                                    <dyn:GTDCustInvoiceLineContract>
                                        {
                                            if ($GTDCustInvoiceLineContract/dyn:BillingCode)
                                            then <dyn:BillingCode>{fn:data($GTDCustInvoiceLineContract/dyn:BillingCode)}</dyn:BillingCode>
                                            else ()
                                        }
                                        {
                                            if ($GTDCustInvoiceLineContract/dyn:ClaimNumber)
                                            then <dyn:ClaimNumber>{fn:data($GTDCustInvoiceLineContract/dyn:ClaimNumber)}</dyn:ClaimNumber>
                                            else ()
                                        }
                                        {
                                            if ($GTDCustInvoiceLineContract/dyn:DefaultDim_Zona)
                                            then <dyn:DefaultDim_Zona>{fn:data($GTDCustInvoiceLineContract/dyn:DefaultDim_Zona)}</dyn:DefaultDim_Zona>
                                            else ()
                                        }
                                        {
                                            if ($GTDCustInvoiceLineContract/dyn:LineAmount)
                                            then <dyn:LineAmount>{fn:data($GTDCustInvoiceLineContract/dyn:LineAmount)}</dyn:LineAmount>
                                            else ()
                                        }
                                        {
                                            if ($GTDCustInvoiceLineContract/dyn:NumOfPayment)
                                            then <dyn:NumOfPayment>{fn:data($GTDCustInvoiceLineContract/dyn:NumOfPayment)}</dyn:NumOfPayment>
                                            else ()
                                        }
                                        {
                                            if ($GTDCustInvoiceLineContract/dyn:Qty)
                                            then <dyn:Qty>{fn:data($GTDCustInvoiceLineContract/dyn:Qty)}</dyn:Qty>
                                            else ()
                                        }
                                        {
                                            if ($GTDCustInvoiceLineContract/dyn:RefVoucherBSS)
                                            then <dyn:RefVoucherBSS>{fn:data($GTDCustInvoiceLineContract/dyn:RefVoucherBSS)}</dyn:RefVoucherBSS>
                                            else ()
                                        }
                                        {
                                            if ($GTDCustInvoiceLineContract/dyn:RenegotiationNum)
                                            then <dyn:RenegotiationNum>{fn:data($GTDCustInvoiceLineContract/dyn:RenegotiationNum)}</dyn:RenegotiationNum>
                                            else ()
                                        }
                                        {
                                            if ($GTDCustInvoiceLineContract/dyn:SubGroupDescription)
                                            then <dyn:SubGroupDescription>{fn:data($GTDCustInvoiceLineContract/dyn:SubGroupDescription)}</dyn:SubGroupDescription>
                                            else ()
                                        }
                                        {
                                            if ($GTDCustInvoiceLineContract/dyn:SubGroupId)
                                            then <dyn:SubGroupId>{fn:data($GTDCustInvoiceLineContract/dyn:SubGroupId)}</dyn:SubGroupId>
                                            else ()
                                        }
                                        {
                                            if ($GTDCustInvoiceLineContract/dyn:UnitPrice)
                                            then <dyn:UnitPrice>{fn:data($GTDCustInvoiceLineContract/dyn:UnitPrice)}</dyn:UnitPrice>
                                            else ()
                                        }
                                    </dyn:GTDCustInvoiceLineContract>
                                }
                            </dyn:Lines>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:PaymSched)
                        then <dyn:PaymSched>{fn:data($GTDCust/ns1:_header/dyn:PaymSched)}</dyn:PaymSched>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:Payment)
                        then <dyn:Payment>{fn:data($GTDCust/ns1:_header/dyn:Payment)}</dyn:Payment>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:PreviousBalance)
                        then <dyn:PreviousBalance>{fn:data($GTDCust/ns1:_header/dyn:PreviousBalance)}</dyn:PreviousBalance>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:ReasonCode)
                        then <dyn:ReasonCode>{fn:data($GTDCust/ns1:_header/dyn:ReasonCode)}</dyn:ReasonCode>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:ReasonText)
                        then <dyn:ReasonText>{fn:data($GTDCust/ns1:_header/dyn:ReasonText)}</dyn:ReasonText>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:References)
                        then 
                            <dyn:References>
                                {
                                    for $GTDCustInvoiceReferenceContract in $GTDCust/ns1:_header/dyn:References/dyn:GTDCustInvoiceReferenceContract
                                    return 
                                    <dyn:GTDCustInvoiceReferenceContract>
                                        {
                                            if ($GTDCustInvoiceReferenceContract/dyn:DocDateRef)
                                            then <dyn:DocDateRef>{fn:data($GTDCustInvoiceReferenceContract/dyn:DocDateRef)}</dyn:DocDateRef>
                                            else ()
                                        }
                                        {
                                            if ($GTDCustInvoiceReferenceContract/dyn:IdDocRef)
                                            then <dyn:IdDocRef>{fn:data($GTDCustInvoiceReferenceContract/dyn:IdDocRef)}</dyn:IdDocRef>
                                            else ()
                                        }
                                        {
                                            if ($GTDCustInvoiceReferenceContract/dyn:IdPV)
                                            then <dyn:IdPV>{fn:data($GTDCustInvoiceReferenceContract/dyn:IdPV)}</dyn:IdPV>
                                            else ()
                                        }
                                        {
                                            if ($GTDCustInvoiceReferenceContract/dyn:IdTdocRef)
                                            then <dyn:IdTdocRef>{fn:data($GTDCustInvoiceReferenceContract/dyn:IdTdocRef)}</dyn:IdTdocRef>
                                            else ()
                                        }
                                        {
                                            if ($GTDCustInvoiceReferenceContract/dyn:ObsRef)
                                            then <dyn:ObsRef>{fn:data($GTDCustInvoiceReferenceContract/dyn:ObsRef)}</dyn:ObsRef>
                                            else ()
                                        }
                                    </dyn:GTDCustInvoiceReferenceContract>
                                }
                            </dyn:References>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:SettleBalance)
                        then <dyn:SettleBalance>{fn:data($GTDCust/ns1:_header/dyn:SettleBalance)}</dyn:SettleBalance>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:StartingPeriod)
                        then <dyn:StartingPeriod>{fn:data($GTDCust/ns1:_header/dyn:StartingPeriod)}</dyn:StartingPeriod>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:TaxAmount)
                        then <dyn:TaxAmount>{fn:data($GTDCust/ns1:_header/dyn:TaxAmount)}</dyn:TaxAmount>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:TemplateName)
                        then <dyn:TemplateName>{fn:data($GTDCust/ns1:_header/dyn:TemplateName)}</dyn:TemplateName>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:TradeDocumentId)
                        then <dyn:TradeDocumentId>{fn:data($GTDCust/ns1:_header/dyn:TradeDocumentId)}</dyn:TradeDocumentId>
                        else ()
                    }
                    {
                        if ($GTDCust/ns1:_header/dyn:VoucherBSS)
                        then <dyn:VoucherBSS>{fn:data($GTDCust/ns1:_header/dyn:VoucherBSS)}</dyn:VoucherBSS>
                        else ()
                    }
                </ns1:_header>
            else ()
        }
    </ns1:GTDCustInvoicePostServicePostInvoiceRequest>
};

local:func($GTDCust)