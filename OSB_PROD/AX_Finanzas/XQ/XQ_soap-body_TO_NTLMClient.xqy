xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/utility/NTLM/client";
declare namespace tem="http://tempuri.org";

(:: import schema at "../../UTL_NTLMClient/ProxyServices/WSDL/NTLMClientService.wsdl" ::)

declare variable $input as node() external;

declare function local:soapToNTLMClient($input as node()) as element() (:: schema-element(ns1:reqNTLM) ::) {
    <ns1:reqNTLM> 
        <ns1:wsURL>{fn:string('http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDCustIntegrationServices/xppservice.svc')}</ns1:wsURL>
        <ns1:host>{fn:string('aosaifsrv01')}</ns1:host>
        <ns1:user>{fn:string('svsbus')}</ns1:user>
        <ns1:password>cachito</ns1:password>
        <ns1:soapAction>{fn:string('http://tempuri.org/GTDCustInvoicePostService/postInvoice')}</ns1:soapAction>
        <ns1:xmlInput>
<soapenv:Envelope 	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts">
<soapenv:Header>
      <dat:CallContext>
         <dat:Company>INT</dat:Company>
      </dat:CallContext>
   </soapenv:Header>          
<soapenv:Body>{$input/tem:GTDCustInvoicePostServicePostInvoiceRequest}</soapenv:Body>
</soapenv:Envelope>          
        </ns1:xmlInput>
    </ns1:reqNTLM>
};

local:soapToNTLMClient($input)