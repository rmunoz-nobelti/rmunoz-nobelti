xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/wsdl/scm/GestorStockEquipo";
(:: import schema at "../WSDL/GestorStockEquipo.wsdl" ::)

declare namespace agr = "http://cl.grupogtd.com/schema/GestorStockEquipo/agregarStockEquipo";

declare variable $reason as xs:string external;

declare function local:func($reason as xs:string) as element() (:: schema-element(ns1:agregarStockEquipoRsp) ::) {

    if (fn:contains($reason,"rutProveedor")) then
        <ns1:agregarStockEquipoRsp>
            <agr:errorCodigo>V001</agr:errorCodigo>
            <agr:errorTipo>Validación</agr:errorTipo>
            <agr:errorDescripcion>Rut debe tener dígito verificador y guión</agr:errorDescripcion>
        </ns1:agregarStockEquipoRsp>
     else if (fn:contains($reason,"idOrdenCompra")) then
        <ns1:agregarStockEquipoRsp>
            <agr:errorCodigo>V002</agr:errorCodigo>
            <agr:errorTipo>Validación</agr:errorTipo>
            <agr:errorDescripcion>Largo del número de Orden de Compra</agr:errorDescripcion>
        </ns1:agregarStockEquipoRsp>
    else if (fn:contains($reason,"T001")) then
        <ns1:agregarStockEquipoRsp>
            <agr:errorCodigo>T001</agr:errorCodigo>
            <agr:errorTipo>Técnico</agr:errorTipo>
            <agr:errorDescripcion>404 backend no disponible</agr:errorDescripcion>
        </ns1:agregarStockEquipoRsp>
    else if (fn:contains($reason,".validacion")) then
        <ns1:agregarStockEquipoRsp>
            <agr:errorCodigo>V003</agr:errorCodigo>
            <agr:errorTipo>Validación</agr:errorTipo>
            <agr:errorDescripcion>Request no valido</agr:errorDescripcion>
        </ns1:agregarStockEquipoRsp>
    else()
};

local:func($reason)