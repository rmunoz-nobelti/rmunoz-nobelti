xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.grupogtd.com/wsdl/scm/GestorStockEquipo";
(:: import schema at "../WSDL/GestorStockEquipo.wsdl" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/SP_PPUT_RECEPCION_EQUIPO";
(:: import schema at "../../PS00002_AgregarStockEquipoLoc/JCA/Schemas/SP_PPUT_RECEPCION_EQUIPO_sp.xsd" ::)

declare namespace agr = "http://cl.grupogtd.com/schema/GestorStockEquipo/agregarStockEquipo";

declare variable $OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::) external;

declare function local:func($OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::)) as element() (:: schema-element(ns2:agregarStockEquipoRsp) ::) {
    <ns2:agregarStockEquipoRsp>
        <agr:errorCodigo>{fn:data($OutputParameters/ns1:CODIGO_ERROR_OUT)}</agr:errorCodigo>
        <agr:errorTipo></agr:errorTipo>
         {
         if (fn:data($OutputParameters/ns1:GLS_ERR_OUT)) then
            <agr:errorDescripcion>{fn:data($OutputParameters/ns1:GLS_ERR_OUT)}</agr:errorDescripcion>
         else
             <agr:errorDescripcion>ÉXITO</agr:errorDescripcion>
         }
        
    </ns2:agregarStockEquipoRsp>
};

local:func($OutputParameters)