xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/wsdl/scm/GestorStockEquipo";
(:: import schema at "../WSDL/GestorStockEquipo.wsdl" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/SP_PPUT_RECEPCION_EQUIPO";
(:: import schema at "../../PS00002_AgregarStockEquipoLoc/JCA/Schemas/SP_PPUT_RECEPCION_EQUIPO_sp.xsd" ::)

declare namespace agr = "http://cl.grupogtd.com/schema/GestorStockEquipo/agregarStockEquipo";

declare variable $agregarStockEquipoReq as element() (:: schema-element(ns1:agregarStockEquipoReq) ::) external;

declare function local:func($agregarStockEquipoReq as element() (:: schema-element(ns1:agregarStockEquipoReq) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:VCODSKU>{fn:data($agregarStockEquipoReq/agr:codigoSKU)}</ns2:VCODSKU>
        <ns2:VNOMBRE>{fn:data($agregarStockEquipoReq/agr:nombreArticulo)}</ns2:VNOMBRE>
        <ns2:VCANTIDAD>{fn:data($agregarStockEquipoReq/agr:cantidadSKU)}</ns2:VCANTIDAD>
        <ns2:VDATERECEP>{fn:data($agregarStockEquipoReq/agr:fechaRecepcion)}</ns2:VDATERECEP>
        <ns2:VIDOC>{fn:data($agregarStockEquipoReq/agr:idOrdenCompra)}</ns2:VIDOC>
        <ns2:VRUTPROVEEDOR>{fn:data($agregarStockEquipoReq/agr:rutProveedor)}</ns2:VRUTPROVEEDOR>
        <ns2:VALMACEN>{fn:data($agregarStockEquipoReq/agr:codigoBodega)}</ns2:VALMACEN>       
    </ns2:InputParameters>
};

local:func($agregarStockEquipoReq)