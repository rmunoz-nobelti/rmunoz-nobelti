xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/Repactacion/establecerCuotasRepactacion";
(:: import schema at "../XSD/establecerCuotasRepactacion.xsd" ::)

declare variable $codigoError as xs:string external;
declare variable $tipoError as xs:string external;
declare variable $descripcion as xs:string external;

declare function local:XQ_EstablecerCuotasRepactacionExpValidacion_TO_EstablecerCuotasRepactacionExp($codigoError as xs:string,
                                                                                                     $tipoError as xs:string,
                                                                                                     $descripcion as xs:string) 
                                                                                                     as element() (:: schema-element(ns1:establecerCuotasRepactacionRsp) ::) {
    <ns1:establecerCuotasRepactacionRsp>
        <ns1:errorCodigo>{fn:data($codigoError)}</ns1:errorCodigo>
        <ns1:errorTipo>{fn:data($tipoError)}</ns1:errorTipo>
        <ns1:errorDescripcion>{fn:data($descripcion)}</ns1:errorDescripcion>
    </ns1:establecerCuotasRepactacionRsp>
};

local:XQ_EstablecerCuotasRepactacionExpValidacion_TO_EstablecerCuotasRepactacionExp($codigoError, $tipoError, $descripcion)