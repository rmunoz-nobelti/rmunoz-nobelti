xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/Repactacion/establecerCuotasRepactacion";
(:: import schema at "../XSD/establecerCuotasRepactacion.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/SP_AX_PPUT_REPAC_CUOTAS";
(:: import schema at "../../PS00011_EstablecerCuotasRepactacionLoc/JCA/Schemas/SP_AX_PPUT_REPAC_CUOTAS_sp.xsd" ::)

declare variable $inputMessage as element() (:: schema-element(ns1:establecerCuotasRepactacionReq) ::) external;


declare function local:XQ_EstablecerCuotasRepactacionExp_TO_EstablecerCuotasRepactacionLoc($inputMessage as element() (:: schema-element(ns1:establecerCuotasRepactacionReq) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:V_ACCOUNT_NO>{fn:data($inputMessage/ns1:nroCuenta)}</ns2:V_ACCOUNT_NO>
        <ns2:V_SALES_CHANNEL_ID>1</ns2:V_SALES_CHANNEL_ID>
        <ns2:V_TYPE_ID_NRC>5812</ns2:V_TYPE_ID_NRC>
        <ns2:V_RATE>{fn:data($inputMessage/ns1:valorCuota)}</ns2:V_RATE>
        <ns2:V_EFF_DATE>{fn:data($inputMessage/ns1:fechaVencimiento)}</ns2:V_EFF_DATE>
        <ns2:V_CHG_WHO>AX</ns2:V_CHG_WHO>
        <ns2:V_TOTAL_INSTALLMENTS>{fn:data($inputMessage/ns1:cantCuotas)}</ns2:V_TOTAL_INSTALLMENTS>
        <ns2:V_CURRENT_INSTALLMENT>{fn:data($inputMessage/ns1:nroCuota)}</ns2:V_CURRENT_INSTALLMENT>
        <ns2:V_CUST_ORDER_NUMBER>{fn:data($inputMessage/ns1:idRepactacion)}</ns2:V_CUST_ORDER_NUMBER>
    </ns2:InputParameters>
};

local:XQ_EstablecerCuotasRepactacionExp_TO_EstablecerCuotasRepactacionLoc($inputMessage)