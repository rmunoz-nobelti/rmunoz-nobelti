xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.grupogtd.com/schema/Repactacion/establecerCuotasRepactacion";
(:: import schema at "../XSD/establecerCuotasRepactacion.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/SP_AX_PPUT_REPAC_CUOTAS";
(:: import schema at "../../PS00011_EstablecerCuotasRepactacionLoc/JCA/Schemas/SP_AX_PPUT_REPAC_CUOTAS_sp.xsd" ::)

declare variable $outputMessage as element() (:: schema-element(ns1:OutputParameters) ::) external;

declare function local:XQ_EstablecerCuotasRepactacionLoc_TO_EstablecerCuotasRepactacionExp($outputMessage as element() (:: schema-element(ns1:OutputParameters) ::)) as element() (:: schema-element(ns2:establecerCuotasRepactacionRsp) ::) {
    <ns2:establecerCuotasRepactacionRsp>
        <ns2:errorCodigo>{fn:data($outputMessage/ns1:CODIGO_ERROR_OUT)}</ns2:errorCodigo>
        <ns2:errorDescripcion>{fn:data($outputMessage/ns1:GLS_ERR_OUT)}</ns2:errorDescripcion>
    </ns2:establecerCuotasRepactacionRsp>
};

local:XQ_EstablecerCuotasRepactacionLoc_TO_EstablecerCuotasRepactacionExp($outputMessage)