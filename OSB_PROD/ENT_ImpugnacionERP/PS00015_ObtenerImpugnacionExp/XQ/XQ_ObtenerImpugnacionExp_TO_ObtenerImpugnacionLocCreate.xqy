xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ImpugnacionERP/obtenerImpugnacion";
(:: import schema at "../XSD/obtenerImpugnacion.xsd" ::)
declare namespace ns2="http://tempuri.org";
(:: import schema at "../../PS00015_ObtenerImpugnacionLoc/WS/Schemas/XMLSchema_854349591.xsd" ::)

declare namespace acc = "http://schemas.microsoft.com/dynamics/2008/01/documents/ACCImpugment";

declare variable $input as element() (:: schema-element(ns1:obtenerImpugnacionReq) ::) external;

declare function local:XQ_ObtenerImpugnacionExp_TO_ObtenerImpugnacionLocCreate($input as element() (:: schema-element(ns1:obtenerImpugnacionReq) ::)) as element() (:: schema-element(ns2:ACCImpugmentServiceCreateRequest) ::) {
    <ns2:ACCImpugmentServiceCreateRequest>
        <acc:ACCImpugment>
            <acc:ACCImpugmentTable_1 class="">
                <acc:_DocumentHash></acc:_DocumentHash>
                <acc:ACCClaimId>{fn:data($input/ns1:DATOS_RECLAMO/ns1:idReclamo)}</acc:ACCClaimId>
                <acc:CompanyId>{fn:data($input/ns1:DATOS_RECLAMO/ns1:empresa)}</acc:CompanyId>
                <acc:CustAccount>{fn:data($input/ns1:DATOS_RECLAMO/ns1:rutCliente)}</acc:CustAccount>
                {
                    for $DET_RECLAMO in $input/ns1:DATOS_RECLAMO/ns1:DETALLE_RECLAMO
                    return 
                    <acc:ACCImpugmentLine_1>
                        <acc:ACCBillingCode>{fn:data($DET_RECLAMO/ns1:billingCode)}</acc:ACCBillingCode>
                        <acc:ACCClaimId>{fn:data($input/ns1:DATOS_RECLAMO/ns1:idReclamo)}</acc:ACCClaimId>
                        {
                          if (fn:data($DET_RECLAMO/ns1:estado)= 'APROBADO')
                            then <acc:ACCImpugmentstate>Approve</acc:ACCImpugmentstate>
                          else if (fn:data($DET_RECLAMO/ns1:estado)= 'RECHAZADO')
                            then <acc:ACCImpugmentstate>Refused</acc:ACCImpugmentstate>
                            else()
                        }
                        <acc:ACCVoucherBSS>{fn:data($DET_RECLAMO/ns1:voucherBSS)}</acc:ACCVoucherBSS>
                        <acc:CompanyId>{fn:data($input/ns1:DATOS_RECLAMO/ns1:empresa)}</acc:CompanyId>
                        <acc:CustAccount>{fn:data($input/ns1:DATOS_RECLAMO/ns1:rutCliente)}</acc:CustAccount>
                        <acc:DeniedValue>{fn:data($DET_RECLAMO/ns1:montoRechazado)}</acc:DeniedValue>
                        <acc:Description>{fn:data($DET_RECLAMO/ns1:DATOS_PRODUCTO/ns1:descripcion)}</acc:Description>
                        {
                          if (fn:data($DET_RECLAMO/ns1:codProveedor) =6)
                          then <acc:SubGroup>{fn:data($DET_RECLAMO/ns1:DATOS_PRODUCTO/ns1:axSubGrupo)}</acc:SubGroup>
                          else (<acc:SubGroup>{fn:data($DET_RECLAMO/ns1:codProveedor)}</acc:SubGroup>)
                        }
                        <acc:Value>{fn:data($DET_RECLAMO/ns1:valorImpugnado)}</acc:Value>
                    </acc:ACCImpugmentLine_1>
                }
            </acc:ACCImpugmentTable_1>
        </acc:ACCImpugment>
    </ns2:ACCImpugmentServiceCreateRequest>
};

local:XQ_ObtenerImpugnacionExp_TO_ObtenerImpugnacionLocCreate($input)