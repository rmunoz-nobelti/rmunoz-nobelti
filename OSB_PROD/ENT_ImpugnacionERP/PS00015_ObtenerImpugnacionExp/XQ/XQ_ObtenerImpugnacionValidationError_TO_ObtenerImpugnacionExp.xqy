xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ImpugnacionERP/obtenerImpugnacion";
(:: import schema at "../XSD/obtenerImpugnacion.xsd" ::)


declare function local:XQ_ObtenerImpugnacionValidationError_TO_ObtenerImpugnacionExp() as element() (:: schema-element(ns1:obtenerImpugnacionRsp) ::) {
    <ns1:obtenerImpugnacionRsp>
        <ns1:errorCodigo>V001</ns1:errorCodigo>
        <ns1:errorTipo>Validación</ns1:errorTipo>
        <ns1:errorDescripcion>Error en los datos de entrada</ns1:errorDescripcion>    
    </ns1:obtenerImpugnacionRsp>
};

local:XQ_ObtenerImpugnacionValidationError_TO_ObtenerImpugnacionExp()