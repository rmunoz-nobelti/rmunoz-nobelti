xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ImpugnacionERP/obtenerImpugnacion";
(:: import schema at "../XSD/obtenerImpugnacion.xsd" ::)
declare namespace ns2="http://tempuri.org";
(:: import schema at "../../PS00015_ObtenerImpugnacionLoc/WS/Schemas/XMLSchema_854349591.xsd" ::)

declare namespace ent1 = "http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKey";

declare namespace ent = "http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKeyList";

declare namespace acc = "http://schemas.microsoft.com/dynamics/2008/01/documents/ACCImpLineUpdate";

declare variable $input as element() (:: schema-element(ns1:obtenerImpugnacionReq) ::) external;
declare variable $inputFind as element() (:: schema-element(ns2:ACCImpLineUpdateServiceFindResponse) ::) external;


declare function local:XQ_ObtenerImpugnacionExp_TO_ObtenerImpugnacionLocUpdate($input as element() (:: schema-element(ns1:obtenerImpugnacionReq) ::), 
                                                                               $inputFind as element() (:: schema-element(ns2:ACCImpLineUpdateServiceFindResponse) ::)) 
                                                                               as element() (:: schema-element(ns2:ACCImpLineUpdateServiceUpdateRequest) ::) {
    <ns2:ACCImpLineUpdateServiceUpdateRequest>
        <ent:EntityKeyList>
            {
                for $ACCDetalle in $inputFind/acc:ACCImpLineUpdate/acc:ACCImpugmentLine_1
                return 
                <ent1:EntityKey>
                    <ent1:KeyData>
                        <ent1:KeyField>
                            <ent1:Field>RecId</ent1:Field>
                            <ent1:Value>{fn:data($ACCDetalle/acc:RecId)}</ent1:Value>
                        </ent1:KeyField>
                    </ent1:KeyData>
                </ent1:EntityKey>
            }
        </ent:EntityKeyList>
        <acc:ACCImpLineUpdate>
            {
              for $DET_RECLAMO at $count in $input/ns1:DATOS_RECLAMO/ns1:DETALLE_RECLAMO
              return
                <acc:ACCImpugmentLine_1>
                    <acc:_DocumentHash>{fn:data($inputFind/acc:ACCImpLineUpdate/acc:ACCImpugmentLine_1[$count]/acc:_DocumentHash)}</acc:_DocumentHash>
                    
                     <acc:RecId>{
                     for $billing at $c in $inputFind/acc:ACCImpLineUpdate/acc:ACCImpugmentLine_1
                     return
                     fn:data($billing/acc:RecId[data($DET_RECLAMO/ns1:billingCode) = data($billing/acc:ACCBillingCode)])
                     }</acc:RecId>
                    <acc:ACCBillingCode>{fn:data($DET_RECLAMO/ns1:billingCode)}</acc:ACCBillingCode>
                    <acc:ACCClaimId>{fn:data($input/ns1:DATOS_RECLAMO/ns1:idReclamo)}</acc:ACCClaimId>
                
                    {
                          if (fn:data($DET_RECLAMO/ns1:estado)= 'APROBADO')
                            then <acc:ACCImpugmentstate>Approve</acc:ACCImpugmentstate>
                          else if (fn:data($DET_RECLAMO/ns1:estado)= 'RECHAZADO')
                            then <acc:ACCImpugmentstate>Refused</acc:ACCImpugmentstate>
                            else()
                    }
                    <acc:ACCVoucherBSS>{fn:data($DET_RECLAMO/ns1:voucherBSS)}</acc:ACCVoucherBSS>
                    <acc:CompanyId>{fn:data($input/ns1:DATOS_RECLAMO/ns1:empresa)}</acc:CompanyId>
                    <acc:CustAccount>{fn:data($input/ns1:DATOS_RECLAMO/ns1:rutCliente)}</acc:CustAccount>
                    <acc:DeniedValue>{fn:data($DET_RECLAMO/ns1:montoRechazado)}</acc:DeniedValue>
                    <acc:Description>{fn:data($DET_RECLAMO/ns1:DATOS_PRODUCTO/ns1:descripcion)}</acc:Description>
                    {
                      if (fn:data($DET_RECLAMO/ns1:codProveedor) =6)
                      then <acc:SubGroup>{fn:data($DET_RECLAMO/ns1:DATOS_PRODUCTO/ns1:axSubGrupo)}</acc:SubGroup>
                      else (<acc:SubGroup>{fn:data($DET_RECLAMO/ns1:codProveedor)}</acc:SubGroup>)
                    }
            
                    <acc:Value>{fn:data($DET_RECLAMO/ns1:valorImpugnado)}</acc:Value>
                </acc:ACCImpugmentLine_1>
            }
        </acc:ACCImpLineUpdate>
    </ns2:ACCImpLineUpdateServiceUpdateRequest>
};

local:XQ_ObtenerImpugnacionExp_TO_ObtenerImpugnacionLocUpdate($input, $inputFind)