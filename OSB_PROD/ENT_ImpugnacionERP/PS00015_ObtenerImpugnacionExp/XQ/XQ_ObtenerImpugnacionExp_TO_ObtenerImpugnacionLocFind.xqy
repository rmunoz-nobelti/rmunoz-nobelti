xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ImpugnacionERP/obtenerImpugnacion";
(:: import schema at "../XSD/obtenerImpugnacion.xsd" ::)
declare namespace ns2="http://tempuri.org";
(:: import schema at "../../PS00015_ObtenerImpugnacionLoc/WS/Schemas/XMLSchema_854349591.xsd" ::)

declare namespace que = "http://schemas.microsoft.com/dynamics/2006/02/documents/QueryCriteria";

declare variable $input as element() (:: schema-element(ns1:obtenerImpugnacionReq) ::) external;

declare function local:XQ_ObtenerImpugnacionExp_TO_ObtenerImpugnacionLocFind($input as element() (:: schema-element(ns1:obtenerImpugnacionReq) ::)) as element() (:: schema-element(ns2:ACCImpLineUpdateServiceFindRequest) ::) {
    <ns2:ACCImpLineUpdateServiceFindRequest>
        <que:QueryCriteria>
            <que:CriteriaElement>
                <que:DataSourceName>ACCImpugmentLine_1</que:DataSourceName>
                <que:FieldName>ACCClaimId</que:FieldName>
                <que:Operator>equal</que:Operator>
                <que:Value1>{fn:data($input/ns1:DATOS_RECLAMO/ns1:idReclamo)}</que:Value1>
                <que:Value2></que:Value2>
            </que:CriteriaElement>
        </que:QueryCriteria>
    </ns2:ACCImpLineUpdateServiceFindRequest>
};

local:XQ_ObtenerImpugnacionExp_TO_ObtenerImpugnacionLocFind($input)