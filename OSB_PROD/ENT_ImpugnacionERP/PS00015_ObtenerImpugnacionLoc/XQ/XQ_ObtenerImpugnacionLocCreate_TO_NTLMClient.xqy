xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace cli="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/XSD/NTLMClient.xsd" ::)
declare namespace ns1="http://tempuri.org";
(:: import schema at "../WS/Schemas/XMLSchema_854349591.xsd" ::)

declare namespace acc = "http://schemas.microsoft.com/dynamics/2008/01/documents/ACCImpugment";

declare variable $input as element() (:: schema-element(ns1:ACCImpugmentServiceCreateRequest) ::) external;

declare function local:XQ_ObtenerImpugnacionLocCreate_TO_NTLMClient($input as element() (:: schema-element(ns1:ACCImpugmentServiceCreateRequest) ::)) as element() (:: schema-element(cli:request) ::) {
    <cli:reqNTLM>
        <cli:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDCustIntegrationServices/xppservice.svc</cli:wsURL>
        <cli:host>aosaifsrv01</cli:host>
        <cli:user>svsbus</cli:user>
        <cli:password>cachito</cli:password>
        <cli:soapAction>http://tempuri.org/ACCImpugmentService/create</cli:soapAction>
        <cli:xmlInput>
        {<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:tem="http://tempuri.org" xmlns:acc="http://schemas.microsoft.com/dynamics/2008/01/documents/ACCImpugment">
           <soapenv:Header>
              <dat:CallContext>
                 <dat:Company>MQH</dat:Company>
              </dat:CallContext>
           </soapenv:Header>
           <soapenv:Body>
              <tem:ACCImpugmentServiceCreateRequest>
                 <acc:ACCImpugment>
                 {
                    for $DATOS_DETALLE in $input/acc:ACCImpugment/acc:ACCImpugmentTable_1
                    return
                      <acc:ACCImpugmentTable_1 class="entity">
                         <acc:_DocumentHash></acc:_DocumentHash>
                         <acc:ACCClaimId>{fn:data($DATOS_DETALLE/acc:ACCClaimId)}</acc:ACCClaimId>
                         <acc:CompanyId>{fn:data($DATOS_DETALLE/acc:CompanyId)}</acc:CompanyId>
                         <acc:CustAccount>{fn:data($DATOS_DETALLE/acc:CustAccount)}</acc:CustAccount>
                         {
                            for $DATOS_DETALLE_ACC in $DATOS_DETALLE/acc:ACCImpugmentLine_1
                            return
                            <acc:ACCImpugmentLine_1 class="entity">
                              <acc:ACCBillingCode>{fn:data($DATOS_DETALLE_ACC/acc:ACCBillingCode)}</acc:ACCBillingCode>
                              <acc:ACCClaimId>{fn:data($DATOS_DETALLE_ACC/acc:ACCClaimId)}</acc:ACCClaimId>
                              <acc:ACCImpugmentstate>{fn:data($DATOS_DETALLE_ACC/acc:ACCImpugmentstate)}</acc:ACCImpugmentstate>
                              <acc:ACCVoucherBSS>{fn:data($DATOS_DETALLE_ACC/acc:ACCVoucherBSS)}</acc:ACCVoucherBSS>
                              <acc:CompanyId>{fn:data($DATOS_DETALLE_ACC/acc:CompanyId)}</acc:CompanyId>
                              <acc:CustAccount>{fn:data($DATOS_DETALLE_ACC/acc:CustAccount)}</acc:CustAccount>
                              <acc:DeniedValue>{fn:data($DATOS_DETALLE_ACC/acc:DeniedValue)}</acc:DeniedValue>
                              <acc:Description>{fn:data($DATOS_DETALLE_ACC/acc:Description)}</acc:Description>
                              <acc:SubGroup>{fn:data($DATOS_DETALLE_ACC/acc:SubGroup)}</acc:SubGroup>
                              <acc:Value>{fn:data($DATOS_DETALLE_ACC/acc:Value)}</acc:Value>
                           </acc:ACCImpugmentLine_1>
                         }
                      </acc:ACCImpugmentTable_1>
                 }
                 </acc:ACCImpugment>
              </tem:ACCImpugmentServiceCreateRequest>
           </soapenv:Body>
        </soapenv:Envelope>}
      </cli:xmlInput>
    </cli:reqNTLM>
};

local:XQ_ObtenerImpugnacionLocCreate_TO_NTLMClient($input)