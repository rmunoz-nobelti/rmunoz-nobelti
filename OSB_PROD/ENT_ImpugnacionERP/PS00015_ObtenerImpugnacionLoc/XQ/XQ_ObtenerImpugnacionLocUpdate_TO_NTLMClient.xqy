xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace cli="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/XSD/NTLMClient.xsd" ::)
declare namespace ns1="http://tempuri.org";
(:: import schema at "../WS/Schemas/XMLSchema_854349591.xsd" ::)

declare namespace ent1 = "http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKey";

declare namespace ent = "http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKeyList";

declare namespace acc = "http://schemas.microsoft.com/dynamics/2008/01/documents/ACCImpLineUpdate";

declare variable $input as element() (:: schema-element(ns1:ACCImpLineUpdateServiceUpdateRequest) ::) external;

declare function local:XQ_ObtenerImpugnacionLocUpdate_TO_NTLMClient($input as element() (:: schema-element(ns1:ACCImpLineUpdateServiceUpdateRequest) ::)) as element() (:: schema-element(cli:request) ::)  {
        <cli:reqNTLM>
        <cli:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDCustIntegrationServices/xppservice.svc</cli:wsURL>
        <cli:host>aosaifsrv01</cli:host>
        <cli:user>svsbus</cli:user>
        <cli:password>cachito</cli:password>
        <cli:soapAction>http://tempuri.org/ACCImpLineUpdateService/update</cli:soapAction>
        <cli:xmlInput>
        {<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:tem="http://tempuri.org" xmlns:ent="http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKeyList" xmlns:ent1="http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKey" xmlns:acc="http://schemas.microsoft.com/dynamics/2008/01/documents/ACCImpLineUpdate">
           <soapenv:Header>
              <dat:CallContext>
                 <dat:Company>MQH</dat:Company>
              </dat:CallContext>
           </soapenv:Header>
           <soapenv:Body>
              <tem:ACCImpLineUpdateServiceUpdateRequest>
                 <ent:EntityKeyList>
                 {
                    for $DATOS_DETALLE in $input/ent:EntityKeyList/ent1:EntityKey
                    return
                    <ent1:EntityKey>
                       <ent1:KeyData>
                          <ent1:KeyField>
                             <ent1:Field>{fn:data($DATOS_DETALLE/ent1:KeyData/ent1:KeyField/ent1:Field)}</ent1:Field>
                             <ent1:Value>{fn:data($DATOS_DETALLE/ent1:KeyData/ent1:KeyField/ent1:Value)}</ent1:Value>
                          </ent1:KeyField>
                       </ent1:KeyData>
                    </ent1:EntityKey>
                  }
                 </ent:EntityKeyList>
                  
                 <acc:ACCImpLineUpdate>
                 {
                    for $DATOS_DETALLE_ACC in $input/acc:ACCImpLineUpdate/acc:ACCImpugmentLine_1
                    return
                      <acc:ACCImpugmentLine_1 class="entity" action="update">
                        <acc:_DocumentHash>{fn:data($DATOS_DETALLE_ACC/acc:_DocumentHash)}</acc:_DocumentHash>
                        <acc:RecId>{fn:data($DATOS_DETALLE_ACC/acc:RecId)}</acc:RecId>
                        <acc:ACCBillingCode>{fn:data($DATOS_DETALLE_ACC/acc:ACCBillingCode)}</acc:ACCBillingCode>
                        <acc:ACCClaimId>{fn:data($DATOS_DETALLE_ACC/acc:ACCClaimId)}</acc:ACCClaimId>
                        <acc:ACCImpugmentstate>{fn:data($DATOS_DETALLE_ACC/acc:ACCImpugmentstate)}</acc:ACCImpugmentstate>
                        <acc:ACCVoucherBSS>{fn:data($DATOS_DETALLE_ACC/acc:ACCVoucherBSS)}</acc:ACCVoucherBSS>
                        <acc:CompanyId>{fn:data($DATOS_DETALLE_ACC/acc:CompanyId)}</acc:CompanyId>
                        <acc:CustAccount>{fn:data($DATOS_DETALLE_ACC/acc:CustAccount)}</acc:CustAccount>
                        <acc:DeniedValue>{fn:data($DATOS_DETALLE_ACC/acc:DeniedValue)}</acc:DeniedValue>
                        <acc:Description>{fn:data($DATOS_DETALLE_ACC/acc:Description)}</acc:Description>
                        <acc:SubGroup>{fn:data($DATOS_DETALLE_ACC/acc:SubGroup)}</acc:SubGroup>
                        <acc:Value>{fn:data($DATOS_DETALLE_ACC/acc:Value)}</acc:Value>
                      </acc:ACCImpugmentLine_1>
                 }       
                 </acc:ACCImpLineUpdate>
              </tem:ACCImpLineUpdateServiceUpdateRequest>
           </soapenv:Body>
        </soapenv:Envelope>}
      </cli:xmlInput>
    </cli:reqNTLM>
};

local:XQ_ObtenerImpugnacionLocUpdate_TO_NTLMClient($input)