xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace cli="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/XSD/NTLMClient.xsd" ::)
declare namespace ns1="http://tempuri.org";
(:: import schema at "../WS/Schemas/XMLSchema_854349591.xsd" ::)

declare namespace que = "http://schemas.microsoft.com/dynamics/2006/02/documents/QueryCriteria";

declare variable $input as element() (:: schema-element(ns1:ACCImpLineUpdateServiceFindRequest) ::) external;

declare function local:XQ_ObtenerImpugnacionLocFind_TO_NTLMClient($input as element() (:: schema-element(ns1:ACCImpLineUpdateServiceFindRequest) ::)) as element() (:: schema-element(cli:request) ::) {
    <cli:reqNTLM>
        <cli:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDCustIntegrationServices/xppservice.svc</cli:wsURL>
        <cli:host>aosaifsrv01</cli:host>
        <cli:user>svsbus</cli:user>
        <cli:password>cachito</cli:password>
        <cli:soapAction>http://tempuri.org/ACCImpLineUpdateService/find</cli:soapAction>
        <cli:xmlInput>
        {<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:tem="http://tempuri.org" xmlns:quer="http://schemas.microsoft.com/dynamics/2006/02/documents/QueryCriteria">
         <soapenv:Header>
            <dat:CallContext>
               <dat:Company>MQH</dat:Company>
            </dat:CallContext>
         </soapenv:Header>
         <soapenv:Body>
            <tem:ACCImpLineUpdateServiceFindRequest>
               <quer:QueryCriteria>
                  <quer:CriteriaElement>
                     <quer:DataSourceName>{fn:data($input/que:QueryCriteria/que:CriteriaElement/que:DataSourceName)}</quer:DataSourceName>
                     <quer:FieldName>{fn:data($input/que:QueryCriteria/que:CriteriaElement/que:FieldName)}</quer:FieldName>
                     <quer:Operator>{fn:data($input/que:QueryCriteria/que:CriteriaElement/que:Operator)}</quer:Operator>
                     <quer:Value1>{fn:data($input/que:QueryCriteria/que:CriteriaElement/que:Value1)}</quer:Value1>
                  </quer:CriteriaElement>
               </quer:QueryCriteria>
            </tem:ACCImpLineUpdateServiceFindRequest>
         </soapenv:Body>
        </soapenv:Envelope>}
      </cli:xmlInput>
    </cli:reqNTLM>
};

local:XQ_ObtenerImpugnacionLocFind_TO_NTLMClient($input)