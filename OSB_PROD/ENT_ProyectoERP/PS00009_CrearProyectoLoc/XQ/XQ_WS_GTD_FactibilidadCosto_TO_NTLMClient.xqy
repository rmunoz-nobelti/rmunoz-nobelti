xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/GTDCostoFactibilidadService/create";
(:: import schema at "../Schemas/gtdFactibilidadCosto.xsd" ::)
declare namespace cli="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/XSD/NTLMClient.xsd" ::)

declare variable $inpud as element() (:: schema-element(ns1:GTDCostoFactibilidadServiceCreateRequest) ::) external;

declare function local:XQ_WS_GTD_ArticulosRequeridos_TO_NTLMClient($inpud as element() (:: schema-element(ns1:GTDCostoFactibilidadServiceCreateRequest) ::)) as element() (:: schema-element(cli:request) ::) {
    <cli:reqNTLM>
        <cli:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDFactibilidadCosto/xppservice.svc</cli:wsURL>
        <cli:host>axtest</cli:host>
        <cli:user>svsbus</cli:user>
        <cli:password>cachito</cli:password>
        <cli:soapAction>http://tempuri.org/GTDCostoFactibilidadService/create</cli:soapAction>
        <cli:xmlInput>{<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:tem="http://tempuri.org" xmlns:gtd="http://schemas.microsoft.com/dynamics/2008/01/documents/GTDCostoFactibilidad" xmlns:shar="http://schemas.microsoft.com/dynamics/2008/01/sharedtypes">
   <soapenv:Header>
      <dat:CallContext>
         <dat:Company>{fn:data($inpud/ns1:Company)}</dat:Company>
         <dat:Language>ES</dat:Language>
         <dat:LogonAsUser></dat:LogonAsUser>
         <dat:MessageId></dat:MessageId>
         <dat:PartitionKey></dat:PartitionKey>
      </dat:CallContext>
   </soapenv:Header>
   <soapenv:Body>
      <tem:GTDCostoFactibilidadServiceCreateRequest>
         <gtd:GTDCostoFactibilidad>
        {
              for $DATOS_DETALLE_Row in $inpud/ns1:ProjForecastCost
              return 
              <gtd:ProjForecastCost class="entity" >
                 <gtd:CategoryId>{fn:data($DATOS_DETALLE_Row/ns1:CategoryId)}</gtd:CategoryId>
                 <gtd:CostPrice>{fn:data($DATOS_DETALLE_Row/ns1:CostPrice)}</gtd:CostPrice>
                 <gtd:ProjId>{fn:data($DATOS_DETALLE_Row/ns1:ProjId)}</gtd:ProjId>
                 <gtd:Qty>{fn:data($DATOS_DETALLE_Row/ns1:Qty)}</gtd:Qty>
                 <gtd:StartDate>{fn:data($DATOS_DETALLE_Row/ns1:StartDate)}</gtd:StartDate>
                 <gtd:Txt>{fn:data($DATOS_DETALLE_Row/ns1:Txt)}</gtd:Txt>
              </gtd:ProjForecastCost>
        }
         </gtd:GTDCostoFactibilidad>
      </tem:GTDCostoFactibilidadServiceCreateRequest>
   </soapenv:Body>
</soapenv:Envelope>}</cli:xmlInput>
    </cli:reqNTLM>
};

local:XQ_WS_GTD_ArticulosRequeridos_TO_NTLMClient($inpud)