xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ACCeSalesLineQueryService/create";
(:: import schema at "../Schemas/gtdArticulosRequeridos.xsd" ::)
declare namespace cli="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/XSD/NTLMClient.xsd" ::)

declare variable $inpud as element() (:: schema-element(ns1:ACCeSalesLineQueryServiceCreateRequest) ::) external;

declare function local:XQ_WS_GTD_ArticulosRequeridos_TO_NTLMClient($inpud as element() (:: schema-element(ns1:ACCeSalesLineQueryServiceCreateRequest) ::)) as element() (:: schema-element(cli:request) ::) {
    <cli:reqNTLM>
        <cli:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDArticulosRequeridos/xppservice.svc</cli:wsURL>
        <cli:host>axtest</cli:host>
        <cli:user>svsbus</cli:user>
        <cli:password>cachito</cli:password>
        <cli:soapAction>http://tempuri.org/ACCeSalesLineQueryService/create</cli:soapAction>
        <cli:xmlInput>{<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:tem="http://tempuri.org" xmlns:acc="http://schemas.microsoft.com/dynamics/2008/01/documents/ACCeSalesLineQuery" xmlns:shar="http://schemas.microsoft.com/dynamics/2008/01/sharedtypes">
   <soapenv:Header>
      <dat:CallContext>
         <dat:Company>{fn:data($inpud/ns1:Company)}</dat:Company>
         <dat:Language>ES</dat:Language>
      </dat:CallContext>
   </soapenv:Header>
   <soapenv:Body>
      <tem:ACCeSalesLineQueryServiceCreateRequest>
         <acc:ACCeSalesLineQuery>
          {
              for $DATOS_DETALLE_Row in $inpud/ns1:SalesLine
              return 
              <acc:SalesLine class="entity" >
                 <acc:ItemId>{fn:data($DATOS_DETALLE_Row/ns1:ItemId)}</acc:ItemId>
                 <acc:LineNum>{fn:data($DATOS_DETALLE_Row/ns1:LineNum)}</acc:LineNum>
                 <acc:ProjId>{fn:data($DATOS_DETALLE_Row/ns1:ProjId)}</acc:ProjId>   
                 <acc:ReceiptDateRequested>{fn:data($DATOS_DETALLE_Row/ns1:ReceiptDateRequested)}</acc:ReceiptDateRequested>
                 <acc:Reservation>{fn:data($DATOS_DETALLE_Row/ns1:Reservation)}</acc:Reservation>
                 <acc:SalesQty>{fn:data($DATOS_DETALLE_Row/ns1:SalesQty)}</acc:SalesQty>
              </acc:SalesLine>
          }
         </acc:ACCeSalesLineQuery>
      </tem:ACCeSalesLineQueryServiceCreateRequest>
   </soapenv:Body>
</soapenv:Envelope>}</cli:xmlInput>
    </cli:reqNTLM>
};

local:XQ_WS_GTD_ArticulosRequeridos_TO_NTLMClient($inpud)