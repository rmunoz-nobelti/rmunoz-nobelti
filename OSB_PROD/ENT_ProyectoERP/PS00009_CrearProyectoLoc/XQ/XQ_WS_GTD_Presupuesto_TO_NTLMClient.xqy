xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ACCpProyBudgetServiceService/create";
(:: import schema at "../Schemas/gtdPresupuesto.xsd" ::)
declare namespace cli="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/XSD/NTLMClient.xsd" ::)

declare variable $inpud as element() (:: schema-element(ns1:ACCpProyBudgetServiceServiceCreateRequest) ::) external;

declare function local:XQ_WS_GTD_ArticulosRequeridos_TO_NTLMClient($inpud as element() (:: schema-element(ns1:ACCpProyBudgetServiceServiceCreateRequest) ::)) as element() (:: schema-element(cli:request) ::) {
    <cli:reqNTLM>
        <cli:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDPresupuesto/xppservice.svc</cli:wsURL>
        <cli:host>axtest</cli:host>
        <cli:user>svsbus</cli:user>
        <cli:password>{fn:data("cachito")}</cli:password>
        <cli:soapAction>http://tempuri.org/ACCpProyBudgetServiceService/create</cli:soapAction>
        <cli:xmlInput>{<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:tem="http://tempuri.org" xmlns:acc="http://schemas.microsoft.com/dynamics/2008/01/documents/ACCpProyBudgetService" xmlns:shar="http://schemas.microsoft.com/dynamics/2008/01/sharedtypes">
   <soapenv:Header>
      <dat:CallContext>
         <dat:Company>{fn:data($inpud/ns1:Company)}</dat:Company>
         <dat:Language>ES</dat:Language>
         <dat:LogonAsUser></dat:LogonAsUser>
         <dat:MessageId></dat:MessageId>
         <dat:PartitionKey></dat:PartitionKey>
      </dat:CallContext>
   </soapenv:Header>
   <soapenv:Body>
      <tem:ACCpProyBudgetServiceServiceCreateRequest>
         <acc:ACCpProyBudgetService>
            <acc:ProjBudget class="entity">
               <acc:Description>{fn:data($inpud/ns1:Description)}</acc:Description>
               <acc:RootProjId>{fn:data($inpud/ns1:RootProjId)}</acc:RootProjId>
<acc:BudgetWorkflowStatus>{fn:data($inpud/ns1:BudgetWorkflowStatus)}</acc:BudgetWorkflowStatus>
        <acc:OriginalBudgetForecastModel>{fn:data($inpud/ns1:OriginalBudgetForecastModel)}</acc:OriginalBudgetForecastModel>
        <acc:RemainingBudgetForecastModel>{fn:data($inpud/ns1:RemainingBudgetForecastModel)}</acc:RemainingBudgetForecastModel>
               {
                    for $DATOS_DETALLE_Row in $inpud/ns1:ProjBudgetLine
                    return 
                    <acc:ProjBudgetLine class="entity">
                      <acc:CategoryId>{fn:data($DATOS_DETALLE_Row/ns1:CategoryId)}</acc:CategoryId>
                      <acc:ProjId>{fn:data($DATOS_DETALLE_Row/ns1:ProjId)}</acc:ProjId>
                      <acc:ProjTransType>{fn:data($DATOS_DETALLE_Row/ns1:ProjTransType)}</acc:ProjTransType>
<acc:OriginalBudget>{fn:data($DATOS_DETALLE_Row/ns1:TotalBudget)}</acc:OriginalBudget>
                <acc:ProjBudgetLineType>{fn:data($DATOS_DETALLE_Row/ns1:ProjBudgetLineType)}</acc:ProjBudgetLineType>
                <acc:TotalBudget>{fn:data($DATOS_DETALLE_Row/ns1:TotalBudget)}</acc:TotalBudget>
                <acc:ProjAllocationMethod>{fn:data($DATOS_DETALLE_Row/ns1:ProjAllocationMethod)}</acc:ProjAllocationMethod>
                    </acc:ProjBudgetLine>
                }
            </acc:ProjBudget>
         </acc:ACCpProyBudgetService>
      </tem:ACCpProyBudgetServiceServiceCreateRequest>
   </soapenv:Body>
</soapenv:Envelope>}</cli:xmlInput>
    </cli:reqNTLM>
};

local:XQ_WS_GTD_ArticulosRequeridos_TO_NTLMClient($inpud)