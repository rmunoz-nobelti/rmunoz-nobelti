xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ACCeProjTableQueryService/create";
(:: import schema at "../Schemas/gtdCabeceraProyectos.xsd" ::)
declare namespace cli="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/XSD/NTLMClient.xsd" ::)

declare variable $inpud as element() (:: schema-element(ns1:ACCeProjTableQueryServiceCreateRequest) ::) external;

declare function local:XQ_WS_GTD_ArticulosRequeridos_TO_NTLMClient($inpud as element() (:: schema-element(ns1:ACCeProjTableQueryServiceCreateRequest) ::)) as element() (:: schema-element(cli:request) ::) {
    <cli:reqNTLM>
        <cli:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/ACCeProjTableQuery/xppservice.svc</cli:wsURL>
        <cli:host>axtest</cli:host>
        <cli:user>svsbus</cli:user>
        <cli:password>cachito</cli:password>
        <cli:soapAction>http://tempuri.org/ACCeProjTableQueryService/create</cli:soapAction>
        <cli:xmlInput>{<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:tem="http://tempuri.org" xmlns:acc="http://schemas.microsoft.com/dynamics/2008/01/documents/ACCeProjTableQuery" xmlns:shar="http://schemas.microsoft.com/dynamics/2008/01/sharedtypes">
   <soapenv:Header>
      <dat:CallContext>
         <dat:Company>{fn:data($inpud/ns1:Company)}</dat:Company>
         <dat:Language>ES</dat:Language>
      </dat:CallContext>
   </soapenv:Header>
   <soapenv:Body>
      <tem:ACCeProjTableQueryServiceCreateRequest>
         <acc:ACCeProjTableQuery>
            <acc:SenderId>{fn:data($inpud/ns1:Company)}</acc:SenderId>
            <acc:ProjTable class="entity">
               <acc:ACCeAddress>{fn:data($inpud/ns1:ACCeAddrress)}</acc:ACCeAddress>
               <acc:ACCeCostcenter>{fn:data($inpud/ns1:ACCeCostCenter)}</acc:ACCeCostcenter>
               {
                   if (fn:boolean($inpud/ns1:ACCeCodigoServicio)) then
                       <acc:ACCeCodigoServicio>{fn:data($inpud/ns1:ACCeCodigoServicio)}</acc:ACCeCodigoServicio>
                   else()
               }
               {
                   if (fn:boolean($inpud/ns1:ACCeFill)) then
                       <acc:ACCeFill>{fn:data($inpud/ns1:ACCeFill)}</acc:ACCeFill>
                   else()
               }
               {
                   if (fn:boolean($inpud/ns1:ACCeNFicha)) then
                       <acc:ACCeNFicha>{fn:data($inpud/ns1:ACCeNFicha)}</acc:ACCeNFicha>
                   else()
                   
               }
               {
                   if (fn:boolean($inpud/ns1:ACCeOrdenTrabajo)) then
                       <acc:ACCeOrdenTrabajo>{fn:data($inpud/ns1:ACCeOrdenTrabajo)}</acc:ACCeOrdenTrabajo>
                   else()
               }
               <acc:ACCesegmentid>{fn:data($inpud/ns1:ACCeSegmentId)}</acc:ACCesegmentid>
               <acc:CustAccount>{fn:data($inpud/ns1:CustAccount)}</acc:CustAccount>
               <acc:Name>{fn:data($inpud/ns1:Name)}</acc:Name>
               <acc:ProjectedEndDate>{fn:data($inpud/ns1:ProjectedEndDate)}</acc:ProjectedEndDate>
               <acc:ProjectedStartDate>{fn:data($inpud/ns1:ProjectedStartDate)}</acc:ProjectedStartDate>
               <acc:ProjGroupId>{fn:data($inpud/ns1:ProjGroupId)}</acc:ProjGroupId>
               {
                   if (fn:boolean($inpud/ns1:ProjId)) then
                       <acc:ProjId>{fn:data($inpud/ns1:ProjId)}</acc:ProjId>
                   else()
               }
               <acc:ACCParentId>{fn:data($inpud/ns1:ACCParentId)}</acc:ACCParentId>
               {
                   if (fn:boolean($inpud/ns1:ACCeHomePassProjected)) then
                       <acc:ACCeHomePassProjected>{fn:data($inpud/ns1:ACCeHomePassProjected)}</acc:ACCeHomePassProjected>
                   else()
               }
               {
                   if (fn:boolean($inpud/ns1:ACCeIdOportunidad)) then
                       <acc:ACCeIdOportunidad>{fn:data($inpud/ns1:ACCeIdOportunidad)}</acc:ACCeIdOportunidad>
                   else()
               }
               {
                   if (fn:boolean($inpud/ns1:ACCeQtyFilamentProjected)) then
                       <acc:ACCeQtyFilamentProjected>{fn:data($inpud/ns1:ACCeQtyFilamentProjected)}</acc:ACCeQtyFilamentProjected>
                   else()
               }
               <acc:Type>{fn:data($inpud/ns1:Type)}</acc:Type>
            </acc:ProjTable>
         </acc:ACCeProjTableQuery>
      </tem:ACCeProjTableQueryServiceCreateRequest>
   </soapenv:Body>
</soapenv:Envelope>}</cli:xmlInput>
    </cli:reqNTLM>
};

local:XQ_WS_GTD_ArticulosRequeridos_TO_NTLMClient($inpud)