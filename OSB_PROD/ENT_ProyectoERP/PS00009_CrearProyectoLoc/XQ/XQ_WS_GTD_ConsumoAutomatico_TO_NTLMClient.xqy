xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/GTDInventJournalPostService/create";
(:: import schema at "../Schemas/gtdConsumoAutomatico.xsd" ::)
declare namespace cli="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/XSD/NTLMClient.xsd" ::)

declare variable $inpud as element() (:: schema-element(ns1:GTDInventJournalPostServiceCreateRequest) ::) external;

declare function local:XQ_WS_GTD_ArticulosRequeridos_TO_NTLMClient($inpud as element() (:: schema-element(ns1:GTDInventJournalPostServiceCreateRequest) ::)) as element() (:: schema-element(cli:request) ::) {
    <cli:reqNTLM>
        <cli:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDConsumoAutomatico/xppservice.svc</cli:wsURL>
        <cli:host>axtest</cli:host>
        <cli:user>svsbus</cli:user>
        <cli:password>cachito</cli:password>
        <cli:soapAction>http://tempuri.org/GTDInventJournalPostService/CreatingJournalHeader</cli:soapAction>
        <cli:xmlInput>{<soapenv:Envelope xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:dyn="http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org">
   <soapenv:Header>
      <dat:CallContext>
         <dat:Company>{fn:data($inpud/ns1:Company)}</dat:Company>
         <dat:Language>ES</dat:Language>
      </dat:CallContext>
   </soapenv:Header>
   <soapenv:Body>
      <tem:GTDInventJournalPostServiceCreatingJournalHeaderRequest>
         <tem:_journalHeader>
            <dyn:JournalBLines>
            {
                for $DATOS_DETALLE_Row in $inpud/ns1:GTDInventJournalLinesContract
                return 
                <dyn:GTDInventJournalLinesContract>
                    <dyn:ItemId>{fn:data($DATOS_DETALLE_Row/ns1:ItemId)}</dyn:ItemId>
                    <dyn:Qty>{fn:data($DATOS_DETALLE_Row/ns1:Qty)}</dyn:Qty>
                    <dyn:parmInventLocationId>{fn:data($DATOS_DETALLE_Row/ns1:parmInventLocationId)}</dyn:parmInventLocationId>
                    <dyn:parmInventSiteId>{fn:data($DATOS_DETALLE_Row/ns1:parmInventSiteId)}</dyn:parmInventSiteId>
                    {
                       if (fn:boolean($DATOS_DETALLE_Row/ns1:parmInventStatusId)) then
                           <dyn:parmInventStatusId>{fn:data($DATOS_DETALLE_Row/ns1:parmInventStatusId)}</dyn:parmInventStatusId>
                       else()
                     }
                    <dyn:parmProjId>{fn:data($DATOS_DETALLE_Row/ns1:parmProjId)}</dyn:parmProjId>
                    <dyn:parmProjLinePropertyId>{fn:data($DATOS_DETALLE_Row/ns1:parmProjLinePropertyId)}</dyn:parmProjLinePropertyId>
                    <dyn:parmTransDate>{fn:data($DATOS_DETALLE_Row/ns1:parmTransDate)}</dyn:parmTransDate>
                    <dyn:parmWMSLocationId>{fn:data($DATOS_DETALLE_Row/ns1:parmWMSLocationId)}</dyn:parmWMSLocationId>
                    {
                       if (fn:boolean($DATOS_DETALLE_Row/ns1:parmLicensePlateId)) then
                           <dyn:parmLicensePlateId>{fn:data($DATOS_DETALLE_Row/ns1:parmLicensePlateId)}</dyn:parmLicensePlateId>
                       else()
                     }
                     {
                       if (fn:boolean($DATOS_DETALLE_Row/ns1:parmInventSerialId)) then
                           <dyn:parmInventSerialId>{fn:data($DATOS_DETALLE_Row/ns1:parmInventSerialId)}</dyn:parmInventSerialId>
                       else()
                     }
                 </dyn:GTDInventJournalLinesContract>
            }
            </dyn:JournalBLines>
            <dyn:journalATable>
               <dyn:GTDInventJournalHeaderContract/>
            </dyn:journalATable>
            <dyn:parmACCIssuePointDescription>{fn:data($inpud/ns1:parmACCIssuePointDescription)}</dyn:parmACCIssuePointDescription>
            <dyn:parmACCTradeDocumentId>{fn:data($inpud/ns1:parmACCTradeDocumentId)}</dyn:parmACCTradeDocumentId>
            <dyn:parmDescription>{fn:data($inpud/ns1:parmDescription)}</dyn:parmDescription>
            <dyn:parmJournalNameId>{fn:data($inpud/ns1:parmJournalNameId)}</dyn:parmJournalNameId>
            <dyn:parmNO2CLDeliveryName>{fn:data($inpud/ns1:parmNO2CLDeliveryName)}</dyn:parmNO2CLDeliveryName>
            <dyn:parmNO2CLPackingSlipAddress>{fn:data($inpud/ns1:parmNO2CLPackingSlipAddress)}</dyn:parmNO2CLPackingSlipAddress>
            {
               if (fn:boolean($inpud/ns1:parmGTD_DimActi_Instal)) then
                   <dyn:parmGTD_DimActi_Instal>{fn:data($inpud/ns1:parmGTD_DimActi_Instal)}</dyn:parmGTD_DimActi_Instal>
               else()
             }
             {
               if (fn:boolean($inpud/ns1:parmGTD_Act_Fix)) then
                   <dyn:parmGTD_Act_Fix>{fn:data($inpud/ns1:parmGTD_Act_Fix)}</dyn:parmGTD_Act_Fix>
               else()
             }
             {
               if (fn:boolean($inpud/ns1:ParmGTD_DIMCostCenter)) then
                   <dyn:ParmGTD_DIMCostCenter>{fn:data($inpud/ns1:ParmGTD_DIMCostCenter)}</dyn:ParmGTD_DIMCostCenter>
               else()
             }
             {
               if (fn:boolean($inpud/ns1:parmGTD_DimAffiliatedCompany)) then
                   <dyn:parmGTD_DimAffiliatedCompany>{fn:data($inpud/ns1:parmGTD_DimAffiliatedCompany)}</dyn:parmGTD_DimAffiliatedCompany>
               else()
             }
             {
               if (fn:boolean($inpud/ns1:parmGTD_DimCustomer)) then
                   <dyn:parmGTD_DimCustomer>{fn:data($inpud/ns1:parmGTD_DimCustomer)}</dyn:parmGTD_DimCustomer>
               else()
             }
             {
               if (fn:boolean($inpud/ns1:parmGTD_DimArea)) then
                   <dyn:parmGTD_DimArea>{fn:data($inpud/ns1:parmGTD_DimArea)}</dyn:parmGTD_DimArea>
               else()
             }
         </tem:_journalHeader>
      </tem:GTDInventJournalPostServiceCreatingJournalHeaderRequest>
   </soapenv:Body>
</soapenv:Envelope>}</cli:xmlInput>
    </cli:reqNTLM>
};

local:XQ_WS_GTD_ArticulosRequeridos_TO_NTLMClient($inpud)