xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/GTDArticulosFactibilidadService/create";
(:: import schema at "../Schemas/gtdFactibilidadArticulo.xsd" ::)
declare namespace cli="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/XSD/NTLMClient.xsd" ::)

declare variable $inpud as element() (:: schema-element(ns1:GTDArticulosFactibilidadServiceCreateRequest) ::) external;

declare function local:XQ_WS_GTD_ArticulosRequeridos_TO_NTLMClient($inpud as element() (:: schema-element(ns1:GTDArticulosFactibilidadServiceCreateRequest) ::)) as element() (:: schema-element(cli:request) ::) {
    <cli:reqNTLM>
        <cli:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDFactibilidadArticulo/xppservice.svc</cli:wsURL>
        <cli:host>axtest</cli:host>
        <cli:user>svsbus</cli:user>
        <cli:password>cachito</cli:password>
        <cli:soapAction>http://tempuri.org/GTDArticulosFactibilidadService/create</cli:soapAction>
        <cli:xmlInput>{<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:tem="http://tempuri.org" xmlns:gtd="http://schemas.microsoft.com/dynamics/2008/01/documents/GTDArticulosFactibilidad" xmlns:shar="http://schemas.microsoft.com/dynamics/2008/01/sharedtypes">
   <soapenv:Header>
      <dat:CallContext>
         <dat:Company>{fn:data($inpud/ns1:Company)}</dat:Company>
         <dat:Language></dat:Language>
         <dat:LogonAsUser></dat:LogonAsUser>
         <dat:MessageId></dat:MessageId>
         <dat:PartitionKey></dat:PartitionKey>
      </dat:CallContext>
   </soapenv:Header>
   <soapenv:Body>
      <tem:GTDArticulosFactibilidadServiceCreateRequest>
         <gtd:GTDArticulosFactibilidad>
         {
              for $DATOS_DETALLE_Row in $inpud/ns1:ForecastSales
              return 
              <gtd:ForecastSales class="entity" >
                 <gtd:CostPrice>{fn:data($DATOS_DETALLE_Row/ns1:CostPrice)}</gtd:CostPrice>
                 <gtd:ItemId>{fn:data($DATOS_DETALLE_Row/ns1:ItemId)}</gtd:ItemId>
                 <gtd:ProjCategoryId>{fn:data($DATOS_DETALLE_Row/ns1:ProjCategoryId)}</gtd:ProjCategoryId>
                 <gtd:ProjId>{fn:data($DATOS_DETALLE_Row/ns1:ProjId)}</gtd:ProjId>
                 <gtd:SalesQty>{fn:data($DATOS_DETALLE_Row/ns1:SalesQty)}</gtd:SalesQty>
                 <gtd:StartDate>{fn:data($DATOS_DETALLE_Row/ns1:StartDate)}</gtd:StartDate>
              </gtd:ForecastSales>
          }
         </gtd:GTDArticulosFactibilidad>
      </tem:GTDArticulosFactibilidadServiceCreateRequest>
   </soapenv:Body>
</soapenv:Envelope>}</cli:xmlInput>
    </cli:reqNTLM>
};

local:XQ_WS_GTD_ArticulosRequeridos_TO_NTLMClient($inpud)