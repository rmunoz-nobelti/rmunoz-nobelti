xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ProyectoERP/crearProyecto";
(:: import schema at "../XSD/crearProyecto.xsd" ::)

declare variable $reason as xs:string external;

declare function local:XQ_CrearProyectoValidacion_CrearProyectoExp($reason as xs:string) as element() (:: schema-element(ns1:crearProyectoRsp) ::) {
    if (fn:contains($reason,"rutCliente")) then
                  <ns1:crearProyectoRsp>
                      <ns1:errorCodigo>V001</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                      <ns1:errorDescripcion>Rut debe venir con digito verificador y guion y rellenado con ceros a la izquierda, máximo 10 caracteres.</ns1:errorDescripcion>    
                  </ns1:crearProyectoRsp>
    else  if (fn:contains($reason,"empresa")) then
                <ns1:crearProyectoRsp>
                      <ns1:errorCodigo>V002</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>Valor empresa inválido. Solo 'TLD', 'MQH'</ns1:errorDescripcion>    
                  </ns1:crearProyectoRsp>
    else  if (fn:contains($reason,"fecha")) then
                <ns1:crearProyectoRsp>
                      <ns1:errorCodigo>V003</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>fechaInicial no debe ser mayor a fechaFinal</ns1:errorDescripcion>    
                  </ns1:crearProyectoRsp>
    else  if (fn:contains($reason,"SegmentoCliente")) then
                <ns1:crearProyectoRsp>
                      <ns1:errorCodigo>V004</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>Valor segmentoCliente inválido</ns1:errorDescripcion>    
                  </ns1:crearProyectoRsp>
    else  if (fn:contains($reason,"GrupoProyecto")) then
                <ns1:crearProyectoRsp>
                      <ns1:errorCodigo>V005</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>Valor grupoProyecto inválido</ns1:errorDescripcion>    
                  </ns1:crearProyectoRsp>
   else  if (fn:contains($reason,"Comunas")) then
                <ns1:crearProyectoRsp>
                      <ns1:errorCodigo>V006</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>Valor Comuna inválido</ns1:errorDescripcion>    
                  </ns1:crearProyectoRsp>
   else  if (fn:contains($reason,"CentroCosto")) then
                <ns1:crearProyectoRsp>
                      <ns1:errorCodigo>V007</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>Valor centroCosto inválido</ns1:errorDescripcion>    
                  </ns1:crearProyectoRsp>
   else  if (fn:contains($reason,"idTablaERP")) then
                <ns1:crearProyectoRsp>
                      <ns1:errorCodigo>V008</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>Tipo de dato idTablaERP inválido</ns1:errorDescripcion>    
                  </ns1:crearProyectoRsp>
            else (
              <ns1:crearProyectoRsp>
                      <ns1:errorCodigo>V009</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>Error en datos de entrada al servicio</ns1:errorDescripcion>    
                  </ns1:crearProyectoRsp>
            )
};

local:XQ_CrearProyectoValidacion_CrearProyectoExp($reason)