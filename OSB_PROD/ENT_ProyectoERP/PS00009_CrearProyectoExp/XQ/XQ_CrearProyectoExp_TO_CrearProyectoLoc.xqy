xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ProyectoERP/crearProyecto";
(:: import schema at "../../PS00009_CrearProyectoLoc/JCA/Schemas/crearProyecto.xsd" ::)

declare variable $input as element() (:: schema-element(ns1:crearProyectoReq) ::) external;

declare function local:XQ_CrearProyectoExp_TO_CrearProyectoLoc($input as element() (:: schema-element(ns1:crearProyectoReq) ::)) as element() (:: schema-element(ns1:crearProyectoReq) ::) {
    <ns1:crearProyectoReq>
        <ns1:tipoProyecto>{fn:data($input/ns1:tipoProyecto)}</ns1:tipoProyecto>
        <ns1:empresa>{fn:data($input/ns1:empresa)}</ns1:empresa>
        <ns1:nombreProyecto>{fn:data($input/ns1:nombreProyecto)}</ns1:nombreProyecto>
        <ns1:rutCliente>{fn:data($input/ns1:rutCliente)}</ns1:rutCliente>
        <ns1:grupoProyecto>{dvmtr:lookup("ENT_ProyectoERP/PS00009_CrearProyectoExp/DVM/GrupoProyecto", "Key", fn:string($input/ns1:grupoProyecto), "Value", "")}</ns1:grupoProyecto>
        <ns1:fechaInicial>{fn:data($input/ns1:fechaInicial)}</ns1:fechaInicial>
        <ns1:fechaFinal>{fn:data($input/ns1:fechaFinal)}</ns1:fechaFinal>
        <ns1:segmentoCliente>{dvmtr:lookup("ENT_ProyectoERP/PS00009_CrearProyectoExp/DVM/SegmentoCliente", "Key", fn:string($input/ns1:segmentoCliente), "Value", "")}</ns1:segmentoCliente>
        <ns1:comuna>{dvmtr:lookup("ENT_ProyectoERP/PS00009_CrearProyectoExp/DVM/Comunas", "Key", fn:string($input/ns1:comuna), "Value", "")}</ns1:comuna>
        <ns1:centroCosto>{dvmtr:lookup("ENT_ProyectoERP/PS00009_CrearProyectoExp/DVM/CentroCosto", "Key", fn:string($input/ns1:centroCosto), "Value", "")}</ns1:centroCosto>
        <ns1:idTablaERP>{fn:data($input/ns1:idTablaERP)}</ns1:idTablaERP>
        <ns1:idProyectoPadre>{fn:data($input/ns1:idProyectoPadre)}</ns1:idProyectoPadre>
        {
            if ($input/ns1:numeroFicha)
            then <ns1:numeroFicha>{fn:data($input/ns1:numeroFicha)}</ns1:numeroFicha>
            else (<ns1:numeroFicha></ns1:numeroFicha>)
        }
        {
            if ($input/ns1:codFiil)
            then <ns1:codFiil>{fn:data($input/ns1:codFiil)}</ns1:codFiil>
            else (<ns1:codFiil></ns1:codFiil>)
        }
        {
            if ($input/ns1:codServicio)
            then <ns1:codServicio>{fn:data($input/ns1:codServicio)}</ns1:codServicio>
            else (<ns1:codServicio></ns1:codServicio>)
        }
        {
            if ($input/ns1:idOportunidad)
            then <ns1:idOportunidad>{fn:data($input/ns1:idOportunidad)}</ns1:idOportunidad>
            else (<ns1:idOportunidad></ns1:idOportunidad>)
        }
        {
            if ($input/ns1:codOrdenTrabajo)
            then <ns1:codOrdenTrabajo>{fn:data($input/ns1:codOrdenTrabajo)}</ns1:codOrdenTrabajo>
            else (<ns1:codOrdenTrabajo></ns1:codOrdenTrabajo>)
        }
        {
            if ($input/ns1:hpHomePass)
            then <ns1:hpHomePass>{fn:data($input/ns1:hpHomePass)}</ns1:hpHomePass>
            else (<ns1:hpHomePass></ns1:hpHomePass>)
        }
        {
            if ($input/ns1:filamentos)
            then <ns1:filamentos>{fn:data($input/ns1:filamentos)}</ns1:filamentos>
            else (<ns1:filamentos></ns1:filamentos>)
        }
    </ns1:crearProyectoReq>
};

local:XQ_CrearProyectoExp_TO_CrearProyectoLoc($input)