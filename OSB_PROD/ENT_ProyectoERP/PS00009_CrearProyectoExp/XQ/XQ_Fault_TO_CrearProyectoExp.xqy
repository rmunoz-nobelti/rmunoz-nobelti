xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ProyectoERP/crearProyecto";
(:: import schema at "../XSD/crearProyecto.xsd" ::)

declare variable $errorCod as xs:string external;
declare variable $errorTip as xs:string external;
declare variable $errorDesc as xs:string external;

declare function local:XQ_Fault_TO_CrearProyectoExp($errorCod as xs:string, 
                                                    $errorTip as xs:string, 
                                                    $errorDesc as xs:string) 
                                                    as element() (:: schema-element(ns1:crearProyectoRsp) ::) {
    <ns1:crearProyectoRsp>
        <ns1:errorCodigo>{fn:data($errorCod)}</ns1:errorCodigo>
        <ns1:errorTipo>{fn:data($errorTip)}</ns1:errorTipo>
        <ns1:errorDescripcion>{fn:data($errorDesc)}</ns1:errorDescripcion>
    </ns1:crearProyectoRsp>
};

local:XQ_Fault_TO_CrearProyectoExp($errorCod, $errorTip, $errorDesc)