xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "../XSD/listarHijosServiciosResponse.xsd" ::)

declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/PGET_LISTA_HIJOS";
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/PGET_LISTA_SERVICIOS";

declare variable $request as element() external;

declare function local:func($request as element()) as element() (:: schema-element(RECLAMO) ::) {
<XA_TELE_recl_hijo>
{
    for $RECLAMO in $request/ns1:RET_CUR_Row
    return 
    <RECLAMO>
        <ID_RECLAMO_HIJO>{fn:data($RECLAMO/ns1:ID_RECLAMO_HIJO)}</ID_RECLAMO_HIJO>
        <FECHA_RECEPCION>{fn:data($RECLAMO/ns1:FECHA_RECEPCION)}</FECHA_RECEPCION>
        <ESTADO>{fn:data($RECLAMO/ns1:ESTADO)}</ESTADO>
        <NOMBRE_CLIENTE>{fn:data($RECLAMO/ns1:NOMBRE_CLIENTE)}</NOMBRE_CLIENTE>
        <CABLE>{fn:data($RECLAMO/ns1:CABLE)}</CABLE>
        <FILAMENTO>{fn:data($RECLAMO/ns1:FILAMENTO)}</FILAMENTO>
        <SERVICIOS_RECLAMADOS>{fn:data($RECLAMO/ns1:SERVICIOS_RECLAMADOS)}</SERVICIOS_RECLAMADOS>
        <LISTA_SERVICIOS>
            {
                for $SERVICIO in $RECLAMO/ns2:RET_CUR/ns2:RET_CUR_Row
                return 
                <SERVICIO>
                    <COD_SERVICIO>{fn:data($SERVICIO/ns2:COD_SERVICIO)}</COD_SERVICIO>
                    <DESCRIPCION_SERVICIO>{fn:data($SERVICIO/ns2:DESCRIPCION_SERVICIO)}</DESCRIPCION_SERVICIO>
                    <DIRECCION_ORIGEN>{fn:data($SERVICIO/ns2:DIRECCION_ORIGEN)}</DIRECCION_ORIGEN>
                    <COMUNA_ORIGEN>{fn:data($SERVICIO/ns2:COMUNA_ORIGEN)}</COMUNA_ORIGEN>
                    <CIUDAD_ORIGEN>{fn:data($SERVICIO/ns2:CIUDAD_ORIGEN)}</CIUDAD_ORIGEN>
                    <DIRECCION_DESTINO>{fn:data($SERVICIO/ns2:DIRECCION_DESTINO)}</DIRECCION_DESTINO>
                    <COMUNA_DESTINO>{fn:data($SERVICIO/ns2:COMUNA_DESTINO)}</COMUNA_DESTINO>
                    <CIUDAD_DESTINO>{fn:data($SERVICIO/ns2:CIUDAD_DESTINO)}</CIUDAD_DESTINO>
                    <LAT_84_DESTINO>{fn:data($SERVICIO/ns2:LAT_84_DESTINO)}</LAT_84_DESTINO>
                    <LON_84_DESTINO>{fn:data($SERVICIO/ns2:LON_84_DESTINO)}</LON_84_DESTINO>
                </SERVICIO>
            }
        </LISTA_SERVICIOS>
    </RECLAMO>
    }
</XA_TELE_recl_hijo>
};

local:func($request)