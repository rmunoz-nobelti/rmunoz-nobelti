xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/PGET_LISTA_SERVICIOS";
(:: import schema at "../BS/Schemas/PGET_LISTA_SERVICIOS_sp.xsd" ::)

declare variable $idHijo as xs:string external;

declare function local:func($idHijo as xs:string) as element() (:: schema-element(ns1:InputParameters) ::) {
    <ns1:InputParameters>
        <ns1:VID_RECLAMO>{fn:data($idHijo)}</ns1:VID_RECLAMO>
    </ns1:InputParameters>
};

local:func($idHijo)