xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.gtd.com/schema/reclamos/cierreActividadResponse";
(:: import schema at "../XSD/CierreActividadResponse.xsd" ::)

declare variable $codigo as xs:string external;
declare variable $descripcion as xs:string external;

declare function local:func($codigo as xs:string, 
                            $descripcion as xs:string) 
                            as element() (:: schema-element(ns1:cierreActividadResponse) ::) {
    <ns1:cierreActividadResponse>
        <ns1:codigo>{fn:data($codigo)}</ns1:codigo>
        <ns1:descripcion>{fn:data($descripcion)}</ns1:descripcion>
    </ns1:cierreActividadResponse>
};

local:func($codigo, $descripcion)