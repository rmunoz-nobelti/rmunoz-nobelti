xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.gtd.com/schema/reclamos/cierreActividadRequest";
(:: import schema at "../XSD/CierreActividadRequest.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/PPUT_CIERRA_ACTIVIDAD_TOA";
(:: import schema at "../../PSL0003_CierreActividad/XSD/PPUT_CIERRA_ACTIVIDAD_TOA_sp.xsd" ::)

declare variable $request as element() (:: schema-element(ns1:cierreActividadRequest) ::) external;

declare function local:func($request as element() (:: schema-element(ns1:cierreActividadRequest) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:P_IDACTIVIDADTOA>{fn:data($request/ns1:idActividadTOA)}</ns2:P_IDACTIVIDADTOA>
        <ns2:P_LON84EFECTIVO>{fn:data($request/ns1:lon84Efectivo)}</ns2:P_LON84EFECTIVO>
        <ns2:P_LAT84EFECTIVO>{fn:data($request/ns1:lat84Efectivo)}</ns2:P_LAT84EFECTIVO>
        {
            if ($request/ns1:lugarTrabajo)
            then <ns2:P_LUGARTRABAJO>{fn:data($request/ns1:lugarTrabajo)}</ns2:P_LUGARTRABAJO>
            else ()
        }
        {
            if ($request/ns1:asignacionFolios)
            then <ns2:P_ASIGNACIONFOLIOS>{fn:data($request/ns1:asignacionFolios)}</ns2:P_ASIGNACIONFOLIOS>
            else ()
        }
        <ns2:P_TIEMPOTRABAJO>{fn:data($request/ns1:tiempoTrabajo)}</ns2:P_TIEMPOTRABAJO>
        <ns2:P_FECHACIERRE>{
        
        let $fechaIn := substring-before($request/ns1:fechaCierre,' ')
        let $fechaOut := concat(substring($fechaIn,9,2),'/',substring($fechaIn,6,2),'/',substring($fechaIn,1,4))
        let $time := substring-after($request/ns1:fechaCierre,' ')
        return fn-bea:dateTime-from-string-with-format("dd/MM/yyyy HH:mm:ss",concat($fechaOut,' ', $time))
        
        
        }</ns2:P_FECHACIERRE>
        {
            if ($request/ns1:cambiosRed)
            then <ns2:P_CAMBIOSRED>{fn:data($request/ns1:cambiosRed)}</ns2:P_CAMBIOSRED>
            else ()
        }
        {
            if ($request/ns1:conformidadRutPasaporte)
            then <ns2:P_CONFORMIDADRUTPASAPORTE>{fn:data($request/ns1:conformidadRutPasaporte)}</ns2:P_CONFORMIDADRUTPASAPORTE>
            else ()
        }
        {
            if ($request/ns1:conformidadNombre)
            then <ns2:P_CONFORMIDADNOMBRE>{fn:data($request/ns1:conformidadNombre)}</ns2:P_CONFORMIDADNOMBRE>
            else ()
        }
        {
            if ($request/ns1:conformidadFirma)
            then <ns2:P_CONFORMIDADFIRMA>{fn:data($request/ns1:conformidadFirma)}</ns2:P_CONFORMIDADFIRMA>
            else ()
        }
        {
            if ($request/ns1:observacionesCierre)
            then <ns2:P_OBSERVACIONESCIERRE>{fn:data($request/ns1:observacionesCierre)}</ns2:P_OBSERVACIONESCIERRE>
            else ()
        }
        {
            if ($request/ns1:tecnicoInicioActividad)
            then <ns2:P_TECNICOINICIOACTIVIDAD>{fn:data($request/ns1:tecnicoInicioActividad)}</ns2:P_TECNICOINICIOACTIVIDAD>
            else ()
        }
        {
            if ($request/ns1:clasificacion)
            then <ns2:P_CLASIFICACION>{fn:data($request/ns1:clasificacion)}</ns2:P_CLASIFICACION>
            else ()
        }
    </ns2:InputParameters>
};

local:func($request)