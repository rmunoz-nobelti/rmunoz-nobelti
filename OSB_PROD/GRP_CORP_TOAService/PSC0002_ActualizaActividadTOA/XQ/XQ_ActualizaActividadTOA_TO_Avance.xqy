xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/PPUT_AVANCE";
(:: import schema at "../../PSL0004_Avance/XSD/PPUT_AVANCE_sp.xsd" ::)

declare variable $idActividad as xs:string external;
declare variable $fecha as xs:string external;
declare variable $accion as xs:string external;
declare variable $cambios as xs:string external;
declare variable $usuario as xs:string external;

declare function local:func($idActividad as xs:string,$fecha as xs:string,$accion as xs:string,$cambios as xs:string,$usuario as xs:string) as element() (:: schema-element(ns1:InputParameters) ::) {
    <ns1:InputParameters>
        <ns1:P_IDACTIVIDADTOA>{xs:decimal(fn:data($idActividad))}</ns1:P_IDACTIVIDADTOA>
        <ns1:P_FECHA>
        {
        let $fechaIn := substring-before($fecha,' ')
        let $fechaOut := concat(substring($fechaIn,9,2),'/',substring($fechaIn,6,2),'/',substring($fechaIn,1,4))
        let $time := substring-after($fecha,' ')
        return fn-bea:dateTime-from-string-with-format("dd/MM/yyyy HH:mm:ss",concat($fechaOut,' ', $time))
        }
        </ns1:P_FECHA>
        <ns1:P_ACCION>{fn:data($accion)}</ns1:P_ACCION>
        <ns1:P_CAMBIOS>{fn:data($cambios)}</ns1:P_CAMBIOS>
        <ns1:P_USUARIO>{fn:data($usuario)}</ns1:P_USUARIO>
    </ns1:InputParameters>
    
};
(: fn-bea:dateTime-from-string-with-format("yyyy/DD/MM hh:mm",fn:data($fecha)):)
local:func($idActividad,$fecha,$accion,$cambios,$usuario)