xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.gtd.com/schema/reclamos/actualizaActividadRequest";
(:: import schema at "../XSD/ActualizaActividadRequest.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/PPUT_ACTUALIZA_ACTIVIDAD_TOA";
(:: import schema at "../../PSL0005_ActualizaActividad/XSD/PPUT_ACTUALIZA_ACTIVIDAD_TOA_sp.xsd" ::)

declare variable $request as element() (:: schema-element(ns1:actualizaActividadRequest) ::) external;

declare function local:func($request as element() (:: schema-element(ns1:actualizaActividadRequest) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:P_IDACTIVIDADTOA>{fn:data($request/ns1:idActividadTOA)}</ns2:P_IDACTIVIDADTOA>
        {
            if ($request/ns1:estadoTOA)
            then <ns2:P_ESTADO>{fn:data($request/ns1:estadoTOA)}</ns2:P_ESTADO>
            else ()
        }
    </ns2:InputParameters>
};

local:func($request)