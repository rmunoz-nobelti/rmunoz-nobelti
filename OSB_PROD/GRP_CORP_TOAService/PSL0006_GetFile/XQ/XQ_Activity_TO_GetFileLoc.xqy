xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.gtd.com/getFileResponse";
(:: import schema at "../XSD/getFileResponse.xsd" ::)
declare namespace ns1="urn:toa:activity";
(:: import schema at "../../../OFSC/Activity/WSDLs/activity.wsdl" ::)

declare variable $getFileResponse as element() (:: schema-element(ns1:get_file_response) ::) external;

declare function local:func($getFileResponse as element() (:: schema-element(ns1:get_file_response) ::)) as element() (:: schema-element(ns2:getFileResponse) ::) {
    <ns2:getFileResponse>
        <ns2:codigoResultado>{fn:data($getFileResponse/result_code)}</ns2:codigoResultado>
        <ns2:mensajeError>{fn:data($getFileResponse/error_msg)}</ns2:mensajeError>
        <ns2:nombreArchivo>{fn:data($getFileResponse/file_name)}</ns2:nombreArchivo>
        <ns2:datosArchivo>{fn:data($getFileResponse/file_data)}</ns2:datosArchivo>
        <ns2:tipoMime>{fn:data($getFileResponse/file_mime_type)}</ns2:tipoMime>
    </ns2:getFileResponse>
};

local:func($getFileResponse)