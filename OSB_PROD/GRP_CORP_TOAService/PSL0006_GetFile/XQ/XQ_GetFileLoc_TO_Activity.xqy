xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.gtd.com/getFileRequest";
(:: import schema at "../XSD/getFileRequest.xsd" ::)
declare namespace ns2="urn:toa:activity";
(:: import schema at "../../../OFSC/Activity/WSDLs/activity.wsdl" ::)

declare variable $getFileRequest as element() (:: schema-element(ns1:getFileRequest) ::) external;

declare function local:func($getFileRequest as element() (:: schema-element(ns1:getFileRequest) ::)) as element() (:: schema-element(ns2:get_file) ::) {
    <ns2:get_file>
        <entity_id>{fn:data($getFileRequest/ns1:idEntidad)}</entity_id>
        <property_id>{fn:data($getFileRequest/ns1:idPropiedad)}</property_id>
    </ns2:get_file>
};

local:func($getFileRequest)