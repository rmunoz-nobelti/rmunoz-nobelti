xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.grupogtd.com/schema/Material/gestionarMaterial";
(:: import schema at "../XSD/gestionarMaterial.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/SP_PPUT_MATERIAL";
(:: import schema at "../../PS00006_GestionarMaterialLoc/JCA/Schemas/SP_PPUT_MATERIAL_sp.xsd" ::)

declare variable $outPut as element() (:: schema-element(ns1:OutputParameters) ::) external;

declare function local:XQ_GestionarMaterialLoc_TO_GestionarMaterialExp($outPut as element() (:: schema-element(ns1:OutputParameters) ::)) as element() (:: schema-element(ns2:gestionarMaterialRsp) ::) {
   if (fn:contains(fn:data($outPut/ns1:GLS_ERR_OUT),"ERR_INT_0019")) then
                  <ns1:gestionarMaterialRsp>
                      <ns1:errorCodigo>V005</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                      <ns1:errorDescripcion>No es posible modificar subcategoria de EQUIPOS TLD.</ns1:errorDescripcion>    
                  </ns1:gestionarMaterialRsp>
      else(
        <ns2:gestionarMaterialRsp>
            <ns2:errorCodigo>{fn:data($outPut/ns1:CODIGO_ERROR_OUT)}</ns2:errorCodigo>
            <ns2:errorTipo></ns2:errorTipo>
            <ns2:errorDescripcion>{fn:data($outPut/ns1:GLS_ERR_OUT)}</ns2:errorDescripcion>
        </ns2:gestionarMaterialRsp>
        )
};

local:XQ_GestionarMaterialLoc_TO_GestionarMaterialExp($outPut)