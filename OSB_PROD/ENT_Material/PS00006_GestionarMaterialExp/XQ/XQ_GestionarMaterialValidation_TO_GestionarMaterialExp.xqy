xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/Material/gestionarMaterial";
(:: import schema at "../XSD/gestionarMaterial.xsd" ::)

declare variable $reason as xs:string external;

declare function local:XQ_GestionarMaterialValidation_TO_GestionarMaterialExp($reason as xs:string) as element() (:: schema-element(ns1:gestionarMaterialRsp) ::) {
    if (fn:contains($reason,"carreteRetazo")) then
                  <ns1:gestionarMaterialRsp>
                      <ns1:errorCodigo>V001</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                      <ns1:errorDescripcion>Marca si el materia es Cable.</ns1:errorDescripcion>    
                  </ns1:gestionarMaterialRsp>
           else  if (fn:contains($reason,"codigoFlex")) then
                <ns1:gestionarMaterialRsp>
                      <ns1:errorCodigo>V002</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>Código del Articulo/Producto/Material.</ns1:errorDescripcion>    
                  </ns1:gestionarMaterialRsp>
          else  if (fn:contains($reason,"descripcion")) then
                <ns1:gestionarMaterialRsp>
                      <ns1:errorCodigo>V003</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>Nombre del Articulo.</ns1:errorDescripcion>    
                  </ns1:gestionarMaterialRsp> 
          else  if (fn:contains($reason,"estado")) then
                <ns1:gestionarMaterialRsp>
                      <ns1:errorCodigo>V004</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>Estado enviado válido (‘ACTIVO’, ‘NOACT’).</ns1:errorDescripcion>    
                  </ns1:gestionarMaterialRsp>
                  
           else (
                <ns1:gestionarMaterialRsp>
                      <ns1:errorCodigo>V006</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>Error en los datos de entrada</ns1:errorDescripcion>    
                  </ns1:gestionarMaterialRsp>
             )
};

local:XQ_GestionarMaterialValidation_TO_GestionarMaterialExp($reason)