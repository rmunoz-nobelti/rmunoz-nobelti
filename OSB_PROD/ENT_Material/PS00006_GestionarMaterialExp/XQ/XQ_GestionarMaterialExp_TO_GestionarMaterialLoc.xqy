xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/Material/gestionarMaterial";
(:: import schema at "../XSD/gestionarMaterial.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/SP_PPUT_MATERIAL";
(:: import schema at "../../PS00006_GestionarMaterialLoc/JCA/Schemas/SP_PPUT_MATERIAL_sp.xsd" ::)

declare variable $input as element() (:: schema-element(ns1:gestionarMaterialReq) ::) external;

declare function local:XQ_GestionarMaterialExp_TO_GestionarMaterialLoc($input as element() (:: schema-element(ns1:gestionarMaterialReq) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:VCODIGOCATEGORIA>34</ns2:VCODIGOCATEGORIA>
        <ns2:VCODIGOSUBCATEGORIA>{fn:data($input/ns1:subCategoria)}</ns2:VCODIGOSUBCATEGORIA>
        <ns2:VCODIGOFLEX>{fn:data($input/ns1:codigoFlex)}</ns2:VCODIGOFLEX>
        <ns2:VDESCRIPCION>{fn:data($input/ns1:descripcion)}</ns2:VDESCRIPCION>
        <ns2:VESTADO>{fn:data($input/ns1:estado)}</ns2:VESTADO>
        <ns2:VCOSTOPROMEDIO>{fn:data($input/ns1:costoPromedio)}</ns2:VCOSTOPROMEDIO>
        <ns2:VCODIGOUNIDADMEDIDA>{fn:data($input/ns1:codigoUnidadMedida)}</ns2:VCODIGOUNIDADMEDIDA>
        <ns2:VCARRETERETAZO>{fn:data($input/ns1:carreteRetazo)}</ns2:VCARRETERETAZO>
    </ns2:InputParameters>
};

local:XQ_GestionarMaterialExp_TO_GestionarMaterialLoc($input)