xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://tempuri.org";
(:: import schema at "../WS/Schemas/XMLSchema_-255938052.xsd" ::)

declare namespace ent1 = "http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKey";

declare namespace ent = "http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKeyList";

declare variable $response as element() (:: schema-element(ns1:ACCsCancelProjectCreateResponse) ::) external;

declare function local:func($response as element() (:: schema-element(ns1:ACCsCancelProjectCreateResponse) ::)) as element() (:: schema-element(ns1:ACCsCancelProjectCreateResponse) ::) {
    <ns1:ACCsCancelProjectCreateResponse>
        <ent:EntityKeyList>
            <ent1:EntityKey>
                <ent1:KeyData>
                    <ent1:KeyField>
                        <ent1:Field>{fn:data($response/ent:EntityKeyList/ent1:EntityKey/ent1:KeyData/ent1:KeyField/ent1:Field)}</ent1:Field>
                        <ent1:Value>{fn:data($response/ent:EntityKeyList/ent1:EntityKey/ent1:KeyData/ent1:KeyField/ent1:Value)}</ent1:Value>
                    </ent1:KeyField>
                </ent1:KeyData>
            </ent1:EntityKey>
        </ent:EntityKeyList>
    </ns1:ACCsCancelProjectCreateResponse>
};

local:func($response)