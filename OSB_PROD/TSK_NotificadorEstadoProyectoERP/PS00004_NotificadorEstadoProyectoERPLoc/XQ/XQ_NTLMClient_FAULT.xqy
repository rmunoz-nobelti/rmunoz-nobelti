xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/AX/response";
(:: import schema at "../WS/Schemas/responseAX.xsd" ::)

declare variable $faultCode as xs:string external;
declare variable $faultString as xs:string external;

declare function local:func($faultCode as xs:string, 
                            $faultString as xs:string) 
                            as element() (:: schema-element(ns1:AXResponse) ::) {
    <ns1:AXResponse>
        <ns1:responseCode>{fn:data($faultCode)}</ns1:responseCode>
        <ns1:responseString>{fn:data($faultString)}</ns1:responseString>
    </ns1:AXResponse>
};

local:func($faultCode, $faultString)