xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.grupogtd.com/schema/AX/response";
(:: import schema at "../WS/Schemas/responseAX.xsd" ::)
declare namespace ns1="http://tempuri.org";
(:: import schema at "../WS/Schemas/XMLSchema_-255938052.xsd" ::)

declare namespace ent1 = "http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKey";

declare namespace ent = "http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKeyList";

declare variable $input as element() (:: schema-element(ns1:ACCsCancelProjectCreateResponse) ::) external;

declare function local:func($input as element() (:: schema-element(ns1:ACCsCancelProjectCreateResponse) ::)) as element() (:: schema-element(ns2:AXResponse) ::) {
    <ns2:AXResponse>
        <ns2:responseCode>{fn:string("0")}</ns2:responseCode>
    </ns2:AXResponse>
};

local:func($input)