xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace cli="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../WS/Schemas/NTLMClient.xsd" ::)
declare namespace ns1="http://tempuri.org";
(:: import schema at "../WS/Schemas/XMLSchema_-255938052.xsd" ::)

declare namespace acc = "http://schemas.microsoft.com/dynamics/2008/01/documents/ACCsCancelProject";

declare variable $imput as element() (:: schema-element(ns1:ACCsCancelProjectCreateRequest) ::) external;

declare function local:func($imput as element() (:: schema-element(ns1:ACCsCancelProjectCreateRequest) ::)) as element() (:: schema-element(cli:request) ::) {
    <cli:reqNTLM>
        <cli:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDCancelProject/xppservice.svc</cli:wsURL>
        <cli:host>axtest</cli:host>
        <cli:user>svsbus</cli:user>
        <cli:password>cachito</cli:password>
        <cli:soapAction>http://tempuri.org/ACCsCancelProject/create</cli:soapAction>
        <cli:xmlInput>{<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:tem="http://tempuri.org" xmlns:acc="http://schemas.microsoft.com/dynamics/2008/01/documents/ACCsCancelProject">
   <soapenv:Header>
      <dat:CallContext>
         <dat:Company>{fn:data($imput/acc:ACCsCancelProject/acc:SenderId)}</dat:Company>
         <dat:Language>ES</dat:Language>
      </dat:CallContext>
   </soapenv:Header>
   <soapenv:Body>
      <tem:ACCsCancelProjectCreateRequest>
         <acc:ACCsCancelProject>
            <acc:ACCancelProjectsTable_1 class="entity">
               <acc:CodService>{fn:data($imput/acc:ACCsCancelProject/acc:ACCancelProjectsTable_1/acc:CodService)}</acc:CodService>
               <acc:Date>{fn:data($imput/acc:ACCsCancelProject/acc:ACCancelProjectsTable_1/acc:Date)}</acc:Date>
               <acc:OrderJob>{fn:data($imput/acc:ACCsCancelProject/acc:ACCancelProjectsTable_1/acc:OrderJob)}</acc:OrderJob>
               <acc:ProjId>{fn:data($imput/acc:ACCsCancelProject/acc:ACCancelProjectsTable_1/acc:ProjId)}</acc:ProjId>
               <acc:FileNum>{fn:data($imput/acc:ACCsCancelProject/acc:ACCancelProjectsTable_1/acc:FileNum)}</acc:FileNum>
               <acc:Fill>{fn:data($imput/acc:ACCsCancelProject/acc:ACCancelProjectsTable_1/acc:Fill)}</acc:Fill>
               <acc:Status>{fn:data($imput/acc:ACCsCancelProject/acc:ACCancelProjectsTable_1/acc:Status)}</acc:Status>
               <acc:Updated>{fn:data($imput/acc:ACCsCancelProject/acc:ACCancelProjectsTable_1/acc:Updated)}</acc:Updated>
            </acc:ACCancelProjectsTable_1>
         </acc:ACCsCancelProject>
      </tem:ACCsCancelProjectCreateRequest>
   </soapenv:Body>
</soapenv:Envelope>}</cli:xmlInput>
    </cli:reqNTLM>
};

local:func($imput)