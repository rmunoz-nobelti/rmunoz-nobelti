xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/NotificadorEstadoProyectoERP/notificarFichaAnulada";
(:: import schema at "../XSD/notificarFichaAnulada.xsd" ::)
declare namespace ns2="http://cl.grupogtd.com/schema/NotificadorEstadoProyectoERP/notificarFillCSAnulado";
(:: import schema at "../XSD/notificarFillCSAnulado.xsd" ::)

declare variable $output as element() (:: schema-element(ns1:notificarFichaAnuladaRsp) ::) external;

declare function local:XQ_notificarFichaAnulada_TO_notificarFillCSAnulado($output as element() (:: schema-element(ns1:notificarFichaAnuladaRsp) ::)) as element() (:: schema-element(ns2:notificarFillCSAnuladoRsp) ::) {
    <ns2:notificarFillCSAnuladoRsp>
        <ns2:errorCodigo>{fn:data($output/ns1:errorCodigo)}</ns2:errorCodigo>
        <ns2:errorTipo>{fn:data($output/ns1:errorTipo)}</ns2:errorTipo>
        <ns2:errorDescripcion>{fn:data($output/ns1:errorDescripcion)}</ns2:errorDescripcion>
    </ns2:notificarFillCSAnuladoRsp>
};

local:XQ_notificarFichaAnulada_TO_notificarFillCSAnulado($output)