xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/AX/response";
(:: import schema at "../../PS00004_NotificadorEstadoProyectoERPLoc/WS/Schemas/responseAX.xsd" ::)
declare namespace ns2="http://cl.grupogtd.com/schema/NotificadorEstadoProyectoERP/notificarFichaAnulada";
(:: import schema at "../XSD/notificarFichaAnulada.xsd" ::)

declare variable $inputAX as element() (:: schema-element(ns1:AXResponse) ::) external;

declare function local:func($inputAX as element() (:: schema-element(ns1:AXResponse) ::)) as element() (:: schema-element(ns2:notificarFichaAnuladaRsp) ::) {
    <ns2:notificarFichaAnuladaRsp>
        <ns2:errorCodigo>{fn:data($inputAX/ns1:responseCode)}</ns2:errorCodigo>
        <ns2:errorDescripcion>{fn:data($inputAX/ns1:responseString)}</ns2:errorDescripcion>
    </ns2:notificarFichaAnuladaRsp>
};

local:func($inputAX)