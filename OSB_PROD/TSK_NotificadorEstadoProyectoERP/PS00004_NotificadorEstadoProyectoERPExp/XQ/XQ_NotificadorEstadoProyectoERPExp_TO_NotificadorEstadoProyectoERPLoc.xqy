xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/NotificadorEstadoProyectoERP/notificarFichaAnulada";
(:: import schema at "../XSD/notificarFichaAnulada.xsd" ::)
declare namespace ns2="http://tempuri.org";
(:: import schema at "../../PS00004_NotificadorEstadoProyectoERPLoc/WS/Schemas/XMLSchema_-255938052.xsd" ::)

declare namespace acc = "http://schemas.microsoft.com/dynamics/2008/01/documents/ACCsCancelProject";

declare variable $expInput as element() (:: schema-element(ns1:notificarFichaAnuladaReq) ::) external;

declare function local:func($expInput as element() (:: schema-element(ns1:notificarFichaAnuladaReq) ::)) as element() (:: schema-element(ns2:ACCsCancelProjectCreateRequest) ::) {
    <ns2:ACCsCancelProjectCreateRequest>
        <acc:ACCsCancelProject>
            <acc:ClearNilFieldsOnUpdate></acc:ClearNilFieldsOnUpdate>
            <acc:DocPurpose></acc:DocPurpose>
            <acc:SenderId>{fn:data($expInput/ns1:empresa)}</acc:SenderId>
            <acc:ACCancelProjectsTable_1 class="entity">
                <acc:_DocumentHash></acc:_DocumentHash>
                <acc:RecId></acc:RecId>
                <acc:RecVersion></acc:RecVersion>
                <acc:CodService>{fn:data($expInput/ns1:codServicio)}</acc:CodService>
                <acc:Date>{fn:data($expInput/ns1:fecha)}</acc:Date>
                <acc:FileNum>{fn:data($expInput/ns1:nroFicha)}</acc:FileNum>
                <acc:Fill>{fn:data($expInput/ns1:fiil)}</acc:Fill>
                <acc:OrderJob>{fn:data($expInput/ns1:codOrdenTrabajo)}</acc:OrderJob>
                <acc:ProjId>{fn:data($expInput/ns1:idProyecto)}</acc:ProjId>
                <acc:Status>{fn:data($expInput/ns1:estadoLegado)}</acc:Status>
                <acc:Updated>{fn:string("No")}</acc:Updated>
            </acc:ACCancelProjectsTable_1>
        </acc:ACCsCancelProject>
    </ns2:ACCsCancelProjectCreateRequest>
};

local:func($expInput)