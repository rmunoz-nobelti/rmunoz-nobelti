xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.grupogtd.com/schema/NotificadorEstadoProyectoERP/notificarFichaAnulada";
(:: import schema at "../XSD/notificarFichaAnulada.xsd" ::)
declare namespace ns1="http://cl.grupogtd.com/schema/NotificadorEstadoProyectoERP/notificarFillCSAnulado";
(:: import schema at "../XSD/notificarFillCSAnulado.xsd" ::)

declare variable $input as element() (:: schema-element(ns1:notificarFillCSAnuladoReq) ::) external;

declare function local:XQ_notificarFillCSAnulado_TO_notificarFichaAnulada($input as element() (:: schema-element(ns1:notificarFillCSAnuladoReq) ::)) as element() (:: schema-element(ns2:notificarFichaAnuladaReq) ::) {
    <ns2:notificarFichaAnuladaReq>
        <ns2:empresa>{fn:data($input/ns1:empresa)}</ns2:empresa>
        <ns2:fecha>{fn:data($input/ns1:fecha)}</ns2:fecha>
        <ns2:idProyecto>{fn:data($input/ns1:idProyecto)}</ns2:idProyecto>
        <ns2:fiil>{fn:data($input/ns1:fiil)}</ns2:fiil>
        <ns2:codServicio>{fn:data($input/ns1:codServicio)}</ns2:codServicio>
        <ns2:nroFicha>{fn:data($input/ns1:nroFicha)}</ns2:nroFicha>
        <ns2:codOrdenTrabajo>{fn:data($input/ns1:codOrdenTrabajo)}</ns2:codOrdenTrabajo>
        <ns2:estadoLegado>{fn:data($input/ns1:estadoLegado)}</ns2:estadoLegado></ns2:notificarFichaAnuladaReq>
};

local:XQ_notificarFillCSAnulado_TO_notificarFichaAnulada($input)