xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.grupogtd.com/schema/NotificadorEstadoProyectoERP/notificarFichaAnulada";
(:: import schema at "../XSD/notificarFichaAnulada.xsd" ::)
declare namespace ns1="http://tempuri.org";
(:: import schema at "../../PS00004_NotificadorEstadoProyectoERPLoc/WS/Schemas/XMLSchema_-255938052.xsd" ::)

declare namespace ent1 = "http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKey";

declare namespace ent = "http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKeyList";

declare variable $locResponse as element() (:: schema-element(ns1:ACCsCancelProjectCreateResponse) ::) external;

declare function local:func($locResponse as element() (:: schema-element(ns1:ACCsCancelProjectCreateResponse) ::)) as element() (:: schema-element(ns2:notificarFichaAnuladaRsp) ::) {
    <ns2:notificarFichaAnuladaRsp>
        <ns2:errorCodigo>{fn:string('NM')}</ns2:errorCodigo>
        <ns2:errorTipo>{fn:data($locResponse/ent:EntityKeyList/ent1:EntityKey/ent1:KeyData/ent1:KeyField/ent1:Value)}</ns2:errorTipo>
        <ns2:errorDescripcion>{fn:string('Sin Map')}</ns2:errorDescripcion>
    </ns2:notificarFichaAnuladaRsp>
};

local:func($locResponse)