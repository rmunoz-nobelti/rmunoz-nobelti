xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/NotificadorEstadoProyectoERP/notificarFichaAnulada";
(:: import schema at "../XSD/notificarFichaAnulada.xsd" ::)

declare variable $reason as xs:string external;

declare function local:XQ_NotificarFichaAnuladaError_TO_NotificarFichaAnuladaExp($reason as xs:string) as element() (:: schema-element(ns1:notificarFichaAnuladaRsp) ::) {
 if (fn:contains($reason,"nroFicha")) then
                  <ns1:notificarFichaAnuladaRsp>
                      <ns1:errorCodigo>V002</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                      <ns1:errorDescripcion>El campo nroFicha debe ser un entero y no puede ser nulo.</ns1:errorDescripcion>    
                  </ns1:notificarFichaAnuladaRsp>
                           
            else (
                <ns1:notificarFichaAnuladaRsp>
                     <ns1:errorCodigo>V001</ns1:errorCodigo>
                     <ns1:errorTipo>Validación</ns1:errorTipo>
                     <ns1:errorDescripcion>Se debe validar que el formato de la variable de entrada sea correcto.</ns1:errorDescripcion>    
                </ns1:notificarFichaAnuladaRsp>
            )
};

local:XQ_NotificarFichaAnuladaError_TO_NotificarFichaAnuladaExp($reason)