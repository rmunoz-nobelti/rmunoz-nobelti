xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/AX/response";
(:: import schema at "../../../UT_Common/Schemas/responseAX.xsd" ::)
declare namespace ns2="http://cl.grupogtd.com/schema/ProductoERP/gestionarProducto";
(:: import schema at "../XSD/gestionarProducto.xsd" ::)

declare variable $responseMessage as element() (:: schema-element(ns1:AXResponse) ::) external;

declare function local:XQ_GestionarProductoLoc_TO_GestionarProductoExp($responseMessage as element() (:: schema-element(ns1:AXResponse) ::)) as element() (:: schema-element(ns2:gestionarProductoRsp) ::) {
    <ns2:gestionarProductoRsp>
        <ns2:errorCodigo>{fn:data($responseMessage/ns1:responseCode)}</ns2:errorCodigo>
        <ns2:errorDescripcion>{fn:data($responseMessage/ns1:responseString)}</ns2:errorDescripcion>
    </ns2:gestionarProductoRsp>
};

local:XQ_GestionarProductoLoc_TO_GestionarProductoExp($responseMessage)