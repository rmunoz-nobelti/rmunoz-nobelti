xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ProductoERP/gestionarProducto";
(:: import schema at "../XSD/gestionarProducto.xsd" ::)
declare namespace ns2="http://schemas.microsoft.com/dynamics/2010/01/datacontracts";
(:: import schema at "../../PS00014_GestionarProductoLoc/WS/Schemas/XMLSchema_1106358685.xsd" ::)

declare variable $inputMessage as element() (:: schema-element(ns1:gestionarProductoReq) ::) external;

declare function local:XQ_GestionarProductoExp_TO_GestionarProductoLocHeader($inputMessage as element() (:: schema-element(ns1:gestionarProductoReq) ::)) as element() (:: schema-element(ns2:CallContext) ::) {
    <ns2:CallContext>
        <ns2:Company>{fn:data($inputMessage/ns1:empresa/text())}</ns2:Company>
    </ns2:CallContext>
};

local:XQ_GestionarProductoExp_TO_GestionarProductoLocHeader($inputMessage)