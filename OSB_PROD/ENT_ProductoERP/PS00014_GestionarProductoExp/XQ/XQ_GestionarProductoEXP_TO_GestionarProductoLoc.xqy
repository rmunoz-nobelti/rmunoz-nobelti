xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ProductoERP/gestionarProducto";
(:: import schema at "../XSD/gestionarProducto.xsd" ::)
declare namespace ns2="http://tempuri.org";
(:: import schema at "../../PS00014_GestionarProductoLoc/WS/Schemas/XMLSchema_2036091149.xsd" ::)

declare namespace dyn = "http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application";

declare variable $inputMessage as element() (:: schema-element(ns1:gestionarProductoReq) ::) external;

declare function local:XQ_GestionarProductoEXP_TO_GestionarProductoLoc($inputMessage as element() (:: schema-element(ns1:gestionarProductoReq) ::)) as element() (:: schema-element(ns2:GTDCustBillingCodeServiceCreateBillingCodeRequest) ::) {
    <ns2:GTDCustBillingCodeServiceCreateBillingCodeRequest>
        <ns2:_contract>
            <dyn:Amount>{fn:data($inputMessage/ns1:precioUnitario)}</dyn:Amount>
            <dyn:BillingClassification>{fn:data($inputMessage/ns1:tipoFacturacion)}</dyn:BillingClassification>
            <dyn:BillingCode>{fn:data($inputMessage/ns1:codigoProducto)}</dyn:BillingCode>
            <dyn:Currency>{fn:data($inputMessage/ns1:divisa)}</dyn:Currency>
            <dyn:Description>{fn:data($inputMessage/ns1:descripcion)}</dyn:Description>
            <dyn:GTDCustTransGroupDescription>{fn:data($inputMessage/ns1:descripcionGrupo)}</dyn:GTDCustTransGroupDescription>
            <dyn:GTDCustTransGroupId>{fn:data($inputMessage/ns1:codigoGrupo)}</dyn:GTDCustTransGroupId>
            <dyn:GTDCustTransSubGroupDescription>{fn:data($inputMessage/ns1:descripcionSubGrupo)}</dyn:GTDCustTransSubGroupDescription>
            <dyn:GTDCustTransSubGroupId>{fn:data($inputMessage/ns1:codigoSubGrupo)}</dyn:GTDCustTransSubGroupId>
            <dyn:LedgerDimension>{fn:data($inputMessage/ns1:tipoOrganizacion)}</dyn:LedgerDimension>
            <dyn:Percent>{fn:data($inputMessage/ns1:porcentage)}</dyn:Percent>
            {
              if (empty($inputMessage/ns1:importe)) then
                <dyn:RateFieldSelector>0</dyn:RateFieldSelector>
              else
                <dyn:RateFieldSelector>{fn:data($inputMessage/ns1:importe)}</dyn:RateFieldSelector>
            }            
            <dyn:TaxItemGroupHeading>{fn:data($inputMessage/ns1:grupoImpuesto)}</dyn:TaxItemGroupHeading>
            <dyn:TransOpenType>{fn:data($inputMessage/ns1:tipoCobro)}</dyn:TransOpenType>
            {
              if (empty($inputMessage/ns1:useDimensionsFromLine)) then
                <dyn:UseDimensionsFromLine>1</dyn:UseDimensionsFromLine>
              else
                <dyn:UseDimensionsFromLine>{fn:data($inputMessage/ns1:useDimensionsFromLine)}</dyn:UseDimensionsFromLine>
            }            
            {
              if (empty($inputMessage/ns1:useDimensionsFromLine)) then
                <dyn:UseFromBillingClass>1</dyn:UseFromBillingClass>
              else
                <dyn:UseFromBillingClass>{fn:data($inputMessage/ns1:claseFacturacion)}</dyn:UseFromBillingClass>
            }            
            <!--<dyn:UseScript>{fn:data($inputMessage/ns1:useScript)}</dyn:UseScript>-->
            <dyn:UseScript>0</dyn:UseScript>
            <dyn:ValidFrom>{fn:data($inputMessage/ns1:fechaCreacion)}</dyn:ValidFrom>
            {
                if ($inputMessage/ns1:fechaBaja)
                then <dyn:ValidTo>{fn:data($inputMessage/ns1:fechaBaja)}</dyn:ValidTo>
                else 
                  <dyn:ValidTo>{fn-bea:date-from-string-with-format('yyyy-MMM-dd', '2154-12-31')}</dyn:ValidTo>
            }
        </ns2:_contract>
    </ns2:GTDCustBillingCodeServiceCreateBillingCodeRequest>
};

local:XQ_GestionarProductoEXP_TO_GestionarProductoLoc($inputMessage)