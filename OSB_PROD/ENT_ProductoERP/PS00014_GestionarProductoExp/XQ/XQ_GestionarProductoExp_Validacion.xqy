xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ProductoERP/gestionarProducto";
(:: import schema at "../XSD/gestionarProducto.xsd" ::)
declare variable $reason as xs:string external;

declare function local:XQ_GestionarProductoExp_Validacion($reason as xs:string) as element() (:: schema-element(ns1:gestionarProductoRsp) ::) {
    if (fn:contains($reason,"empresa")) then
      <ns1:gestionarProductoRsp>
        <ns1:errorCodigo>V001</ns1:errorCodigo>
        <ns1:errorTipo>Validación</ns1:errorTipo>
        <ns1:errorDescripcion>Dato Empresa es inválido.</ns1:errorDescripcion>
      </ns1:gestionarProductoRsp>
    else if (fn:contains($reason,"fechaCreacion")) then
      <ns1:gestionarProductoRsp>
        <ns1:errorCodigo>V002</ns1:errorCodigo>
        <ns1:errorTipo>Validación</ns1:errorTipo>
        <ns1:errorDescripcion>Dato fechaCreacion es inválido.</ns1:errorDescripcion>
      </ns1:gestionarProductoRsp>
    else if (fn:contains($reason,"fechaBaja")) then
      <ns1:gestionarProductoRsp>
        <ns1:errorCodigo>V003</ns1:errorCodigo>
        <ns1:errorTipo>Validación</ns1:errorTipo>
        <ns1:errorDescripcion>Dato fechaBaja es inválido.</ns1:errorDescripcion>
      </ns1:gestionarProductoRsp>
    else (
      <ns1:gestionarProductoRsp>
        <ns1:errorCodigo>V004</ns1:errorCodigo>
        <ns1:errorTipo>Validación</ns1:errorTipo>
        <ns1:errorDescripcion>Error en los datos de entrada.</ns1:errorDescripcion>
      </ns1:gestionarProductoRsp>
    )  
    
};


local:XQ_GestionarProductoExp_Validacion($reason)