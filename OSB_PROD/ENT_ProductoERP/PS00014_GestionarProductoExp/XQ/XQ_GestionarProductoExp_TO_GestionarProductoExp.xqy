xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ProductoERP/gestionarProducto";
(:: import schema at "../XSD/gestionarProducto.xsd" ::)

declare variable $inputMessage as element() (:: schema-element(ns1:gestionarProductoReq) ::) external;

declare function local:XQ_GestionarProductoExp_TO_GestionarProductoExp($inputMessage as element() (:: schema-element(ns1:gestionarProductoReq) ::)) as element() (:: schema-element(ns1:gestionarProductoReq) ::) {
    <ns1:gestionarProductoReq>
        <ns1:empresa>{fn:data($inputMessage/ns1:empresa)}</ns1:empresa>
        <ns1:codigoProducto>{fn:data($inputMessage/ns1:codigoProducto)}</ns1:codigoProducto>
        <ns1:descripcion>{fn:data($inputMessage/ns1:descripcion)}</ns1:descripcion>
        <ns1:tipoOrganizacion>{fn:data($inputMessage/ns1:tipoOrganizacion)}</ns1:tipoOrganizacion>
        <ns1:porcentage>{fn:data($inputMessage/ns1:porcentage)}</ns1:porcentage>
        <ns1:divisa>{fn:data($inputMessage/ns1:divisa)}</ns1:divisa>
        <ns1:importe>{fn:data($inputMessage/ns1:importe)}</ns1:importe>
        <ns1:useDimensionsFromLine>{fn:data($inputMessage/ns1:useDimensionsFromLine)}</ns1:useDimensionsFromLine>
        <ns1:claseFacturacion>{fn:data($inputMessage/ns1:claseFacturacion)}</ns1:claseFacturacion>
        <ns1:useScript>{fn:data($inputMessage/ns1:useScript)}</ns1:useScript>
        <ns1:fechaCreacion>{fn:data($inputMessage/ns1:fechaCreacion)}</ns1:fechaCreacion>
        {
          if ($inputMessage/ns1:fechaBaja and string-length(string($inputMessage/ns1:fechaBaja)) > 0)
                then <ns1:fechaBaja>{fn:data($inputMessage/ns1:fechaBaja)}</ns1:fechaBaja>
                else 
                  <ns1:fechaBaja>{fn-bea:date-from-string-with-format('yyyy-MM-dd', '2154-12-31')}</ns1:fechaBaja>
        }
        <ns1:codigoGrupo>{fn:data($inputMessage/ns1:codigoGrupo)}</ns1:codigoGrupo>
        <ns1:descripcionGrupo>{fn:data($inputMessage/ns1:descripcionGrupo)}</ns1:descripcionGrupo>
        <ns1:codigoSubGrupo>{fn:data($inputMessage/ns1:codigoSubGrupo)}</ns1:codigoSubGrupo>
        <ns1:descripcionSubGrupo>{fn:data($inputMessage/ns1:descripcionSubGrupo)}</ns1:descripcionSubGrupo>
        <ns1:grupoImpuesto>{fn:data($inputMessage/ns1:grupoImpuesto)}</ns1:grupoImpuesto>
        <ns1:precioUnitario>{fn:data($inputMessage/ns1:precioUnitario)}</ns1:precioUnitario>
        <ns1:tipoFacturacion>{fn:data($inputMessage/ns1:tipoFacturacion)}</ns1:tipoFacturacion>
        <ns1:tipoCobro>{fn:data($inputMessage/ns1:tipoCobro)}</ns1:tipoCobro>
    </ns1:gestionarProductoReq>
};

local:XQ_GestionarProductoExp_TO_GestionarProductoExp($inputMessage)