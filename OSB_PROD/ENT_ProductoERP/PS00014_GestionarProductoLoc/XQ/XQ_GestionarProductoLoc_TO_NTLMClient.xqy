xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/WSDL/NTLMClientService.wsdl" ::)
declare namespace ns2="http://tempuri.org";
(:: import schema at "../WS/Schemas/XMLSchema_2036091149.xsd" ::)

declare namespace dyn = "http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application";

declare variable $inputMessage as element() (:: schema-element(ns2:GTDCustBillingCodeServiceCreateBillingCodeRequest) ::) external;
declare variable $company as xs:string external;
declare function local:XQ_GestionarProductoLoc_TO_NTLMClient($company as xs:string, $inputMessage as element() (:: schema-element(ns2:GTDCustBillingCodeServiceCreateBillingCodeRequest) ::)) as element() (:: schema-element(ns1:reqNTLM) ::) {
    <ns1:reqNTLM>
        <ns1:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDCustIntegrationServices/xppservice.svc</ns1:wsURL>
        <ns1:host>axtest</ns1:host>
        <ns1:user>svsbus</ns1:user>
        <ns1:password>cachito</ns1:password>
        <ns1:soapAction>http://tempuri.org/GTDCustBillingCodeService/createBillingCode</ns1:soapAction>
        
        <ns1:xmlInput>{<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:tem="http://tempuri.org" xmlns:dyn="http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application">
   <soapenv:Header>
      <dat:CallContext>
         <dat:Company>{fn:data($company)}</dat:Company>
      </dat:CallContext>
   </soapenv:Header>
   <soapenv:Body>
      <tem:GTDCustBillingCodeServiceCreateBillingCodeRequest>
         <tem:_contract>
            <dyn:Amount>{fn:data($inputMessage/ns2:_contract/dyn:Amount)}</dyn:Amount>
            <dyn:BillingClassification>{fn:data($inputMessage/ns2:_contract/dyn:BillingClassification)}</dyn:BillingClassification>
            <dyn:BillingCode>{fn:data($inputMessage/ns2:_contract/dyn:BillingCode)}</dyn:BillingCode>
            <dyn:Currency>{fn:data($inputMessage/ns2:_contract/dyn:Currency)}</dyn:Currency>
            <dyn:Description>{fn:data($inputMessage/ns2:_contract/dyn:Description)}</dyn:Description>
            <dyn:GTDCustTransGroupDescription>{fn:data($inputMessage/ns2:_contract/dyn:GTDCustTransGroupDescription)}</dyn:GTDCustTransGroupDescription>
            <dyn:GTDCustTransGroupId>{fn:data($inputMessage/ns2:_contract/dyn:GTDCustTransGroupId)}</dyn:GTDCustTransGroupId>
            <dyn:GTDCustTransSubGroupDescription>{fn:data($inputMessage/ns2:_contract/dyn:GTDCustTransSubGroupDescription)}</dyn:GTDCustTransSubGroupDescription>
            <dyn:GTDCustTransSubGroupId>{fn:data($inputMessage/ns2:_contract/dyn:GTDCustTransSubGroupId)}</dyn:GTDCustTransSubGroupId>
            <dyn:LedgerDimension>{fn:data($inputMessage/ns2:_contract/dyn:LedgerDimension)}</dyn:LedgerDimension>
            <dyn:Percent>{fn:data($inputMessage/ns2:_contract/dyn:Percent)}</dyn:Percent>
            <dyn:RateFieldSelector>{fn:data($inputMessage/ns2:_contract/dyn:RateFieldSelector)}</dyn:RateFieldSelector>
            <dyn:TaxItemGroupHeading>{fn:data($inputMessage/ns2:_contract/dyn:TaxItemGroupHeading)}</dyn:TaxItemGroupHeading>
            <dyn:TransOpenType>{fn:data($inputMessage/ns2:_contract/dyn:TransOpenType)}</dyn:TransOpenType>
            <dyn:UseDimensionsFromLine>{fn:data($inputMessage/ns2:_contract/dyn:UseDimensionsFromLine)}</dyn:UseDimensionsFromLine>
            <dyn:UseFromBillingClass>{fn:data($inputMessage/ns2:_contract/dyn:UseFromBillingClass)}</dyn:UseFromBillingClass>
            <dyn:UseScript>{fn:data($inputMessage/ns2:_contract/dyn:UseScript)}</dyn:UseScript>
            <dyn:ValidFrom>{fn:data($inputMessage/ns2:_contract/dyn:ValidFrom)}</dyn:ValidFrom>
            <dyn:ValidTo>{fn:data($inputMessage/ns2:_contract/dyn:ValidTo)}</dyn:ValidTo>
         </tem:_contract>
      </tem:GTDCustBillingCodeServiceCreateBillingCodeRequest>
   </soapenv:Body>
</soapenv:Envelope>}</ns1:xmlInput>
    </ns1:reqNTLM>
};

local:XQ_GestionarProductoLoc_TO_NTLMClient($company, $inputMessage)