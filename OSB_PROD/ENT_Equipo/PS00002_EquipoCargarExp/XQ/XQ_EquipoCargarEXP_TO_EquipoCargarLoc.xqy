xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/EquipoCargar/equipoCargarReqOp";
(:: import schema at "../XSD/equipoCargarReq.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/SP_PPUT_EQUIPOS_SERIE";
(:: import schema at "../../PS00002_EquipoCargarLoc/JCA/Schemas/SP_PPUT_EQUIPOS_SERIE_sp.xsd" ::)

declare variable $equipoCargar as element() (:: element(*, ns1:equipoCargarReq) ::) external;

declare function local:func($equipoCargar as element() (:: element(*, ns1:equipoCargarReq) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        {
            if ($equipoCargar/ns1:codigoEquipo)
            then <ns2:VCODIGOEQUIPO>{fn:data($equipoCargar/ns1:codigoEquipo)}</ns2:VCODIGOEQUIPO>
            else ()
        }
        {
            if ($equipoCargar/ns1:numeroSerie)
            then <ns2:VNUMEROSERIE>{fn:data($equipoCargar/ns1:numeroSerie)}</ns2:VNUMEROSERIE>
            else ()
        }
        {
            if ($equipoCargar/ns1:numeroOrdenCompra)
            then <ns2:VNUMEROORDENCOMPRA>{fn:data($equipoCargar/ns1:numeroOrdenCompra)}</ns2:VNUMEROORDENCOMPRA>
            else ()
        }
        {
            if ($equipoCargar/ns1:rutProveedor)
            then <ns2:VRUTPROVEEDOR>{fn:data($equipoCargar/ns1:rutProveedor)}</ns2:VRUTPROVEEDOR>
            else ()
        }
        {
            if ($equipoCargar/ns1:digitador)
            then <ns2:VDIGITADOR>{fn:data($equipoCargar/ns1:digitador)}</ns2:VDIGITADOR>
            else ()
        }
        {
            if ($equipoCargar/ns1:estado)
            then <ns2:VESTADO>{fn:data($equipoCargar/ns1:estado)}</ns2:VESTADO>
            else ()
        }
    </ns2:InputParameters>
};

local:func($equipoCargar)