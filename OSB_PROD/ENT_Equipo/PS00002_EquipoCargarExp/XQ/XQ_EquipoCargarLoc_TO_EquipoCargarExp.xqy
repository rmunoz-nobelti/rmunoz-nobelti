xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.grupogtd.com/wsdl/scm/Equipo";
(:: import schema at "../WSDL/EquipoCargar.wsdl" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/SP_PPUT_EQUIPOS_SERIE";
(:: import schema at "../../PS00002_EquipoCargarLoc/JCA/Schemas/SP_PPUT_EQUIPOS_SERIE_sp.xsd" ::)
declare namespace ns3="http://cl.grupogtd.com/schema/EquipoCargar/equipoCargarRspOp";
(:: import schema at "../XSD/equipoCargarRsp.xsd" ::)

declare variable $schema-element as element() (:: schema-element(ns1:OutputParameters) ::) external;

declare function local:func($schema-element as element() (:: schema-element(ns1:OutputParameters) ::)) as element() (:: schema-element(ns2:equipoCargarRsp) ::) {
      if (fn:number($schema-element/ns1:CODIGO_ERROR_OUT) = -2 ) then
          <ns2:equipoCargarRsp>
              <ns3:errorCodigo>V004</ns3:errorCodigo>
              <ns3:errorTipo>Validación</ns3:errorTipo>
              <ns3:errorDescripcion>Código de Equipo debe ser único</ns3:errorDescripcion>
          </ns2:equipoCargarRsp>
      else  if (fn:number($schema-element/ns1:CODIGO_ERROR_OUT) = -1 ) then
          <ns2:equipoCargarRsp>
              <ns3:errorCodigo>T001</ns3:errorCodigo>
              <ns3:errorTipo>Técnico</ns3:errorTipo>
              <ns3:errorDescripcion>404 backend no disponible</ns3:errorDescripcion>
          </ns2:equipoCargarRsp>
      else
          <ns2:equipoCargarRsp>
              <ns3:errorCodigo>{fn:data($schema-element/ns1:CODIGO_ERROR_OUT)}</ns3:errorCodigo>
              <ns3:errorTipo>Técnico</ns3:errorTipo>
              <ns3:errorDescripcion>{fn:data($schema-element/ns1:GLS_ERR_OUT)}</ns3:errorDescripcion>
          </ns2:equipoCargarRsp> 
};

local:func($schema-element)