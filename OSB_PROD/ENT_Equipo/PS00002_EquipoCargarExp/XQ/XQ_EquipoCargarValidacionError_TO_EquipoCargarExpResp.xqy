xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/EquipoCargar/equipoCargarRspOp";
(:: import schema at "../XSD/equipoCargarRsp.xsd" ::)
declare namespace ns2="http://cl.grupogtd.com/wsdl/scm/Equipo";
(:: import schema at "../WSDL/EquipoCargar.wsdl" ::)

declare variable $reason as xs:string external;

declare function local:func($reason as xs:string) as element() (:: schema-element(ns2:equipoCargarRsp) ::){
            if (fn:contains($reason,"numeroSerie")) then
                  <ns2:equipoCargarRsp>
                      <ns1:errorCodigo>V001</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                      <ns1:errorDescripcion>Largo del Número de Serie</ns1:errorDescripcion>    
                  </ns2:equipoCargarRsp>
            else  if (fn:contains($reason,"numeroOrdenCompra")) then
                <ns2:equipoCargarRsp>
                      <ns1:errorCodigo>V002</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>Largo del Número de la Orden de Compra</ns1:errorDescripcion>    
                  </ns2:equipoCargarRsp>
            else  if (fn:contains($reason,"rutProveedor")) then
                <ns2:equipoCargarRsp>
                     <ns1:errorCodigo>V003</ns1:errorCodigo>
                     <ns1:errorTipo>Validación</ns1:errorTipo>
                     <ns1:errorDescripcion>Rut debe venir con digito verificador y guion</ns1:errorDescripcion>    
                </ns2:equipoCargarRsp>                
            else ()
};

local:func($reason)