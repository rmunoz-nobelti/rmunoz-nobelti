xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/SP_PPUT_EQUIPOS_SERIE";
(:: import schema at "../../PS00002_EquipoCargarLoc/JCA/Schemas/SP_PPUT_EQUIPOS_SERIE_sp.xsd" ::)

declare variable $reason as xs:string external;

declare function local:func($reason as xs:string) as element() (:: schema-element(ns1:OutputParameters) ::) {
      
            if (fn:contains($reason,"java.sql.SQLException: ORA-20100")) then             
                 if (fn:contains($reason,"Codigo de Equipo")) then
                      <ns1:OutputParameters>
                         <ns1:CODIGO_ERROR_OUT>-2</ns1:CODIGO_ERROR_OUT>
                         <ns1:GLS_ERR_OUT>Código de Equipo debe ser único</ns1:GLS_ERR_OUT>
                       </ns1:OutputParameters>
                 else 
                      <ns1:OutputParameters>
                         <ns1:CODIGO_ERROR_OUT>-1</ns1:CODIGO_ERROR_OUT>
                         <ns1:GLS_ERR_OUT>{fn:data($reason)}</ns1:GLS_ERR_OUT>
                       </ns1:OutputParameters>
            else 
                  <ns1:OutputParameters>
                     <ns1:CODIGO_ERROR_OUT>-1</ns1:CODIGO_ERROR_OUT>
                     <ns1:GLS_ERR_OUT>{fn:data($reason)}</ns1:GLS_ERR_OUT>
                   </ns1:OutputParameters>
       
        
    
};

local:func($reason)