xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/AX/response";
(:: import schema at "../../../UT_Common/Schemas/responseAX.xsd" ::)
declare namespace ns2="http://cl.grupogtd.com/schema/ClienteERP/gestionarCliente";
(:: import schema at "../XSD/gestionarCliente.xsd" ::)

declare variable $responseMessage as element() (:: schema-element(ns1:AXResponse) ::) external;

declare function local:XQ_GestionarClienteLoc_TO_GestionarClienteExp($responseMessage as element() (:: schema-element(ns1:AXResponse) ::)) as element() (:: schema-element(ns2:gestionarClienteRsp) ::) {
    <ns2:gestionarClienteRsp>
        <ns2:errorCodigo>{fn:data($responseMessage/ns1:responseCode)}</ns2:errorCodigo>
        <ns2:errorDescripcion>{fn:data($responseMessage/ns1:responseString)}</ns2:errorDescripcion>
    </ns2:gestionarClienteRsp>
};

local:XQ_GestionarClienteLoc_TO_GestionarClienteExp($responseMessage)