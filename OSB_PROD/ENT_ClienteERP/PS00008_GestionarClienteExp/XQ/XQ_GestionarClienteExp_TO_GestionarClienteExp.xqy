xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ClienteERP/gestionarCliente";
(:: import schema at "../XSD/gestionarCliente.xsd" ::)

declare variable $inputMessage as element() (:: schema-element(ns1:gestionarClienteReq) ::) external;

declare function local:XQ_GestionarClienteExp_TO_GestionarClienteExp($inputMessage as element() (:: schema-element(ns1:gestionarClienteReq) ::)) as element() (:: schema-element(ns1:gestionarClienteReq) ::) {
    <ns1:gestionarClienteReq>
        <ns1:RET_CUR_CLI>
            <ns1:estadoCliente>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:estadoCliente)}</ns1:estadoCliente>
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:cadenaEmpresa)
                then <ns1:cadenaEmpresa>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:cadenaEmpresa)}</ns1:cadenaEmpresa>
                else ()
            }
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:moneda)
                then <ns1:moneda>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:moneda)}</ns1:moneda>
                else ()
            }
            <ns1:cuentaCliente>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:cuentaCliente)}</ns1:cuentaCliente>
            <ns1:grupoCliente>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:grupoCliente)}</ns1:grupoCliente>
            <ns1:empresa>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:empresa)}</ns1:empresa>
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:empresaRelacionada)
                then <ns1:empresaRelacionada>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:empresaRelacionada)}</ns1:empresaRelacionada>
                else ()
            }
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:segmentoCliente)
                then <ns1:segmentoCliente>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:segmentoCliente)}</ns1:segmentoCliente>
                else ()
            }
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:condicionEntrega)
                then <ns1:condicionEntrega>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:condicionEntrega)}</ns1:condicionEntrega>
                else ()
            }
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:rutCuentaFactura)
                then <ns1:rutCuentaFactura>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:rutCuentaFactura)}</ns1:rutCuentaFactura>
                else ()
            }
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:idioma)
                then <ns1:idioma>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:idioma)}</ns1:idioma>
                else ()
            }
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:descripcionGiro)
                then <ns1:descripcionGiro>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:descripcionGiro)}</ns1:descripcionGiro>
                else ()
            }
            {
                
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:lineaNegocioCliente and dvmtr:lookup('ENT_ClienteERP/PS00008_GestionarClienteExp/DVM/GIRO', 'Key', $inputMessage/ns1:RET_CUR_CLI/ns1:lineaNegocioCliente/text(), 'Value', '-1') != '-1')
                then <ns1:lineaNegocioCliente>{dvmtr:lookup('ENT_ClienteERP/PS00008_GestionarClienteExp/DVM/GIRO', 'Key', $inputMessage/ns1:RET_CUR_CLI/ns1:lineaNegocioCliente/text(), 'Value', '-1')}</ns1:lineaNegocioCliente>
                else if (dvmtr:lookup('ENT_ClienteERP/PS00008_GestionarClienteExp/DVM/GIRO', 'Key', $inputMessage/ns1:RET_CUR_CLI/ns1:lineaNegocioCliente/text(), 'Value', '-1') = '-1')
                then <ns1:lineaNegocioCliente>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:lineaNegocioCliente)}</ns1:lineaNegocioCliente>
                else ()
            }
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:nombre)
                then <ns1:nombre>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:nombre)}</ns1:nombre>
                else ()
            }
            <ns1:tipoCliente>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:tipoCliente)}</ns1:tipoCliente>
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:condicionPago)
                then <ns1:condicionPago>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:condicionPago)}</ns1:condicionPago>
                else ()
            }
            <ns1:rutCliente>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:rutCliente)}</ns1:rutCliente>
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:segmento)
                then <ns1:segmento>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:segmento)}</ns1:segmento>
                else ()
            }
        </ns1:RET_CUR_CLI>
        {
            for $RET_CUR_DIR in $inputMessage/ns1:RET_CUR_DIR
            return 
            <ns1:RET_CUR_DIR>
                {
                    if ($RET_CUR_DIR/ns1:complementoDireccion)
                    then <ns1:complementoDireccion>{fn:data($RET_CUR_DIR/ns1:complementoDireccion)}</ns1:complementoDireccion>
                    else ()
                }
                <ns1:comuna>{xs:int(dvmtr:lookup('ENT_ClienteERP/PS00008_GestionarClienteExp/DVM/COMUNA', 'Key', $RET_CUR_DIR/ns1:comuna/text(), 'Value', '0'))}</ns1:comuna>                 
                <ns1:codigoCallejero>{fn:data($RET_CUR_DIR/ns1:codigoCallejero)}</ns1:codigoCallejero>
                <ns1:codigoDependencia>{fn:data($RET_CUR_DIR/ns1:codigoDependencia)}</ns1:codigoDependencia>
                {
                    if ($RET_CUR_DIR/ns1:codigoPostal)
                    then <ns1:codigoPostal>{fn:data($RET_CUR_DIR/ns1:codigoPostal)}</ns1:codigoPostal>
                    else ()
                }
                <ns1:pais>{fn:data($RET_CUR_DIR/ns1:pais)}</ns1:pais>
                {
                    if ($RET_CUR_DIR/ns1:provincia)
                    then <ns1:provincia>{fn:data($RET_CUR_DIR/ns1:provincia)}</ns1:provincia>
                    else ()
                }
                {
                    if ($RET_CUR_DIR/ns1:descripcion)
                    then <ns1:descripcion>{fn:data($RET_CUR_DIR/ns1:descripcion)}</ns1:descripcion>
                    else ()
                }
                <ns1:principal>{fn:data($RET_CUR_DIR/ns1:principal)}</ns1:principal>
                <ns1:proposito>{fn:data($RET_CUR_DIR/ns1:proposito)}</ns1:proposito>                
                {
                    if ($RET_CUR_DIR/ns1:calle)
                    then <ns1:calle>{fn:data($RET_CUR_DIR/ns1:calle)}</ns1:calle>
                    else ()
                }
                {
                    if ($RET_CUR_DIR/ns1:numeroCalle)
                    then <ns1:numeroCalle>{fn:data($RET_CUR_DIR/ns1:numeroCalle)}</ns1:numeroCalle>
                    else ()
                }
            </ns1:RET_CUR_DIR>
        }
    </ns1:gestionarClienteReq>
};

local:XQ_GestionarClienteExp_TO_GestionarClienteExp($inputMessage)