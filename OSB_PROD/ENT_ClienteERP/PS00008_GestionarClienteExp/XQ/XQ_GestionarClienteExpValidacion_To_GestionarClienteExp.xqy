xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ClienteERP/gestionarCliente";
(:: import schema at "../XSD/gestionarCliente.xsd" ::)

declare variable $reason as xs:string external;

declare function local:XQ_GestionarClienteExpValidacion_To_GestionarClienteExp($reason as xs:string) as element() (:: element(*, ns1:gestionarClienteRspType) ::) {   
    
     if (fn:contains($reason,"rutCliente")) then
        <ns1:gestionarClienteRspType>
          <ns1:errorCodigo>V101</ns1:errorCodigo>
          <ns1:errorTipo>Validación</ns1:errorTipo>
          <ns1:errorDescripcion>Formato incorrecto para el dato rut.</ns1:errorDescripcion>
        </ns1:gestionarClienteRspType>
    else if (fn:contains($reason,"tipoCliente")) then
        <ns1:gestionarClienteRspType>
          <ns1:errorCodigo>V102</ns1:errorCodigo>
          <ns1:errorTipo>Validación</ns1:errorTipo>
          <ns1:errorDescripcion>Dato no válido para tipoCliente.</ns1:errorDescripcion>
        </ns1:gestionarClienteRspType>
    else if (fn:contains($reason,"grupoCliente")) then
        <ns1:gestionarClienteRspType>
          <ns1:errorCodigo>V103</ns1:errorCodigo>
          <ns1:errorTipo>Validación</ns1:errorTipo>
          <ns1:errorDescripcion>Dato no válido para grupoCliente.</ns1:errorDescripcion>
        </ns1:gestionarClienteRspType>
    else if (fn:contains($reason,"descripcion")) then
        <ns1:gestionarClienteRspType>
          <ns1:errorCodigo>V104</ns1:errorCodigo>
          <ns1:errorTipo>Validación</ns1:errorTipo>
          <ns1:errorDescripcion>Dato no válido para descripción.</ns1:errorDescripcion>
        </ns1:gestionarClienteRspType>
    else
     if (fn:contains($reason,"segmentoCliente")) then
        <ns1:gestionarClienteRspType>
          <ns1:errorCodigo>V105</ns1:errorCodigo>
          <ns1:errorTipo>Validación</ns1:errorTipo>
          <ns1:errorDescripcion>Dato no válido para segmentoCliente.</ns1:errorDescripcion>
        </ns1:gestionarClienteRspType>
    else
      <ns1:gestionarClienteRspType>
          <ns1:errorCodigo>V106</ns1:errorCodigo>
          <ns1:errorTipo>Validación</ns1:errorTipo>
          <ns1:errorDescripcion>Error en los datos de entrada.</ns1:errorDescripcion>
        </ns1:gestionarClienteRspType>
};

local:XQ_GestionarClienteExpValidacion_To_GestionarClienteExp($reason)