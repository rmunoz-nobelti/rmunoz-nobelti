xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ClienteERP/gestionarCliente";
(:: import schema at "../XSD/gestionarCliente.xsd" ::)
declare namespace ns2="http://tempuri.org";
(:: import schema at "../../PS00008_GestionarClienteLoc/WS/Schemas/XMLSchema_2036091149.xsd" ::)

declare namespace dyn = "http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application";

declare variable $inputMessage as element() (:: schema-element(ns1:gestionarClienteReq) ::) external;

declare function local:XQ_GestionarClienteExp_TO_GestionarClienteLoc($inputMessage as element() (:: schema-element(ns1:gestionarClienteReq) ::)) as element() (:: schema-element(ns2:GTDCustCreateCustomerServiceCreateorUpdateCustomerRequest) ::) {
    <ns2:GTDCustCreateCustomerServiceCreateorUpdateCustomerRequest>
        <ns2:_contract>
            <dyn:AddressLines>
                {
                    for $RET_CUR_DIR in $inputMessage/ns1:RET_CUR_DIR
                    return 
                    <dyn:GTDCustCreatePostalAddressContract>
                        {
                            if ($RET_CUR_DIR/ns1:complementoDireccion)
                            then <dyn:parmBuildingCompliment>{fn:data($RET_CUR_DIR/ns1:complementoDireccion)}</dyn:parmBuildingCompliment>
                            else ()
                        }
                        <dyn:parmCity>{fn:data($RET_CUR_DIR/ns1:comuna)}</dyn:parmCity>
                        <dyn:parmCodCallejero>{fn:data($RET_CUR_DIR/ns1:codigoCallejero)}</dyn:parmCodCallejero>
                        <dyn:parmCodDependenciaRecId>{fn:data($RET_CUR_DIR/ns1:codigoDependencia)}</dyn:parmCodDependenciaRecId>
                        {
                            if ($RET_CUR_DIR/ns1:codigoPostal)
                            then <dyn:parmCodPostal>{fn:data($RET_CUR_DIR/ns1:codigoPostal)}</dyn:parmCodPostal>
                            else ()
                        }
                        <dyn:parmCountryRegionId>{fn:data($RET_CUR_DIR/ns1:pais)}</dyn:parmCountryRegionId>
                        {
                            if ($RET_CUR_DIR/ns1:provincia)
                            then <dyn:parmCounty>{fn:data($RET_CUR_DIR/ns1:provincia)}</dyn:parmCounty>
                            else ()
                        }
                        {
                            if ($RET_CUR_DIR/ns1:descripcion)
                            then <dyn:parmDescription>{fn:data($RET_CUR_DIR/ns1:descripcion)}</dyn:parmDescription>
                            else ()
                        }
                        <dyn:parmIsPrimary>{fn:data($RET_CUR_DIR/ns1:principal)}</dyn:parmIsPrimary>
                        <dyn:parmRole>{fn:data($RET_CUR_DIR/ns1:proposito)}</dyn:parmRole>
                        {
                            if ($RET_CUR_DIR/ns1:calle)
                            then <dyn:parmStreet>{fn:data($RET_CUR_DIR/ns1:calle)}</dyn:parmStreet>
                            else ()
                        }
                        {
                            if ($RET_CUR_DIR/ns1:numeroCalle)
                            then <dyn:parmStreetNum>{fn:data($RET_CUR_DIR/ns1:numeroCalle)}</dyn:parmStreetNum>
                            else ()
                        }</dyn:GTDCustCreatePostalAddressContract>
                }
            </dyn:AddressLines>
            <dyn:Blocked>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:estadoCliente)}</dyn:Blocked>
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:cadenaEmpresa)
                then <dyn:CompanyChain>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:cadenaEmpresa)}</dyn:CompanyChain>
                else ()
            }
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:moneda)
                then <dyn:Currency>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:moneda)}</dyn:Currency>
                else <dyn:Currency>{'CLP'}</dyn:Currency>
            }
            <dyn:CustAccount>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:cuentaCliente)}</dyn:CustAccount>
            <dyn:CustGroup>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:grupoCliente)}</dyn:CustGroup>
            <dyn:DataArea>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:empresa)}</dyn:DataArea>
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:empresaRelacionada)
                then <dyn:Dimension_EmpresaRelacionada>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:empresaRelacionada)}</dyn:Dimension_EmpresaRelacionada>
                else ()
            }
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:segmentoCliente)
                then <dyn:Dimension_SegmentoCliente>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:segmentoCliente)}</dyn:Dimension_SegmentoCliente>
                else ()
            }
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:condicionEntrega)
                then <dyn:DlvTerm>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:condicionEntrega)}</dyn:DlvTerm>
                else <dyn:DlvTerm>{'PROPIA'}</dyn:DlvTerm>
            }
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:rutCuentaFactura)
                then <dyn:InvoiceAccount>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:rutCuentaFactura)}</dyn:InvoiceAccount>
                else ()
            }
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:idioma)
                then <dyn:LanguageId>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:idioma)}</dyn:LanguageId>
                else <dyn:LanguageId>{'Es'}</dyn:LanguageId>
            }
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:descripcionGiro)
                then <dyn:LineOfBusinessDescription>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:descripcionGiro)}</dyn:LineOfBusinessDescription>
                else ()
            }
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:lineaNegocioCliente)
                then <dyn:LineOfBusinessId>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:lineaNegocioCliente)}</dyn:LineOfBusinessId>
                else ()
            }
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:nombre)
                then <dyn:Name>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:nombre)}</dyn:Name>
                else ()
            }
            <dyn:PartyType>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:tipoCliente)}</dyn:PartyType>
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:condicionPago)
                then <dyn:PaymTerm>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:condicionPago)}</dyn:PaymTerm>
                else ()
            }
            <dyn:VATNum>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:rutCliente)}</dyn:VATNum>
            {
                if ($inputMessage/ns1:RET_CUR_CLI/ns1:segmento)
                then <dyn:parmSegmentId>{fn:data($inputMessage/ns1:RET_CUR_CLI/ns1:segmento)}</dyn:parmSegmentId>
                else ()
            }
        </ns2:_contract>
    </ns2:GTDCustCreateCustomerServiceCreateorUpdateCustomerRequest>
};

local:XQ_GestionarClienteExp_TO_GestionarClienteLoc($inputMessage)