xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace cli="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/XSD/NTLMClient.xsd" ::)
declare namespace ns1="http://tempuri.org";
(:: import schema at "../WS/Schemas/XMLSchema_2036091149.xsd" ::)

declare namespace dyn = "http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application";

declare variable $inputMessage as element() (:: schema-element(ns1:GTDCustCreateCustomerServiceCreateorUpdateCustomerRequest) ::) external;

declare function local:XQ_GestionarClienteLoc_TO_NTLMClient($inputMessage as element() (:: schema-element(ns1:GTDCustCreateCustomerServiceCreateorUpdateCustomerRequest) ::)) as element() (:: schema-element(cli:request) ::) {
<cli:reqNTLM>
        <cli:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDCustIntegrationServices/xppservice.svc</cli:wsURL>
        <cli:host>axtest</cli:host>
        <cli:user>svsbus</cli:user>
        <cli:password>cachito</cli:password>
        <cli:soapAction>http://tempuri.org/GTDCustCreateCustomerService/createorUpdateCustomer</cli:soapAction>
        <cli:xmlInput>{
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:tem="http://tempuri.org" xmlns:dyn="http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application">

   <soapenv:Header>
      <dat:CallContext>   
      </dat:CallContext>
   </soapenv:Header>

   <soapenv:Body>
      <tem:GTDCustCreateCustomerServiceCreateorUpdateCustomerRequest>
         <tem:_contract>
            <dyn:AddressLines>
               {
               for $ADDRESS in $inputMessage/ns1:_contract/dyn:AddressLines/dyn:GTDCustCreatePostalAddressContract
                    return 
               <dyn:GTDCustCreatePostalAddressContract>
                  {
                    if ($ADDRESS/dyn:parmBuildingCompliment)
                    then <dyn:parmBuildingCompliment>{fn:data($ADDRESS/dyn:parmBuildingCompliment)}</dyn:parmBuildingCompliment>
                    else ()
                  }
                  {
                    if ($ADDRESS/dyn:parmCity)
                    then <dyn:parmCity>{fn:data($ADDRESS/dyn:parmCity)}</dyn:parmCity>
                    else ()
                  }
                  {
                    if ($ADDRESS/dyn:parmCodCallejero)
                    then <dyn:parmCodCallejero>{fn:data($ADDRESS/dyn:parmCodCallejero)}</dyn:parmCodCallejero>
                    else ()
                  }
                  {
                    if ($ADDRESS/dyn:parmCodDependenciaRecId)
                    then <dyn:parmCodDependenciaRecId>{fn:data($ADDRESS/dyn:parmCodDependenciaRecId)}</dyn:parmCodDependenciaRecId>
                    else ()
                  }
                  {
                    if ($ADDRESS/dyn:parmCodPostal)
                    then <dyn:parmCodPostal>{fn:data($ADDRESS/dyn:parmCodPostal)}</dyn:parmCodPostal>
                    else ()
                  }
                  {
                    if ($ADDRESS/dyn:parmCountryRegionId)
                    then <dyn:parmCountryRegionId>{fn:data($ADDRESS/dyn:parmCountryRegionId)}</dyn:parmCountryRegionId>
                    else ()
                  }
                  {
                    if ($ADDRESS/dyn:parmCounty)
                    then <dyn:parmCounty>{fn:data($ADDRESS/dyn:parmCounty)}</dyn:parmCounty>
                    else ()
                  }
                  {
                    if ($ADDRESS/dyn:parmDescription)
                    then <dyn:parmDescription>{fn:data($ADDRESS/dyn:parmDescription)}</dyn:parmDescription>
                    else ()
                  }
                  {
                    if ($ADDRESS/dyn:parmIsPrimary)
                    then <dyn:parmIsPrimary>{fn:data($ADDRESS/dyn:parmIsPrimary)}</dyn:parmIsPrimary>
                    else ()
                  }
                  {
                    if ($ADDRESS/dyn:parmRole)
                    then <dyn:parmRole>{fn:data($ADDRESS/dyn:parmRole)}</dyn:parmRole>
                    else ()
                  }
                  {
                    if ($ADDRESS/dyn:parmStreet)
                    then <dyn:parmStreet>{fn:data($ADDRESS/dyn:parmStreet)}</dyn:parmStreet>
                    else ()
                  }
                  {
                    if ($ADDRESS/dyn:parmStreetNum)
                    then <dyn:parmStreetNum>{fn:data($ADDRESS/dyn:parmStreetNum)}</dyn:parmStreetNum>
                    else ()
                  }
               </dyn:GTDCustCreatePostalAddressContract>
                }
            </dyn:AddressLines>
            {
              if ($inputMessage/ns1:_contract/dyn:Blocked)
              then <dyn:Blocked>{fn:data($inputMessage/ns1:_contract/dyn:Blocked)}</dyn:Blocked>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:CompanyChain)
              then <dyn:CompanyChain>{fn:data($inputMessage/ns1:_contract/dyn:CompanyChain)}</dyn:CompanyChain>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:Currency)
              then <dyn:Currency>{fn:data($inputMessage/ns1:_contract/dyn:Currency)}</dyn:Currency>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:CustAccount)
              then <dyn:CustAccount>{fn:data($inputMessage/ns1:_contract/dyn:CustAccount)}</dyn:CustAccount>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:CustGroup)
              then <dyn:CustGroup>{fn:data($inputMessage/ns1:_contract/dyn:CustGroup)}</dyn:CustGroup>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:DataArea)
              then <dyn:DataArea>{fn:data($inputMessage/ns1:_contract/dyn:DataArea)}</dyn:DataArea>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:Dimension_EmpresaRelacionada)
              then <dyn:Dimension_EmpresaRelacionada>{fn:data($inputMessage/ns1:_contract/dyn:Dimension_EmpresaRelacionada)}</dyn:Dimension_EmpresaRelacionada>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:Dimension_SegmentoCliente)
              then <dyn:Dimension_SegmentoCliente>{fn:data($inputMessage/ns1:_contract/dyn:Dimension_SegmentoCliente)}</dyn:Dimension_SegmentoCliente>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:DlvTerm)
              then <dyn:DlvTerm>{fn:data($inputMessage/ns1:_contract/dyn:DlvTerm)}</dyn:DlvTerm>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:InvoiceAccount)
              then <dyn:InvoiceAccount>{fn:data($inputMessage/ns1:_contract/dyn:InvoiceAccount)}</dyn:InvoiceAccount>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:LanguageId)
              then <dyn:LanguageId>{fn:data($inputMessage/ns1:_contract/dyn:LanguageId)}</dyn:LanguageId>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:LineOfBusinessDescription)
              then <dyn:LineOfBusinessDescription>{fn:data($inputMessage/ns1:_contract/dyn:LineOfBusinessDescription)}</dyn:LineOfBusinessDescription>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:LineOfBusinessId)
              then <dyn:LineOfBusinessId>{fn:data($inputMessage/ns1:_contract/dyn:LineOfBusinessId)}</dyn:LineOfBusinessId>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:Name)
              then <dyn:Name>{fn:data($inputMessage/ns1:_contract/dyn:Name)}</dyn:Name>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:PartyType)
              then <dyn:PartyType>{fn:data($inputMessage/ns1:_contract/dyn:PartyType)}</dyn:PartyType>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:PaymTerm)
              then <dyn:PaymTerm>{fn:data($inputMessage/ns1:_contract/dyn:PaymTerm)}</dyn:PaymTerm>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:VATNum)
              then <dyn:VATNum>{fn:data($inputMessage/ns1:_contract/dyn:VATNum)}</dyn:VATNum>
              else ()
            }
            {
              if ($inputMessage/ns1:_contract/dyn:parmSegmentId)
              then <dyn:parmSegmentId>{fn:data($inputMessage/ns1:_contract/dyn:parmSegmentId)}</dyn:parmSegmentId>
              else ()
            }
         </tem:_contract>
      </tem:GTDCustCreateCustomerServiceCreateorUpdateCustomerRequest>
   </soapenv:Body>
</soapenv:Envelope>
}</cli:xmlInput>
</cli:reqNTLM>
};

local:XQ_GestionarClienteLoc_TO_NTLMClient($inputMessage)