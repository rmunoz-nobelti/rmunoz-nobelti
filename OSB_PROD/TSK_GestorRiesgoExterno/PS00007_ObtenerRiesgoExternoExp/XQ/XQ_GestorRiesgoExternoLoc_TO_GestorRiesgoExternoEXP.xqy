xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/wsdl/scm/GestorRiesgoExterno";
(:: import schema at "../WSDL/GestorRiesgoExterno.wsdl" ::)
declare namespace ns2="http://cl.grupogtd.com/schema/RiesgoExterno/obtenerRiesgoExterno";
(:: import schema at "../../PS00007_ObtenerRiesgoExternoLoc/JCA/Schemas/obtenerRiesgoExterno.xsd" ::)

declare namespace obt = "http://cl.grupogtd.com/schema/GestorRiesgoExterno/obtenerRiesgoExterno";

declare variable $riesgoExternoObtenerResponse as element() (:: schema-element(ns2:riesgoExternoObtenerResponse) ::) external;

declare function local:func($riesgoExternoObtenerResponse as element() (:: schema-element(ns2:riesgoExternoObtenerResponse) ::)) as element() (:: schema-element(ns1:obtenerRiesgoExternoRsp) ::) {
    <ns1:obtenerRiesgoExternoRsp>
        {
            if ($riesgoExternoObtenerResponse/ns2:errorCodigo)
            then <obt:errorCodigo>{fn:data($riesgoExternoObtenerResponse/ns2:errorCodigo)}</obt:errorCodigo>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:errorTipo )
            then <obt:errorTipo>{fn:data($riesgoExternoObtenerResponse/ns2:errorTipo)}</obt:errorTipo>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:errorDescripcion)
            then <obt:errorDescripcion>{fn:data($riesgoExternoObtenerResponse/ns2:errorDescripcion)}</obt:errorDescripcion>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sIdConsulta and fn:string-length($riesgoExternoObtenerResponse/ns2:sIdConsulta/text()) > 0 )
            then <obt:sIdConsulta>{fn:data($riesgoExternoObtenerResponse/ns2:sIdConsulta)}</obt:sIdConsulta>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sIdTransaccion and fn:string-length($riesgoExternoObtenerResponse/ns2:sIdTransaccion/text()) > 0)
            then <obt:sIdTransaccion>{fn:data($riesgoExternoObtenerResponse/ns2:sIdTransaccion)}</obt:sIdTransaccion>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sRut and fn:string-length($riesgoExternoObtenerResponse/ns2:sRut/text()) > 0) 
            then <obt:sRut>{fn:data($riesgoExternoObtenerResponse/ns2:sRut)}</obt:sRut>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sNombre and fn:string-length($riesgoExternoObtenerResponse/ns2:sNombre/text()) > 0) 
            then <obt:sNombre>{fn:data($riesgoExternoObtenerResponse/ns2:sNombre)}</obt:sNombre>
            else ()
        }
        {
            if (not(empty($riesgoExternoObtenerResponse/ns2:sfechaConstitucion/text())) and not($riesgoExternoObtenerResponse/ns2:sfechaConstitucion/text() = "00000000"))
            then <obt:sfechaConstitucion>{ fn-bea:date-from-string-with-format("yyyyMMdd",string($riesgoExternoObtenerResponse/ns2:sfechaConstitucion))}</obt:sfechaConstitucion>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sISE and fn:string-length($riesgoExternoObtenerResponse/ns2:sISE/text()) > 0) 
            then <obt:sISE>{fn:data($riesgoExternoObtenerResponse/ns2:sISE)}</obt:sISE>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sScore  and fn:string-length($riesgoExternoObtenerResponse/ns2:sScore/text()) > 0) 
            then <obt:sScore>{fn:data($riesgoExternoObtenerResponse/ns2:sScore)}</obt:sScore>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sRentaPresunta and fn:string-length($riesgoExternoObtenerResponse/ns2:sRentaPresunta/text()) > 0) 
            then <obt:sRentaPresunta>{fn:data($riesgoExternoObtenerResponse/ns2:sRentaPresunta)}</obt:sRentaPresunta>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sBoletinConcursal and fn:string-length($riesgoExternoObtenerResponse/ns2:sBoletinConcursal/text()) > 0) 
            then <obt:sBoletinConcursal>{fn:data($riesgoExternoObtenerResponse/ns2:sBoletinConcursal)}</obt:sBoletinConcursal>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sTamaño  and fn:string-length($riesgoExternoObtenerResponse/ns2:sTamaño/text()) > 0) 
            then <obt:sTamaño>{fn:data($riesgoExternoObtenerResponse/ns2:sTamaño)}</obt:sTamaño>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sMonto_Facturacion  and fn:string-length($riesgoExternoObtenerResponse/ns2:sMonto_Facturacion/text()) > 0) 
            then <obt:sMonto_Facturacion>{fn:data($riesgoExternoObtenerResponse/ns2:sMonto_Facturacion)}</obt:sMonto_Facturacion>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sClasificadorEmpresa  and fn:string-length($riesgoExternoObtenerResponse/ns2:sClasificadorEmpresa/text()) > 0) 
            then <obt:sClasificadorEmpresa>{fn:data($riesgoExternoObtenerResponse/ns2:sClasificadorEmpresa)}</obt:sClasificadorEmpresa>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sBaseQuiebras  and fn:string-length($riesgoExternoObtenerResponse/ns2:sBaseQuiebras/text()) > 0) 
            then <obt:sBaseQuiebras>{fn:data($riesgoExternoObtenerResponse/ns2:sBaseQuiebras)}</obt:sBaseQuiebras>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sBolComNroDoc and fn:string-length($riesgoExternoObtenerResponse/ns2:sBolComNroDoc/text()) > 0) 
            then <obt:sBolComNroDoc>{fn:data($riesgoExternoObtenerResponse/ns2:sBolComNroDoc)}</obt:sBolComNroDoc>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sBolComMontoTotal and fn:string-length($riesgoExternoObtenerResponse/ns2:sBolComMontoTotal/text()) > 0) 
            then <obt:sBolComMontoTotal>{fn:data($riesgoExternoObtenerResponse/ns2:sBolComMontoTotal)}</obt:sBolComMontoTotal>
            else ()
        }
        {
            if (not(empty($riesgoExternoObtenerResponse/ns2:sBolComFecha/text())) and not($riesgoExternoObtenerResponse/ns2:sBolComFecha/text() = "00000000")) 
            then <obt:sBolComFecha>{fn-bea:date-from-string-with-format("yyyyMMdd",string($riesgoExternoObtenerResponse/ns2:sBolComFecha))}</obt:sBolComFecha>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sDicomNroDoc and fn:string-length($riesgoExternoObtenerResponse/ns2:sDicomNroDoc/text()) > 0) 
            then <obt:sDicomNroDoc>{fn:data($riesgoExternoObtenerResponse/ns2:sDicomNroDoc)}</obt:sDicomNroDoc>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sDicomMontoTotal and fn:string-length($riesgoExternoObtenerResponse/ns2:sDicomMontoTotal/text()) > 0) 
            then <obt:sDicomMontoTotal>{fn:data($riesgoExternoObtenerResponse/ns2:sDicomMontoTotal)}</obt:sDicomMontoTotal>
            else ()
        }
        {
            if (not(empty($riesgoExternoObtenerResponse/ns2:sDicomFecha/text() ) ) and not($riesgoExternoObtenerResponse/ns2:sDicomFecha/text() = "00000000"))
            then <obt:sDicomFecha>{fn-bea:date-from-string-with-format("yyyyMMdd", string($riesgoExternoObtenerResponse/ns2:sDicomFecha))}</obt:sDicomFecha>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sIcomNroDoc and fn:string-length($riesgoExternoObtenerResponse/ns2:sIcomNroDoc/text()) > 0) 
            then <obt:sIcomNroDoc>{fn:data($riesgoExternoObtenerResponse/ns2:sIcomNroDoc)}</obt:sIcomNroDoc>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sIcomMontoTotal and fn:string-length($riesgoExternoObtenerResponse/ns2:sIcomMontoTotal/text()) > 0) 
            then <obt:sIcomMontoTotal>{fn:data($riesgoExternoObtenerResponse/ns2:sIcomMontoTotal)}</obt:sIcomMontoTotal>
            else ()
        }
        {
            if (not(empty($riesgoExternoObtenerResponse/ns2:sIcomFecha/text())) and not($riesgoExternoObtenerResponse/ns2:sIcomFecha/text() = "00000000"))
            then <obt:sIcomFecha>{fn-bea:date-from-string-with-format("yyyyMMdd",string($riesgoExternoObtenerResponse/ns2:sIcomFecha))}</obt:sIcomFecha>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns2:sUsuario)
            then <obt:sUsuario>{fn:data($riesgoExternoObtenerResponse/ns2:sUsuario)}</obt:sUsuario>
            else ()
        }
        {
            if (not(empty($riesgoExternoObtenerResponse/ns2:sFechaCreacion/text())) and not($riesgoExternoObtenerResponse/ns2:sFechaCreacion/text() = "00000000"))
            then <obt:sFechaCreacion>{fn:data($riesgoExternoObtenerResponse/ns2:sFechaCreacion)}</obt:sFechaCreacion>
            else ()
        }   
    </ns1:obtenerRiesgoExternoRsp>
};

local:func($riesgoExternoObtenerResponse)