xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/RiesgoExterno/obtenerRiesgoExterno";
(:: import schema at "../../PS00007_ObtenerRiesgoExternoLoc/JCA/Schemas/obtenerRiesgoExterno.xsd" ::)
declare namespace ns2="http://cl.grupogtd.com/wsdl/scm/GestorRiesgoExterno";
(:: import schema at "../WSDL/GestorRiesgoExterno.wsdl" ::)

declare namespace rie = "http://cl.grupogtd.com/schema/RiesgoExterno/obtenerRiesgoExternoRspOp";

declare variable $riesgoExternoObtenerResponse as element() (:: schema-element(ns1:riesgoExternoObtenerResponse) ::) external;

declare function local:func($riesgoExternoObtenerResponse as element() (:: schema-element(ns1:riesgoExternoObtenerResponse) ::)) as element() (:: schema-element(ns2:obtenerRiesgoExternoRsp) ::) {
    <ns2:obtenerRiesgoExternoRsp>
        {
            if ($riesgoExternoObtenerResponse/ns1:errorCodigo)
            then <ns1:errorCodigo>{fn:data($riesgoExternoObtenerResponse/ns1:errorCodigo)}</ns1:errorCodigo>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:errorTipo )
            then <ns1:errorTipo>{fn:data($riesgoExternoObtenerResponse/ns1:errorTipo)}</ns1:errorTipo>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:errorDescripcion)
            then <ns1:errorDescripcion>{fn:data($riesgoExternoObtenerResponse/ns1:errorDescripcion)}</ns1:errorDescripcion>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sIdConsulta and fn:string-length($riesgoExternoObtenerResponse/ns1:sIdConsulta/text()) > 0 )
            then <ns1:sIdConsulta>{fn:data($riesgoExternoObtenerResponse/ns1:sIdConsulta)}</ns1:sIdConsulta>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sIdTransaccion and fn:string-length($riesgoExternoObtenerResponse/ns1:sIdTransaccion/text()) > 0)
            then <ns1:sIdTransaccion>{fn:data($riesgoExternoObtenerResponse/ns1:sIdTransaccion)}</ns1:sIdTransaccion>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sRut and fn:string-length($riesgoExternoObtenerResponse/ns1:sRut/text()) > 0) 
            then <ns1:sRut>{fn:data($riesgoExternoObtenerResponse/ns1:sRut)}</ns1:sRut>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sNombre and fn:string-length($riesgoExternoObtenerResponse/ns1:sNombre/text()) > 0) 
            then <ns1:sNombre>{fn:data($riesgoExternoObtenerResponse/ns1:sNombre)}</ns1:sNombre>
            else ()
        }
        {
            if (not(empty($riesgoExternoObtenerResponse/ns1:sfechaConstitucion/text()) ))
            then <ns1:sfechaConstitucion>{ fn-bea:date-from-string-with-format("yyyyMMdd",string($riesgoExternoObtenerResponse/ns1:sfechaConstitucion))}</ns1:sfechaConstitucion>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sISE and fn:string-length($riesgoExternoObtenerResponse/ns1:sISE/text()) > 0) 
            then <ns1:sISE>{fn:data($riesgoExternoObtenerResponse/ns1:sISE)}</ns1:sISE>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sScore  and fn:string-length($riesgoExternoObtenerResponse/ns1:sScore/text()) > 0) 
            then <ns1:sScore>{fn:data($riesgoExternoObtenerResponse/ns1:sScore)}</ns1:sScore>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sRentaPresunta and fn:string-length($riesgoExternoObtenerResponse/ns1:sRentaPresunta/text()) > 0) 
            then <ns1:sRentaPresunta>{fn:data($riesgoExternoObtenerResponse/ns1:sRentaPresunta)}</ns1:sRentaPresunta>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sBoletinConcursal and fn:string-length($riesgoExternoObtenerResponse/ns1:sBoletinConcursal/text()) > 0) 
            then <ns1:sBoletinConcursal>{fn:data($riesgoExternoObtenerResponse/ns1:sBoletinConcursal)}</ns1:sBoletinConcursal>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sTamaño  and fn:string-length($riesgoExternoObtenerResponse/ns1:sTamaño/text()) > 0) 
            then <ns1:sTamaño>{fn:data($riesgoExternoObtenerResponse/ns1:sTamaño)}</ns1:sTamaño>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sMonto_Facturacion  and fn:string-length($riesgoExternoObtenerResponse/ns1:sMonto_Facturacion/text()) > 0) 
            then <ns1:sMonto_Facturacion>{fn:data($riesgoExternoObtenerResponse/ns1:sMonto_Facturacion)}</ns1:sMonto_Facturacion>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sClasificadorEmpresa  and fn:string-length($riesgoExternoObtenerResponse/ns1:sClasificadorEmpresa/text()) > 0) 
            then <ns1:sClasificadorEmpresa>{fn:data($riesgoExternoObtenerResponse/ns1:sClasificadorEmpresa)}</ns1:sClasificadorEmpresa>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sBaseQuiebras  and fn:string-length($riesgoExternoObtenerResponse/ns1:sBaseQuiebras/text()) > 0) 
            then <ns1:sBaseQuiebras>{fn:data($riesgoExternoObtenerResponse/ns1:sBaseQuiebras)}</ns1:sBaseQuiebras>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sBolComNroDoc and fn:string-length($riesgoExternoObtenerResponse/ns1:sBolComNroDoc/text()) > 0) 
            then <ns1:sBolComNroDoc>{fn:data($riesgoExternoObtenerResponse/ns1:sBolComNroDoc)}</ns1:sBolComNroDoc>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sBolComMontoTotal and fn:string-length($riesgoExternoObtenerResponse/ns1:sBolComMontoTotal/text()) > 0) 
            then <ns1:sBolComMontoTotal>{fn:data($riesgoExternoObtenerResponse/ns1:sBolComMontoTotal)}</ns1:sBolComMontoTotal>
            else ()
        }
        {
            if (not(empty($riesgoExternoObtenerResponse/ns1:sBolComFecha/text())) and not($riesgoExternoObtenerResponse/ns1:sBolComFecha/text() = "00000000")) 
            then <ns1:sBolComFecha>{fn-bea:date-from-string-with-format("yyyy-MM-dd",string($riesgoExternoObtenerResponse/ns1:sBolComFecha))}</ns1:sBolComFecha>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sDicomNroDoc and fn:string-length($riesgoExternoObtenerResponse/ns1:sDicomNroDoc/text()) > 0) 
            then <ns1:sDicomNroDoc>{fn:data($riesgoExternoObtenerResponse/ns1:sDicomNroDoc)}</ns1:sDicomNroDoc>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sDicomMontoTotal and fn:string-length($riesgoExternoObtenerResponse/ns1:sDicomMontoTotal/text()) > 0) 
            then <ns1:sDicomMontoTotal>{fn:data($riesgoExternoObtenerResponse/ns1:sDicomMontoTotal)}</ns1:sDicomMontoTotal>
            else ()
        }
        {
            if (not(empty($riesgoExternoObtenerResponse/ns1:sDicomFecha/text() ) ) and not($riesgoExternoObtenerResponse/ns1:sDicomFecha/text() = "00000000"))
            then <ns1:sDicomFecha>{fn-bea:date-from-string-with-format("yyyy-MM-dd", string($riesgoExternoObtenerResponse/ns1:sDicomFecha))}</ns1:sDicomFecha>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sIcomNroDoc and fn:string-length($riesgoExternoObtenerResponse/ns1:sIcomNroDoc/text()) > 0) 
            then <ns1:sIcomNroDoc>{fn:data($riesgoExternoObtenerResponse/ns1:sIcomNroDoc)}</ns1:sIcomNroDoc>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sIcomMontoTotal and fn:string-length($riesgoExternoObtenerResponse/ns1:sIcomMontoTotal/text()) > 0) 
            then <ns1:sIcomMontoTotal>{fn:data($riesgoExternoObtenerResponse/ns1:sIcomMontoTotal)}</ns1:sIcomMontoTotal>
            else ()
        }
        {
            if (not(empty($riesgoExternoObtenerResponse/ns1:sIcomFecha/text())) and not($riesgoExternoObtenerResponse/ns1:sDicomFecha/text() = "00000000"))
            then <ns1:sIcomFecha>{fn-bea:date-from-string-with-format("yyyy-MM-dd",string($riesgoExternoObtenerResponse/ns1:sIcomFecha))}</ns1:sIcomFecha>
            else ()
        }
        {
            if ($riesgoExternoObtenerResponse/ns1:sUsuario)
            then <ns1:sUsuario>{fn:data($riesgoExternoObtenerResponse/ns1:sUsuario)}</ns1:sUsuario>
            else ()
        }
        {
            if (not(empty($riesgoExternoObtenerResponse/ns1:sFechaCreacion/text())))
            then <ns1:sFechaCreacion>{fn:data($riesgoExternoObtenerResponse/ns1:sFechaCreacion)}</ns1:sFechaCreacion>
            else ()
        }        
    </ns2:obtenerRiesgoExternoRsp>
};

local:func($riesgoExternoObtenerResponse)