xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/GestorRiesgoExterno/obtenerRiesgoExterno";
(:: import schema at "../XSD/obtenerRiesgoExterno.xsd" ::)
declare namespace ns2="http://cl.grupogtd.com/schema/RiesgoExterno/obtenerRiesgoExterno";
(:: import schema at "../../PS00007_ObtenerRiesgoExternoLoc/JCA/Schemas/obtenerRiesgoExterno.xsd" ::)

declare variable $obtenerRiesgoExternoReq as element() (:: element(*, ns1:obtenerRiesgoExternoReq) ::) external;

declare function local:func($obtenerRiesgoExternoReq as element() (:: element(*, ns1:obtenerRiesgoExternoReq) ::)) as element() (:: schema-element(ns2:riesgoExternoObtenerRequest) ::) {
    <ns2:riesgoExternoObtenerRequest>
        <ns2:rut>{fn:data($obtenerRiesgoExternoReq/ns1:rut)}</ns2:rut>
        <ns2:sistema>{fn:data($obtenerRiesgoExternoReq/ns1:sistema)}</ns2:sistema>
        <ns2:proceso>{fn:data($obtenerRiesgoExternoReq/ns1:proceso)}</ns2:proceso>
        <ns2:idProceso>{fn:data($obtenerRiesgoExternoReq/ns1:idProceso)}</ns2:idProceso>
        <ns2:usuario>{fn:data($obtenerRiesgoExternoReq/ns1:usuario)}</ns2:usuario>
        <ns2:canal>{fn:data($obtenerRiesgoExternoReq/ns1:canal)}</ns2:canal>
        <ns2:empresa>{fn:data($obtenerRiesgoExternoReq/ns1:empresa)}</ns2:empresa>
        <ns2:tipoCliente>{fn:data($obtenerRiesgoExternoReq/ns1:tipoCliente)}</ns2:tipoCliente>
    </ns2:riesgoExternoObtenerRequest>
};

local:func($obtenerRiesgoExternoReq)