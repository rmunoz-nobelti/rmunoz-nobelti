xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/wsdl/scm/GestorRiesgoExterno";
(:: import schema at "../WSDL/GestorRiesgoExterno.wsdl" ::)

declare namespace obt = "http://cl.grupogtd.com/schema/GestorRiesgoExterno/obtenerRiesgoExterno";

declare variable $reason as xs:string external;

declare function local:func($reason as xs:string) as element() (:: schema-element(ns1:obtenerRiesgoExternoRsp) ::) {
    if (fn:contains($reason,"rut")) then
      <ns1:obtenerRiesgoExternoRsp>
          <obt:errorCodigo>V001</obt:errorCodigo>
          <obt:errorTipo>Validación</obt:errorTipo>
          <obt:errorDescripcion>Formato RUT: 10 caracteres con ceros a la izquierda guion y digito verificador. Ej: 00999999-9</obt:errorDescripcion>
      </ns1:obtenerRiesgoExternoRsp>
    else if (fn:contains($reason,"tipoCliente")) then
      <ns1:obtenerRiesgoExternoRsp>
          <obt:errorCodigo>V002</obt:errorCodigo>
          <obt:errorTipo>Validación</obt:errorTipo>
          <obt:errorDescripcion>tipoCliente = PROSPECTO, CLIENTE, EXCLIENTE</obt:errorDescripcion>
      </ns1:obtenerRiesgoExternoRsp>
    else if (fn:contains($reason,"validacion")) then
      <ns1:obtenerRiesgoExternoRsp>
          <obt:errorCodigo>V003</obt:errorCodigo>
          <obt:errorTipo>Validación</obt:errorTipo>
          <obt:errorDescripcion>Request no valido</obt:errorDescripcion>
      </ns1:obtenerRiesgoExternoRsp>
    else if (fn:contains($reason,"T004")) then
      <ns1:obtenerRiesgoExternoRsp>
          <obt:errorCodigo>T004</obt:errorCodigo>
          <obt:errorTipo>Técnico</obt:errorTipo>
          <obt:errorDescripcion>System time out BE</obt:errorDescripcion>
      </ns1:obtenerRiesgoExternoRsp>
    else()
};

local:func($reason)