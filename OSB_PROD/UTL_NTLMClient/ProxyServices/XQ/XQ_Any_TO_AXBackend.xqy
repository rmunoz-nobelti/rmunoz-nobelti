xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../WSDL/NTLMClientService.wsdl" ::)
declare namespace ns2="http://www.openuri.org/";
(:: import schema at "../BS/WSDL/BS00001_NTLMClient.wsdl" ::)

declare variable $req as element() (:: schema-element(ns1:reqNTLM) ::) external;

declare function local:func($req as element() (:: schema-element(ns1:reqNTLM) ::)) as element() (:: schema-element(ns2:consume) ::) {
    <ns2:consume>
        <ns2:wsURL>{fn:data($req/ns1:wsURL)}</ns2:wsURL>
        <ns2:host>{fn:data($req/ns1:host)}</ns2:host>
        <ns2:user>{fn:data($req/ns1:user)}</ns2:user>
        <ns2:password>{fn:data($req/ns1:password)}</ns2:password>
        <ns2:soapAction>{fn:data($req/ns1:soapAction)}</ns2:soapAction>
        <ns2:xmlInput>{fn-bea:serialize($req/ns1:xmlInput/*)}</ns2:xmlInput>
    </ns2:consume>
};

local:func($req)