xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKey";
(:: import schema at "../XSD/EntityKey.xsd" ::)

declare variable $responseEntity as element() (:: schema-element(ns1:EntityKeyList) ::) external;

declare function local:func($responseEntity as element() (:: schema-element(ns1:EntityKeyList) ::)) as element() (:: schema-element(ns1:EntityKeyList) ::) {
    <ns1:EntityKeyList>
        {
            for $EntityKey in $responseEntity/ns1:EntityKey
            return 
            <ns1:EntityKey>
                <ns1:KeyData>
                    <ns1:KeyField>
                        <ns1:Field>{fn:data($EntityKey/ns1:KeyData/ns1:KeyField/ns1:Field)}</ns1:Field>
                        <ns1:Value>{fn:data($EntityKey/ns1:KeyData/ns1:KeyField/ns1:Value)}</ns1:Value>
                    </ns1:KeyField>
                </ns1:KeyData>
            </ns1:EntityKey>
        }</ns1:EntityKeyList>
};

local:func($responseEntity)