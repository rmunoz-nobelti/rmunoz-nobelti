xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare variable $strFault as xs:string external;
declare variable $strElement as xs:string external;

declare function local:getElementFromFault($strFault as xs:string, 
                                           $strElement as xs:string) 
                                           as xs:string {

  (: 
  
  Esta funcion recibe como parametros de entrada:
  
    strFault: es el string que almacena el fault que retorna un proceso BPEL al momento de lanzar un Throw con rollback.
              Este string no es XML. Almacena texto normal mas algunos elementos XML, pero como no es XML propiamente tal,
              hay que parsear dentro del string.
              
              Este es un ejemplo de este fault:
-- ini ejemplo              
faultName: {{http://schemas.oracle.com/bpel/extension}rollback}
messageType: {{http://schemas.oracle.com/bpel/extension}RuntimeFaultMessage}
parts: {{
summary=&lt;summary xmlns:def="http://www.w3.org/2001/XMLSchema" xsi:type="def:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">Error Fault CatchAll&lt;/summary>
,code=&lt;code xmlns:def="http://www.w3.org/2001/XMLSchema" xsi:type="def:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">99&lt;/code>
,detail=&lt;detail xmlns:def="http://www.w3.org/2001/XMLSchema" xsi:type="def:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">BPEL: Error en proceso BPEL, transacicon no completada.&lt;/detail>}
-- fin ejemplo

    strElement: indica el elemento dentro de $strFault que interesa recuperar.
                Los valores posibles son summary, code, detail
    
    Retorna: el valor del elemento XML solicitado en strElement.
    
    Ejemplo: si $strElement==detail, entonces la funcion retorna:
    BPEL: Error en proceso BPEL, transacicon no completada.
  :)
  
  let $strIni := fn:concat('<', $strElement)
  let $strFin := fn:concat('</', $strElement, '>')
  let $strSubstr := fn:substring-after ( 
                      fn:substring-before ( fn:substring-after( $strFault, $strIni), $strFin ),
                      '>')
  
  return $strSubstr
};

local:getElementFromFault($strFault, $strElement)