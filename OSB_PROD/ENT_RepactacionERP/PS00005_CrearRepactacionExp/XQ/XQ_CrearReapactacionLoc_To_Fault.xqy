xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/RepactacionERP/crearRepactacion";
(:: import schema at "../XSD/crearRepactacion.xsd" ::)

declare variable $errorCodigo as xs:string external;
declare variable $errorDescription as xs:string external;

declare function local:func($errorCodigo as xs:string, 
                            $errorDescription as xs:string) 
                            as element() (:: schema-element(ns1:crearRepactacionRsp) ::) {
    <ns1:crearRepactacionRsp>
        <ns1:idRepactacionERP></ns1:idRepactacionERP>
        <ns1:errorCodigo>{fn:data($errorCodigo)}</ns1:errorCodigo>
        <ns1:errorDescripcion>{fn:data($errorDescription)}</ns1:errorDescripcion>
    </ns1:crearRepactacionRsp>
};

local:func($errorCodigo, $errorDescription)