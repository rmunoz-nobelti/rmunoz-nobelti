xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/RepactacionERP/crearRepactacion";
(:: import schema at "../XSD/crearRepactacion.xsd" ::)

declare variable $reason as xs:string external;

declare function local:func($reason as xs:string) as element() (:: element(*, ns1:crearRepactacionRspType) ::) { 
   
     if (fn:contains($reason,"rutCliente")) then
        <ns1:crearRepactacionRspType>
          <ns1:errorCodigo>V001</ns1:errorCodigo>
          <ns1:errorTipo>Validación</ns1:errorTipo>
          <ns1:errorDescripcion>Rut debe venir con digito verificador y guion</ns1:errorDescripcion>
        </ns1:crearRepactacionRspType>
     else if (fn:contains($reason,"T1101")) then
        <ns1:crearRepactacionRspType>
          <ns1:errorCodigo>T1101</ns1:errorCodigo>
          <ns1:errorTipo>Técnico</ns1:errorTipo>
          <ns1:errorDescripcion>404 backend no disponible</ns1:errorDescripcion>
        </ns1:crearRepactacionRspType>
    else if (fn:contains($reason,"T1102")) then
        <ns1:crearRepactacionRspType>
          <ns1:errorCodigo>T1102</ns1:errorCodigo>
          <ns1:errorTipo>Técnico</ns1:errorTipo>
          <ns1:errorDescripcion>System time out BE</ns1:errorDescripcion>
        </ns1:crearRepactacionRspType>
    else()
};

local:func($reason)