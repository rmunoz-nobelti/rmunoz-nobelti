xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.grupogtd.com/schema/RepactacionERP/crearRepactacion";
(:: import schema at "../XSD/crearRepactacion.xsd" ::)
declare namespace ns1="http://tempuri.org";
(:: import schema at "../../PS00005_CrearRepactacionLoc/WS/Schemas/xppservice.xsd" ::)

declare namespace dyn = "http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application";

declare variable $response as element() (:: schema-element(ns1:GTDCustRenegotiateServicePostRenegotiationResponse) ::) external;

declare function local:func($response as element() (:: schema-element(ns1:GTDCustRenegotiateServicePostRenegotiationResponse) ::)) as element() (:: schema-element(ns2:crearRepactacionRsp) ::) {
    <ns2:crearRepactacionRsp>
        <ns2:idRepactacionERP>{fn:data($response/ns1:response/dyn:parmRenegotiationId)}</ns2:idRepactacionERP>
        <ns2:errorCodigo>0</ns2:errorCodigo>
        <ns2:errorDescripcion></ns2:errorDescripcion>
    </ns2:crearRepactacionRsp>
};

local:func($response)