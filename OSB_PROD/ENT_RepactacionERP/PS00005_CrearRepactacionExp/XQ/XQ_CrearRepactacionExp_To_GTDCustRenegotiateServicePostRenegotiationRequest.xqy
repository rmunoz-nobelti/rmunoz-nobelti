xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.grupogtd.com/schema/RepactacionERP/crearRepactacion";
(:: import schema at "../XSD/crearRepactacion.xsd" ::)
declare namespace ns1="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/WSDL/NTLMClientService.wsdl" ::)

declare variable $payloadIn as element() (:: schema-element(ns2:crearRepactacionReq) ::) external;

declare function local:func($payloadIn as element() (:: schema-element(ns2:crearRepactacionReq) ::)) as element() (:: schema-element(ns1:reqNTLM) ::) {
    <ns1:reqNTLM>
        <ns1:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDCustIntegrationServices/xppservice.svc</ns1:wsURL>
        <ns1:host>aosaifsrv01</ns1:host>
        <ns1:user>svsbus</ns1:user>
        <ns1:password>cachito</ns1:password>
        <ns1:soapAction>http://tempuri.org/GTDCustRenegotiateService/postRenegotiation</ns1:soapAction>
        <ns1:xmlInput>{
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:tem="http://tempuri.org" xmlns:dyn="http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application">
               <soapenv:Header>
                  <dat:CallContext>
                     <!--Optional:-->
                     <dat:Company>{fn:data($payloadIn/ns2:empresa)}</dat:Company>
                     <!--Optional:-->
                     <dat:Language></dat:Language>
                     <!--Optional:-->
                     <dat:LogonAsUser></dat:LogonAsUser>
                     <!--Optional:-->
                     <dat:MessageId></dat:MessageId>
                     <!--Optional:-->
                     <dat:PartitionKey></dat:PartitionKey>
                     <!--Optional:-->
                     <dat:PropertyBag>
                        <!--Zero or more repetitions:-->
                        <arr:KeyValueOfstringstring>
                           <arr:Key></arr:Key>
                           <arr:Value></arr:Value>
                        </arr:KeyValueOfstringstring>
                     </dat:PropertyBag>
                  </dat:CallContext>
               </soapenv:Header>
               <soapenv:Body>
                  <tem:GTDCustRenegotiateServicePostRenegotiationRequest>
                     <!--Optional:-->
                     <tem:_contract>
                        <!--Optional:-->
                        <dyn:Company>{fn:data($payloadIn/ns2:empresa)}</dyn:Company>
                        <!--Optional:-->
                        <dyn:CurrencyCode>CLP</dyn:CurrencyCode>
                        <!--Optional:-->
                        <dyn:CustAccount>{fn:data($payloadIn/ns2:rutCliente)}</dyn:CustAccount>
                        <!--Optional:-->
                        <dyn:Downpayment>{fn:data($payloadIn/ns2:montoPie)}</dyn:Downpayment>
                        <!--Optional:-->
                        <dyn:InvoiceAccount>{fn:data($payloadIn/ns2:nroCuenta)}</dyn:InvoiceAccount>
                        <!--Optional:-->
                        <dyn:NumOfPayment>{fn:data($payloadIn/ns2:cantCuotas)}</dyn:NumOfPayment>
                        <!--Optional:-->
                        <dyn:RenegotiatedAmount>{fn:data($payloadIn/ns2:totalRepactado)}</dyn:RenegotiatedAmount>
                        <!--Optional:-->
                        <dyn:RenegotiationDate>{fn:data($payloadIn/ns2:fechaRepactacion)}</dyn:RenegotiationDate>
                        <!--Optional:-->
                        <dyn:RenegotiationId>{fn:data($payloadIn/ns2:idRepactacion)}</dyn:RenegotiationId>
                     </tem:_contract>
                  </tem:GTDCustRenegotiateServicePostRenegotiationRequest>
               </soapenv:Body>
            </soapenv:Envelope>
    }
    </ns1:xmlInput>
    </ns1:reqNTLM>
};

local:func($payloadIn)