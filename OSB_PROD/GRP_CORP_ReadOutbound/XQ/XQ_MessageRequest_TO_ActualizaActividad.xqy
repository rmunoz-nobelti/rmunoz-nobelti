xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "../XSD/actualizaRequest.xsd" ::)
declare namespace ns1="http://cl.gtd.com/schema/reclamos/actualizaActividadRequest";
(:: import schema at "../../GRP_CORP_TOAService/PSC0002_ActualizaActividadTOA/XSD/ActualizaActividadRequest.xsd" ::)

declare variable $actualizaRequest as element() (:: schema-element(actualizaActividadRequest) ::) external;

declare function local:func($actualizaRequest as element() (:: schema-element(actualizaActividadRequest) ::)) as element() (:: schema-element(ns1:actualizaActividadRequest) ::) {
    <ns1:actualizaActividadRequest>
        <ns1:idActividadTOA>{fn:data($actualizaRequest/idActividadTOA)}</ns1:idActividadTOA>
        <ns1:estadoTOA>{fn:data($actualizaRequest/estadoTOA)}</ns1:estadoTOA>
        <ns1:avances>{fn-bea:serialize($actualizaRequest/avances)}</ns1:avances>
    </ns1:actualizaActividadRequest>
};

local:func($actualizaRequest)