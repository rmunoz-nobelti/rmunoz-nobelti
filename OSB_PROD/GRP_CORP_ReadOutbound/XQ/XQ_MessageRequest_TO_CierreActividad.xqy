xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "../XSD/cierreRequest.xsd" ::)
declare namespace ns1="http://cl.gtd.com/schema/reclamos/cierreActividadRequest";
(:: import schema at "../../GRP_CORP_TOAService/PSC0001_CierreActividadTOA/XSD/CierreActividadRequest.xsd" ::)

declare variable $messageRequest as element() (:: schema-element(cierreActividadRequest) ::) external;
declare variable $firma as xs:base64Binary external;

declare function local:func($messageRequest as element() (:: schema-element(cierreActividadRequest) ::),$firma as xs:base64Binary) as element() (:: schema-element(ns1:cierreActividadRequest) ::) {
    <ns1:cierreActividadRequest>
        <ns1:idActividadTOA>{fn:data($messageRequest/idActividadTOA)}</ns1:idActividadTOA>
        <ns1:lon84Efectivo>{fn:data($messageRequest/lon84Efectivo)}</ns1:lon84Efectivo>
        <ns1:lat84Efectivo>{fn:data($messageRequest/lat84Efectivo)}</ns1:lat84Efectivo>
        <ns1:lugarTrabajo>{fn:data($messageRequest/lugarTrabajo)}</ns1:lugarTrabajo>
        <ns1:asignacionFolios>{fn:data($messageRequest/asignacionFolios)}</ns1:asignacionFolios>
        <ns1:tiempoTrabajo>{fn:data($messageRequest/tiempoTrabajo)}</ns1:tiempoTrabajo>
        <ns1:fechaCierre>{fn:data($messageRequest/fechaCierre)}</ns1:fechaCierre>
        <ns1:cambiosRed>{fn:data($messageRequest/cambiosRed)}</ns1:cambiosRed>
        <ns1:conformidadRutPasaporte>{fn:data($messageRequest/conformidadRutPasaporte)}</ns1:conformidadRutPasaporte>
        <ns1:conformidadNombre>{fn:data($messageRequest/conformidadNombre)}</ns1:conformidadNombre>
        <ns1:conformidadFirma>{$firma}</ns1:conformidadFirma>
        <ns1:observacionesCierre>{fn:data($messageRequest/observacionesCierre)}</ns1:observacionesCierre>
        <ns1:tecnicoInicioActividad>{fn:data($messageRequest/tecnicoInicioActividad)}</ns1:tecnicoInicioActividad>
        <ns1:clasificacion>{fn:data($messageRequest/clasificacion)}</ns1:clasificacion>
    </ns1:cierreActividadRequest>
};

local:func($messageRequest,$firma)