xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "../XSD/actualizaRequest.xsd" ::)
declare namespace ns1="urn:toa:outbound";
(:: import schema at "../../OFSC/Outbound/WSDLs/outbound.wsdl" ::)

declare variable $actualizarActividad as element() (:: schema-element(actualizaActividadRequest) ::) external;
declare variable $status as xs:string external;
declare variable $messageId as xs:string external;

declare function local:func($actualizarActividad as element() (:: schema-element(actualizaActividadRequest) ::),$status as xs:string, $messageId as xs:string) as element() (:: schema-element(ns1:set_message_status) ::) {
    <ns1:set_message_status>
        <messages>
            <message>
                <message_id>{fn:data($messageId)}</message_id>
                <status>{fn:data($status)}</status>
                <description>{fn:data($actualizarActividad/estadoTOA)}</description>
            </message>
        </messages>
    </ns1:set_message_status>
};

local:func($actualizarActividad,$status,$messageId)