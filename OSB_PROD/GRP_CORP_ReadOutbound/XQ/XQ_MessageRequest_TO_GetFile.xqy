xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.gtd.com/getFileRequest";
(:: import schema at "../../GRP_CORP_TOAService/PSL0006_GetFile/XSD/getFileRequest.xsd" ::)

declare variable $entityKey as xs:string external;
declare variable $propertyId as xs:string external;

declare function local:func($entityKey as xs:string, 
                            $propertyId as xs:string) 
                            as element() (:: schema-element(ns1:getFileRequest) ::) {
    <ns1:getFileRequest>
        <ns1:idEntidad>{fn:data($entityKey)}</ns1:idEntidad>
        <ns1:idPropiedad>{fn:data($propertyId)}</ns1:idPropiedad>
    </ns1:getFileRequest>
};

local:func($entityKey, $propertyId)