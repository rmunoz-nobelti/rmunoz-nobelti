xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.gtd.com/schema/reclamos/cierreActividadRequest";
(:: import schema at "../../GRP_CORP_TOAService/PSC0001_CierreActividadTOA/XSD/CierreActividadRequest.xsd" ::)
declare namespace ns1="urn:toatech:agent";
(:: import schema at "../../OFSC/MiddlewareGateway/WSDLs/middleware_advanced.xsd" ::)

declare variable $sendMessageRequest as element() (:: schema-element(ns1:send_message) ::) external;

declare function local:func($sendMessageRequest as element() (:: schema-element(ns1:send_message) ::)) as element() (:: schema-element(ns2:cierreActividadRequest) ::) {
    
	let $message := fn-bea:inlinedXML($sendMessageRequest/ns1:messages/ns1:message/ns1:body)
	let $xmlMessage := fn-bea:inlinedXML($message)
	return	
	<ns2:cierreActividadRequest>
        <ns2:idActividadTOA>{fn:data($xmlMessage/activity_number)}</ns2:idActividadTOA>
        <ns2:lon84Efectivo>{fn:data($xmlMessage/activity_coordx)}</ns2:lon84Efectivo>
        <ns2:lat84Efectivo>{fn:data($xmlMessage/activity_coordy)}</ns2:lat84Efectivo>
        {
            if ($sendMessageRequest/ns1:messages/ns1:message/ns1:body)
            then <ns2:lugarTrabajo>{fn:data($sendMessageRequest/ns1:messages/ns1:message/ns1:body)}</ns2:lugarTrabajo>
            else ()
        }
        {
            if ($sendMessageRequest/ns1:messages/ns1:message/ns1:body)
            then <ns2:asignacionFolios>{fn:data($xmlMessage/resource_name)}</ns2:asignacionFolios>
            else ()
        }
        <ns2:tiempoTrabajo>{fn:data($xmlMessage/activity_duration)}</ns2:tiempoTrabajo>
        <ns2:fechaCierre></ns2:fechaCierre>
        {
            if ($sendMessageRequest/ns1:messages/ns1:message/ns1:body)
            then <ns2:cambiosRed>{fn:data($sendMessageRequest/ns1:messages/ns1:message/ns1:body)}</ns2:cambiosRed>
            else ()
        }
        {
            if ($sendMessageRequest/ns1:messages/ns1:message/ns1:body)
            then <ns2:conformidadRutPasaporte>{fn:data($sendMessageRequest/ns1:messages/ns1:message/ns1:body)}</ns2:conformidadRutPasaporte>
            else ()
        }
        {
            if ($xmlMessage/property/label/text() = 'XA_TELE_nomb_recepcion')
            then 
            let $prop := $xmlMessage/property
            return
            <ns2:conformidadNombre>{$prop/value/text()}</ns2:conformidadNombre>
            else ()
        }
        {
            if ($sendMessageRequest/ns1:messages/ns1:message/ns1:body)
            then <ns2:conformidadFirma>{fn:data($sendMessageRequest/ns1:messages/ns1:message/ns1:body)}</ns2:conformidadFirma>
            else ()
        }
        {
            if ($sendMessageRequest/ns1:messages/ns1:message/ns1:body)
            then <ns2:observacionesCierre>{fn:data($sendMessageRequest/ns1:messages/ns1:message/ns1:body)}</ns2:observacionesCierre>
            else ()
        }
        {
            if ($sendMessageRequest/ns1:messages/ns1:message/ns1:body)
            then <ns2:avances>{fn:data($sendMessageRequest/ns1:messages/ns1:message/ns1:body)}</ns2:avances>
            else ()
        }
        {
            if ($sendMessageRequest/ns1:messages/ns1:message/ns1:body)
            then <ns2:tecnicoInicioActividad>{fn:data($xmlMessage/resource_name)}</ns2:tecnicoInicioActividad>
            else ()
        }
        {
            if ($sendMessageRequest/ns1:messages/ns1:message/ns1:body)
            then <ns2:clasificacion>{fn:data($xmlMessage/activity_worktype_label)}</ns2:clasificacion>
            else ()
        }
    </ns2:cierreActividadRequest>
};

local:func($sendMessageRequest)