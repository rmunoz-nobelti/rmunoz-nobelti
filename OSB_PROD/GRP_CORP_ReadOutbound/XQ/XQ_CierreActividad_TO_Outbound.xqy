xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "../XSD/cierreRequest.xsd" ::)
declare namespace ns1="urn:toa:outbound";
(:: import schema at "../../OFSC/Outbound/WSDLs/outbound.wsdl" ::)

declare variable $cierreActividad as element() (:: schema-element(cierreActividadRequest) ::) external;
declare variable $status as xs:string external;
declare variable $messageId as xs:string external;

declare function local:func($cierreActividad as element() (:: schema-element(cierreActividadRequest) ::),$status as xs:string,$messageId as xs:string) as element() (:: schema-element(ns1:set_message_status) ::) {
    <ns1:set_message_status>
        <messages>
            <message>
                <message_id>{fn:data($messageId)}</message_id>
                <status>{fn:data($status)}</status>
                <description>{fn:data($cierreActividad/estadoTOA)}</description>
            </message>
        </messages>
    </ns1:set_message_status>
};

local:func($cierreActividad,$status,$messageId)