xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application";
(:: import schema at "../XSD/ClosingProjectResponse.xsd" ::)
declare variable $code as xs:string external;

declare variable $string as xs:string external;


declare function local:XQ_NTLMClient_TO_ClosingProject($code as xs:string, $string as xs:string) as element() (:: schema-element(ns1:GTDClosingAutomaticProjectServiceClosingProjectResponse) ::) {
<ns1:GTDClosingAutomaticProjectServiceClosingProjectResponse>
<ns1:codigo>{fn:data($code)}</ns1:codigo>
        <ns1:descripcion>{fn:data($string)}</ns1:descripcion>
</ns1:GTDClosingAutomaticProjectServiceClosingProjectResponse>
};

local:XQ_NTLMClient_TO_ClosingProject($code, $string)