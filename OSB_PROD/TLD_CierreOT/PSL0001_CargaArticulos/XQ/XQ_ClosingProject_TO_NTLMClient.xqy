xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/XSD/NTLMClient.xsd" ::)
declare namespace ns1="http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application";
(:: import schema at "../XSD/ClosingProjectRequest.xsd" ::)

declare variable $request as element() (:: schema-element(ns1:GTDClosingAutomaticProjectServiceClosingProjectRequest) ::) external;

declare function local:func($request as element() (:: schema-element(ns1:GTDClosingAutomaticProjectServiceClosingProjectRequest) ::)) as element() (:: schema-element(ns2:request) ::) {
    <ns2:reqNTLM>
       <ns2:wsURL>{dvmtr:lookup('UT_Common/DVM/UrlMappingAX', 'Servicio', 'TLD_CierreOT_Svc1', 'Url', 'No se encontró url')}</ns2:wsURL>
        <ns2:host>{dvmtr:lookup('UT_Common/DVM/UrlMappingAX', 'Servicio', 'TLD_CierreOT_Svc1', 'Host', 'No se encontró host')}</ns2:host>
        <ns2:user>{dvmtr:lookup('UT_Common/DVM/UrlMappingAX', 'Servicio', 'TLD_CierreOT_Svc1', 'Usuario', 'No se encontró usuario')}</ns2:user>
        <ns2:password>{dvmtr:lookup('UT_Common/DVM/UrlMappingAX', 'Servicio', 'TLD_CierreOT_Svc1', 'Password', 'No se encontró password')}</ns2:password>
        <ns2:soapAction>http://tempuri.org/GTDClosingAutomaticProjectService/closingProject</ns2:soapAction>
        <ns2:xmlInput>{<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org"
        xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts">
        <soapenv:Header>
      <dat:CallContext>
         <dat:Company>{fn:data($request/ns1:Company)}</dat:Company>
         <dat:Language>ES</dat:Language>
      </dat:CallContext>
   </soapenv:Header>
  <soapenv:Body>
      <tem:GTDClosingAutomaticProjectServiceClosingProjectRequest>
         <tem:_contract>
            <ns1:ACCeOrdenTrabajo>{fn:data($request/ns1:ACCeOrdenTrabajo)}</ns1:ACCeOrdenTrabajo>
            <ns1:EndDate>{fn:data($request/ns1:EndDate)}</ns1:EndDate>
            <ns1:Name>{fn:data($request/ns1:Name)}</ns1:Name>
            <ns1:ProjId>{fn:data($request/ns1:ProjId)}</ns1:ProjId>
            <ns1:String30>{fn:data($request/ns1:String30)}</ns1:String30>
            <ns1:TransDate>{fn:data($request/ns1:TransDate)}</ns1:TransDate>
         </tem:_contract>
      </tem:GTDClosingAutomaticProjectServiceClosingProjectRequest>
   </soapenv:Body>
</soapenv:Envelope>}</ns2:xmlInput>
    </ns2:reqNTLM>
};

local:func($request)