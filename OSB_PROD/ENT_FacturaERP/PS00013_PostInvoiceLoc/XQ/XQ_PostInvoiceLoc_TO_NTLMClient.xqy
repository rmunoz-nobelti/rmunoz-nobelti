xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/WSDL/NTLMClientService.wsdl" ::)
declare namespace ns1="http://tempuri.org";
(:: import schema at "../WS/Schemas/XMLSchema_2036091149.xsd" ::)

declare namespace dyn = "http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application";

declare variable $input as element() (:: schema-element(ns1:GTDCustInvoicePostServicePostInvoiceRequest) ::) external;

declare function local:XQ_PostInvoiceLoc_TO_NTLMClient($input as element() (:: schema-element(ns1:GTDCustInvoicePostServicePostInvoiceRequest) ::)) as element() (:: schema-element(ns2:reqNTLM) ::) {
    <ns2:reqNTLM>
        <ns2:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDCustIntegrationServices/xppservice.svc</ns2:wsURL>
        <ns2:host>axtest</ns2:host>
        <ns2:user>{fn:data("GTDDevelopment.com\\svsbus")}</ns2:user>
        <ns2:password>cachito</ns2:password>
        <ns2:soapAction>http://tempuri.org/GTDCustInvoicePostService/postInvoice</ns2:soapAction>
        <ns2:xmlInput>
          <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:tem="http://tempuri.org" xmlns:dyn="http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application">
                 <soapenv:Header>
                  <dat:CallContext>
                     <dat:Company>tld</dat:Company>
                     <dat:Language/>
                     <dat:LogonAsUser/>
                     <dat:MessageId/>
                     <dat:PartitionKey/>
                     <dat:PropertyBag>
                        <arr:KeyValueOfstringstring>
                           <arr:Key/>
                           <arr:Value/>
                        </arr:KeyValueOfstringstring>
                     </dat:PropertyBag>
                  </dat:CallContext>
               </soapenv:Header>
               <soapenv:Body>
                  <tem:GTDCustInvoicePostServicePostInvoiceRequest>
                     <tem:_header>
                        <dyn:BranchId>{fn:data($input/ns1:_header/dyn:BranchId)}</dyn:BranchId>
                        <dyn:CodCallejero>{fn:data($input/ns1:_header/dyn:CodCallejero)}</dyn:CodCallejero>
                        <dyn:CurrencyCode>{fn:data($input/ns1:_header/dyn:CurrencyCode)}</dyn:CurrencyCode>
                        <dyn:CustAccount>{fn:data($input/ns1:_header/dyn:CustAccount)}</dyn:CustAccount>
                        <dyn:DataArea>{fn:data($input/ns1:_header/dyn:DataArea)}</dyn:DataArea>
                        <dyn:DefaultDim_Zona>{fn:data($input/ns1:_header/dyn:DefaultDim_Zona)}</dyn:DefaultDim_Zona>
                        <dyn:DueDate>{fn:data($input/ns1:_header/dyn:DueDate)}</dyn:DueDate>
                        <dyn:InvoiceAccount>{fn:data($input/ns1:_header/dyn:InvoiceAccount)}</dyn:InvoiceAccount>
                        <dyn:InvoiceDate>{fn:data($input/ns1:_header/dyn:InvoiceDate)}</dyn:InvoiceDate>
                        <dyn:InvoiceId>{fn:data($input/ns1:_header/dyn:InvoiceId)}</dyn:InvoiceId>
                        <dyn:IssuePoint>{fn:data($input/ns1:_header/dyn:IssuePoint)}</dyn:IssuePoint>    
                        <dyn:Lines>
                        {
                          for $GTDCustInvoiceLineContract in $input/ns1:_header/dyn:Lines/dyn:GTDCustInvoiceLineContract
                          return
                            <dyn:GTDCustInvoiceLineContract>
                              <dyn:BillingCode>{fn:data($GTDCustInvoiceLineContract/dyn:BillingCode)}</dyn:BillingCode>
                              <dyn:LineAmount>{fn:data($GTDCustInvoiceLineContract/dyn:LineAmount)}</dyn:LineAmount>
                              <dyn:Qty>{fn:data($GTDCustInvoiceLineContract/dyn:Qty)}</dyn:Qty>
                              <dyn:SubGroupDescription>{fn:data($GTDCustInvoiceLineContract/dyn:SubGroupDescription)}</dyn:SubGroupDescription>
                              <dyn:SubGroupId>{fn:data($GTDCustInvoiceLineContract/dyn:SubGroupId)}</dyn:SubGroupId>
                              <dyn:UnitPrice>{fn:data($GTDCustInvoiceLineContract/dyn:UnitPrice)}</dyn:UnitPrice>
                            </dyn:GTDCustInvoiceLineContract>
                        }
                        </dyn:Lines>
                        <dyn:Payment>{fn:data($input/ns1:_header/dyn:Payment)}</dyn:Payment>
                        <dyn:ReasonCode>{fn:data($input/ns1:_header/dyn:ReasonCode)}</dyn:ReasonCode>
                        <dyn:ReasonText>{fn:data($input/ns1:_header/dyn:ReasonText)}</dyn:ReasonText>
                        <dyn:References>
                            {
                              for $GTDCustInvoiceReferenceContract in $input/ns1:_header/dyn:References/dyn:GTDCustInvoiceReferenceContract
                              return
                                <dyn:GTDCustInvoiceReferenceContract>
                                  <dyn:DocDateRef>{fn:data($GTDCustInvoiceReferenceContract/dyn:DocDateRef)}</dyn:DocDateRef>
                                  <dyn:IdDocRef>{fn:data($GTDCustInvoiceReferenceContract/dyn:IdDocRef)}</dyn:IdDocRef>                 
                                  <dyn:IdTdocRef>{fn:data($GTDCustInvoiceReferenceContract/dyn:IdTdocRef)}</dyn:IdTdocRef>
                                </dyn:GTDCustInvoiceReferenceContract>
                            }
                        </dyn:References>
                        <dyn:TaxAmount>{fn:data($input/ns1:_header/dyn:TaxAmount)}</dyn:TaxAmount>
                        <dyn:TradeDocumentId>{fn:data($input/ns1:_header/dyn:TradeDocumentId)}</dyn:TradeDocumentId>
                        <dyn:VoucherBSS>{fn:data($input/ns1:_header/dyn:VoucherBSS)}</dyn:VoucherBSS>
                     </tem:_header>
                  </tem:GTDCustInvoicePostServicePostInvoiceRequest>
               </soapenv:Body>
            </soapenv:Envelope>
        </ns2:xmlInput>
    </ns2:reqNTLM>
};

local:XQ_PostInvoiceLoc_TO_NTLMClient($input)