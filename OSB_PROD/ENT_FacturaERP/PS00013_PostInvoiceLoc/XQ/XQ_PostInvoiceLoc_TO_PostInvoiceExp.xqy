xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/AX/response";
(:: import schema at "../../../UT_Common/Schemas/responseAX.xsd" ::)

declare variable $responseCode as xs:string external;
declare variable $responseString as xs:string external;

declare function local:XQ_PostInvoiceLoc_TO_PostInvoiceExp($responseCode as xs:string, 
                                                           $responseString as xs:string) 
                                                           as element() (:: schema-element(ns1:AXResponse) ::) {
    <ns1:AXResponse>
        <ns1:responseCode>{fn:data($responseCode)}</ns1:responseCode>
        <ns1:responseString>{fn:data($responseString)}</ns1:responseString>
    </ns1:AXResponse>
};

local:XQ_PostInvoiceLoc_TO_PostInvoiceExp($responseCode, $responseString)