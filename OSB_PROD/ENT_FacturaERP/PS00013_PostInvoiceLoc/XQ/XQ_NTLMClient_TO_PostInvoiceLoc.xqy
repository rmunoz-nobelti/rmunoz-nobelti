xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/WSDL/NTLMClientService.wsdl" ::)
declare namespace ns2="http://tempuri.org";
(:: import schema at "../WS/Schemas/XMLSchema_2036091149.xsd" ::)

declare variable $resp as element() (:: schema-element(ns1:rspNTLM) ::) external;

declare function local:XQ_NTLMClient_TO_PostInvoiceLoc($resp as element() (:: schema-element(ns1:rspNTLM) ::)) as element() (:: schema-element(ns2:GTDCustInvoicePostServicePostInvoiceResponse) ::) {
    <ns2:GTDCustInvoicePostServicePostInvoiceResponse/>
};

local:XQ_NTLMClient_TO_PostInvoiceLoc($resp)