xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/FacturaERP/postInvoice";
(:: import schema at "../XSD/postInvoice.xsd" ::)

declare variable $inputMessage as element() (:: schema-element(ns1:postInvoiceReq) ::) external;

declare function local:XQ_PostInvoiceExp_TO_PostInvoiceExp($inputMessage as element() (:: schema-element(ns1:postInvoiceReq) ::)) as element() (:: schema-element(ns1:postInvoiceReq) ::) {
    <ns1:postInvoiceReq>
        <ns1:empresa>{fn:data($inputMessage/ns1:empresa)}</ns1:empresa>
        <ns1:numDoc>{fn:data($inputMessage/ns1:numDoc)}</ns1:numDoc>
        <ns1:cuentaFac>{fn:data($inputMessage/ns1:cuentaFac)}</ns1:cuentaFac>
        <ns1:rutCliente>{fn:data($inputMessage/ns1:rutCliente)}</ns1:rutCliente>
        {
            if ($inputMessage/ns1:clienteCom)
            then <ns1:clienteCom>{dvmtr:lookup('ENT_FacturaERP/PS00013_PostInvoiceExp/DVM/ZONA', 'Key', $inputMessage/ns1:clienteCom, 'Value', '-1')}</ns1:clienteCom>
            else()
        }
        <ns1:fechaVencimiento>{fn:data($inputMessage/ns1:fechaVencimiento)}</ns1:fechaVencimiento>
        <ns1:codigoCallejero>{fn:data($inputMessage/ns1:codigoCallejero)}</ns1:codigoCallejero>
        <ns1:iva>{fn:data($inputMessage/ns1:iva)}</ns1:iva>
        {
            if ($inputMessage/ns1:tipoDoc)
            then <ns1:tipoDoc>{dvmtr:lookup('ENT_FacturaERP/PS00013_PostInvoiceExp/DVM/TIPO_DOC', 'Key', $inputMessage/ns1:tipoDoc, 'Value', '-1')}</ns1:tipoDoc>
            else ()
        }
        {
            if ($inputMessage/ns1:voucherBSS)
            then <ns1:voucherBSS>{fn:data($inputMessage/ns1:voucherBSS)}</ns1:voucherBSS>
            else ()
        }
        {
            if ($inputMessage/ns1:codigoOficina)
            then <ns1:codigoOficina>{dvmtr:lookup('ENT_FacturaERP/PS00013_PostInvoiceExp/DVM/SUCURSAL_BRANCH', 'Key', $inputMessage/ns1:codigoOficina, 'Value', '-1')}</ns1:codigoOficina>
            else ()
        }
        {
            if ($inputMessage/ns1:puntoEmision)
            then <ns1:puntoEmision>{dvmtr:lookup('ENT_FacturaERP/PS00013_PostInvoiceExp/DVM/SUCURSAL_ISSUE', 'Key', $inputMessage/ns1:puntoEmision, 'Value', '-1')}</ns1:puntoEmision>
            else ()
        }
        <ns1:fechaEmision>{fn:data($inputMessage/ns1:fechaEmision)}</ns1:fechaEmision>
        {
            if ($inputMessage/ns1:fechaDocReferenciado)
            then <ns1:fechaDocReferenciado>{fn:data($inputMessage/ns1:fechaDocReferenciado)}</ns1:fechaDocReferenciado>
            else ()
        }
        {
            if ($inputMessage/ns1:folioDocReferenciado)
            then <ns1:folioDocReferenciado>{fn:data($inputMessage/ns1:folioDocReferenciado)}</ns1:folioDocReferenciado>
            else ()
        }
        {
            if ($inputMessage/ns1:tipoDocReferenciado)
            then <ns1:tipoDocReferenciado>{dvmtr:lookup('ENT_FacturaERP/PS00013_PostInvoiceExp/DVM/TIPO_DOC', 'Key', $inputMessage/ns1:tipoDocReferenciado, 'Value', '-1')}</ns1:tipoDocReferenciado>
            else ()
        }
        <ns1:detalle>
            {
                for $detallesLista in $inputMessage/ns1:detalle/ns1:detallesLista
                return 
                <ns1:detallesLista>
                    <ns1:codigoProducto>{fn:data($detallesLista/ns1:codigoProducto)}</ns1:codigoProducto>
                    <ns1:cantidad>{fn:data($detallesLista/ns1:cantidad)}</ns1:cantidad>
                    <ns1:precioUnitario>{fn:data($detallesLista/ns1:precioUnitario)}</ns1:precioUnitario>
                    <ns1:precio>{fn:data($detallesLista/ns1:precio)}</ns1:precio>
                    <ns1:descripcionSubGrupo>{fn:data($detallesLista/ns1:descripcionSubGrupo)}</ns1:descripcionSubGrupo>
                    <ns1:subGrupo>{fn:data($detallesLista/ns1:subGrupo)}</ns1:subGrupo>
                </ns1:detallesLista>
            }
        </ns1:detalle>
    </ns1:postInvoiceReq>
};

local:XQ_PostInvoiceExp_TO_PostInvoiceExp($inputMessage)