xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/FacturaERP/postInvoice";
(:: import schema at "../XSD/postInvoice.xsd" ::)


declare function local:XQ_PostInvoiceValidationError_TO_PostInvoiceExp() as element() (:: schema-element(ns1:postInvoiceRsp) ::) {
    <ns1:postInvoiceRsp>
        <ns1:errorCodigo>V001</ns1:errorCodigo>
        <ns1:errorTipo>Validación</ns1:errorTipo>
        <ns1:errorDescripcion>Error en los datos de entrada</ns1:errorDescripcion>    
    </ns1:postInvoiceRsp>
};

local:XQ_PostInvoiceValidationError_TO_PostInvoiceExp()