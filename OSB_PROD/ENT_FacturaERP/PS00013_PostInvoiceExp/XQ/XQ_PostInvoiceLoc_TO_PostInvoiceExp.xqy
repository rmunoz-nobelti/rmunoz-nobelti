xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/AX/response";
(:: import schema at "../../../UT_Common/Schemas/responseAX.xsd" ::)
declare namespace ns2="http://cl.grupogtd.com/schema/FacturaERP/postInvoice";
(:: import schema at "../XSD/postInvoice.xsd" ::)

declare variable $responseMessage as element() (:: schema-element(ns1:AXResponse) ::) external;

declare function local:XQ_PostInvoiceLoc_TO_PostInvoiceExp($responseMessage as element() (:: schema-element(ns1:AXResponse) ::)) as element() (:: schema-element(ns2:postInvoiceRsp) ::) {
    <ns2:postInvoiceRsp>
        <ns2:errorCodigo>{fn:data($responseMessage/ns1:responseCode)}</ns2:errorCodigo>
        <ns2:errorDescripcion>{fn:data($responseMessage/ns1:responseString)}</ns2:errorDescripcion>
    </ns2:postInvoiceRsp>
};

local:XQ_PostInvoiceLoc_TO_PostInvoiceExp($responseMessage)