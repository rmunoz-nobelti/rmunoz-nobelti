xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/FacturaERP/postInvoice";
(:: import schema at "../XSD/postInvoice.xsd" ::)
declare namespace ns2="http://tempuri.org";
(:: import schema at "../../PS00013_PostInvoiceLoc/WS/Schemas/XMLSchema_2036091149.xsd" ::)

declare namespace dyn = "http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application";

declare variable $input as element() (:: schema-element(ns1:postInvoiceReq) ::) external;

declare function local:XQ_PostInvoiceExp_TO_PostInvoiceLoc($input as element() (:: schema-element(ns1:postInvoiceReq) ::)) as element() (:: schema-element(ns2:GTDCustInvoicePostServicePostInvoiceRequest) ::) {
    <ns2:GTDCustInvoicePostServicePostInvoiceRequest>
        <ns2:_header>
            <dyn:Attention></dyn:Attention>
            <dyn:BranchId>{fn:data($input/ns1:codigoOficina)}</dyn:BranchId>
            <dyn:CodCallejero>{fn:data($input/ns1:codigoCallejero)}</dyn:CodCallejero>
            <dyn:CurrencyCode>CLP</dyn:CurrencyCode>
            <dyn:CustAccount>{fn:data($input/ns1:cuentaFac)}</dyn:CustAccount>
            <dyn:DataArea>{fn:data($input/ns1:empresa)}</dyn:DataArea>
            <dyn:DefaultDim_Zona>{fn:data($input/ns1:clienteCom)}</dyn:DefaultDim_Zona>
            <dyn:Downpayment>0</dyn:Downpayment>
            <dyn:DteTemplateId></dyn:DteTemplateId>
            <dyn:DteVersionId></dyn:DteVersionId>
            <dyn:DueDate>{fn:data($input/ns1:fechaVencimiento)}</dyn:DueDate>
            <dyn:EndingPeriod></dyn:EndingPeriod>
            <dyn:GL></dyn:GL>
            <dyn:GUF></dyn:GUF>
            <dyn:GULT>1900-01-01</dyn:GULT>
            <dyn:InvoiceAccount>{fn:data($input/ns1:rutCliente)}</dyn:InvoiceAccount>
            <dyn:InvoiceDate>{fn:data($input/ns1:fechaEmision)}</dyn:InvoiceDate>
            <dyn:InvoiceId>{fn:data($input/ns1:numDoc)}</dyn:InvoiceId>
            {
                if ($input/ns1:puntoEmision)
                then <dyn:IssuePoint>{fn:data($input/ns1:puntoEmision)}</dyn:IssuePoint>
                else ()
            }
            <dyn:Lines>
                {
                    for $detallesLista in $input/ns1:detalle/ns1:detallesLista
                    return 
                    <dyn:GTDCustInvoiceLineContract>
                        <dyn:BillingCode>{fn:data($detallesLista/ns1:codigoProducto)}</dyn:BillingCode>
                        <dyn:LineAmount>{fn:data($detallesLista/ns1:precio)}</dyn:LineAmount>
                        {
                          if (fn:data($input/ns1:tipoDoc) = 'N Cdto Elect') then
                            <dyn:Qty>{xs:int(fn:data($detallesLista/ns1:cantidad)) * -1}</dyn:Qty>
                          else
                            <dyn:Qty>{fn:data($detallesLista/ns1:cantidad)}</dyn:Qty>
                        }
                        <dyn:SubGroupDescription>{fn:data($detallesLista/ns1:descripcionSubGrupo)}</dyn:SubGroupDescription>
                        <dyn:SubGroupId>{fn:data($detallesLista/ns1:subGrupo)}</dyn:SubGroupId>
                        <dyn:UnitPrice>{fn:data($detallesLista/ns1:precioUnitario)}</dyn:UnitPrice></dyn:GTDCustInvoiceLineContract>
                }</dyn:Lines>
            <dyn:PaymSched></dyn:PaymSched>
            <dyn:Payment>CO</dyn:Payment>
            <dyn:PreviousBalance>0</dyn:PreviousBalance>
            {
                if (fn:data($input/ns1:tipoDoc) = dvmtr:lookup('ENT_FacturaERP/PS00013_PostInvoiceExp/DVM/TIPO_DOC', 'Key', '61', 'Value', '-1'))
                then <dyn:ReasonCode>1</dyn:ReasonCode>
                else (
                  <dyn:ReasonCode>0</dyn:ReasonCode>
                )
            }
            {
                if (fn:data($input/ns1:tipoDoc) = dvmtr:lookup('ENT_FacturaERP/PS00013_PostInvoiceExp/DVM/TIPO_DOC', 'Key', '61', 'Value', '-1'))
                then <dyn:ReasonText>Anula Documento de Referencia</dyn:ReasonText>
                else (
                  <dyn:ReasonText></dyn:ReasonText>
                )
            }
            <dyn:References>
                <dyn:GTDCustInvoiceReferenceContract>
                    {
                        if ($input/ns1:fechaDocReferenciado)
                        then <dyn:DocDateRef>{fn:data($input/ns1:fechaDocReferenciado)}</dyn:DocDateRef>
                        else ()
                    }
                    {
                        if ($input/ns1:folioDocReferenciado)
                        then <dyn:IdDocRef>{fn:data($input/ns1:folioDocReferenciado)}</dyn:IdDocRef>
                        else ()
                    }
                    <dyn:IdPV></dyn:IdPV>
                    {
                        if ($input/ns1:tipoDocReferenciado)
                        then <dyn:IdTdocRef>{fn:data($input/ns1:tipoDocReferenciado)}</dyn:IdTdocRef>
                        else ()
                    }
                    <dyn:ObsRef></dyn:ObsRef>
                </dyn:GTDCustInvoiceReferenceContract>
            </dyn:References>
            <dyn:SettleBalance>0</dyn:SettleBalance>
            <dyn:StartingPeriod>1900-01-01</dyn:StartingPeriod>
            <dyn:TaxAmount>{fn:data($input/ns1:iva)}</dyn:TaxAmount>
            <dyn:TemplateName></dyn:TemplateName>
            <dyn:TradeDocumentId>{fn:data($input/ns1:tipoDoc)}</dyn:TradeDocumentId>
            {
                if ($input/ns1:voucherBSS)
                then <dyn:VoucherBSS>{fn:data($input/ns1:voucherBSS)}</dyn:VoucherBSS>
                else ()
            }
        </ns2:_header>
    </ns2:GTDCustInvoicePostServicePostInvoiceRequest>
};

local:XQ_PostInvoiceExp_TO_PostInvoiceLoc($input)