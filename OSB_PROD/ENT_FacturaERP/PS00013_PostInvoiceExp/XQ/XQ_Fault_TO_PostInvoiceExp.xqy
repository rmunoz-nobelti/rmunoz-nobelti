xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/FacturaERP/postInvoice";
(:: import schema at "../XSD/postInvoice.xsd" ::)

declare variable $errorCodigo as xs:string external;
declare variable $errorTipo as xs:string external;
declare variable $errorDescripcion as xs:string external;

declare function local:XQ_Fault_TO_PostInvoiceExp($errorCodigo as xs:string, 
                                                  $errorTipo as xs:string, 
                                                  $errorDescripcion as xs:string) 
                                                  as element() (:: schema-element(ns1:postInvoiceRsp) ::) {
    <ns1:postInvoiceRsp>
        <ns1:errorCodigo>{fn:data($errorCodigo)}</ns1:errorCodigo>
        <ns1:errorTipo>{fn:data($errorTipo)}</ns1:errorTipo>
        <ns1:errorDescripcion>{fn:data($errorDescripcion)}</ns1:errorDescripcion>
    </ns1:postInvoiceRsp>
};

local:XQ_Fault_TO_PostInvoiceExp($errorCodigo, $errorTipo, $errorDescripcion)