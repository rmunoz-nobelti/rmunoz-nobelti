xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "../WS/XSD/clienteType.xsd" ::)
declare namespace ns1="http://cl.grupogtd.com/schema/ClienteLDILoc/datosClienteLDI";
(:: import schema at "../WS/XSD/gestionDatosCliente.xsd" ::)

declare variable $clienteVO as element() (:: schema-element(clienteVO) ::) external;

declare function local:func($clienteVO as element() (:: schema-element(clienteVO) ::)) as element() (:: schema-element(ns1:datosClienteResponse) ::) {
    <ns1:datosClienteResponse>
        <ns1:blocked>{fn:data($clienteVO/blocked)}</ns1:blocked>
        <ns1:companyChainId>{fn:data($clienteVO/companyChain)}</ns1:companyChainId>
        <ns1:currency>{fn:data($clienteVO/currency)}</ns1:currency>
        <ns1:custAccount>{fn:data($clienteVO/custAccount)}</ns1:custAccount>
        <ns1:custGroup>{fn:data($clienteVO/custGroup)}</ns1:custGroup>
        <ns1:dataArea>{fn:data($clienteVO/dataArea)}</ns1:dataArea>
        <ns1:dimensionValue5>{fn:data($clienteVO/dimensionValue5)}</ns1:dimensionValue5>
        <ns1:dimensionValue6>{fn:data($clienteVO/dimensionValue6)}</ns1:dimensionValue6>
        <ns1:dlvTerm>{fn:data($clienteVO/dlvTerm)}</ns1:dlvTerm>
        <ns1:invoiceAccount>{fn:data($clienteVO/invoiceAccount)}</ns1:invoiceAccount>
        <ns1:laguageId>{fn:data($clienteVO/languageId)}</ns1:laguageId>
        <ns1:lineOfBusinessDescription>{fn:data($clienteVO/lineOfBusinessDescription)}</ns1:lineOfBusinessDescription>
        <ns1:lineOfBusinessId>{fn:data($clienteVO/lineOfBusinessId)}</ns1:lineOfBusinessId>
        <ns1:name>{fn:data($clienteVO/name)}</ns1:name>
        <ns1:no2CLCustPartyTypeTable>{fn:data($clienteVO/NO2CLCustPartyTypeTable)}</ns1:no2CLCustPartyTypeTable>
        <ns1:PaymTermId>{fn:data($clienteVO/paymTerm)}</ns1:PaymTermId>
        <ns1:vatNum>{fn:data($clienteVO/VATNum)}</ns1:vatNum>
        <ns1:segmentId>{fn:data($clienteVO/segmentId)}</ns1:segmentId>
        {
            for $axDireccion in $clienteVO/axDireccion
            return 
            <ns1:direccion>
                <ns1:buildingCompliment>{fn:data($axDireccion/buildingCompliment)}</ns1:buildingCompliment>
                <ns1:city>{fn:data($axDireccion/city)}</ns1:city>
                <ns1:codCallejero>{fn:data($axDireccion/codCallejero)}</ns1:codCallejero>
                <ns1:codDependenciaRecId>{fn:data($axDireccion/codDependenciaRecId)}</ns1:codDependenciaRecId>
                <ns1:codigoPostal>{fn:data($axDireccion/codPostal)}</ns1:codigoPostal>
                <ns1:countryRegionId>{fn:data($axDireccion/countryRegionId)}</ns1:countryRegionId>
                <ns1:country>{fn:data($axDireccion/country)}</ns1:country>
                <ns1:description>{fn:data($axDireccion/description)}</ns1:description>
                <ns1:isPrimary>{fn:data($axDireccion/isPrimary)}</ns1:isPrimary>
                <ns1:roles>{fn:data($axDireccion/role)}</ns1:roles>
                <ns1:street>{fn:data($axDireccion/street)}</ns1:street>
                <ns1:streetNumber>{fn:data($axDireccion/streetNum)}</ns1:streetNumber></ns1:direccion>
        }
    </ns1:datosClienteResponse>
	
};

local:func($clienteVO)