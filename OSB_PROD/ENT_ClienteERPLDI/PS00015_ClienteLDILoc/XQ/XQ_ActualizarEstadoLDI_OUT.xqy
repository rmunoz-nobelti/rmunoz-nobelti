xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/ClienteLDILoc/datosClienteLDI";
(:: import schema at "../WS/XSD/gestionDatosCliente.xsd" ::)


declare function local:func() as element() (:: schema-element(ns1:actualizarEstadoResponse) ::) {
    <ns1:actualizarEstadoResponse>
        <ns1:errorCodigo>0</ns1:errorCodigo>
        <ns1:errorTipo></ns1:errorTipo>
        <ns1:errorDescripcion></ns1:errorDescripcion>
    </ns1:actualizarEstadoResponse>
};

local:func()