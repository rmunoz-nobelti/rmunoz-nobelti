xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.gtdteleductos.com/ofsc/integracion";

declare function ns1:getUsername($inputPath as xs:string) as xs:string
{
  let $data := fn-bea:lookupBasicCredentials($inputPath)
  return
    if (exists($data/ns1:username/text()))
    then $data/ns1:username/text()
    else ""
};
 
declare function ns1:getPassword($inputPath as xs:string) as xs:string
{
  let $data := fn-bea:lookupBasicCredentials($inputPath)
  return
    if (exists($data/ns1:password/text()))
    then $data/ns1:password/text()
    else ""
};


declare variable $ns1:in_parameter as node() external;

declare function ns1:func_to_XMLSA($ns1:in_parameter as node()) {
  "Prueba"
};

ns1:func_to_XMLSA($ns1:in_parameter)