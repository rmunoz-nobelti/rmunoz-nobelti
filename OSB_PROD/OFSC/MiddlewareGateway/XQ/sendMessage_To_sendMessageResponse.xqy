xquery version "1.0" encoding "utf-8";

(:: import schema at "../WSDLs/middleware_advanced.xsd" ::)

declare namespace ns0="urn:toatech:agent";
declare namespace xf="http://tempuri.org/OFSC/Middleware/XQ/sendMessage_To_sendMessageResponse/";

declare function xf:sendMessage_To_sendMessageResponse($send_message as element(ns0:send_message),
    $status as xs:string,
    $description as xs:string)
    as element(ns0:send_message_response) {
      <ns0:send_message_response>
            {
                for $message in $send_message/ns0:messages/ns0:message
                return
                    <ns0:message_response>
                        <ns0:message_id>{ data($message/ns0:message_id) }</ns0:message_id>
                        <ns0:status>{ $status }</ns0:status>
                        <ns0:description>{ $description }</ns0:description>
                    </ns0:message_response>
            }
        </ns0:send_message_response>
};

declare variable $send_message as element(ns0:send_message) external;
declare variable $status as xs:string external;
declare variable $description as xs:string external;

xf:sendMessage_To_sendMessageResponse($send_message, $status, $description)