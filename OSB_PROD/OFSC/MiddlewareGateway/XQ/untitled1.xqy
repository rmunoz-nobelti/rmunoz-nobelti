xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="urn:toatech:agent";
(:: import schema at "../WSDLs/middleware_advanced.xsd" ::)

declare variable $req as element() (:: schema-element(ns1:send_message) ::) external;

declare function local:func($req as element() (:: schema-element(ns1:send_message) ::)) as element() (:: schema-element(ns1:send_message_response) ::) {
    <ns1:send_message_response>
        {
            for $message in $req/ns1:messages/ns1:message
            return 
            <ns1:message_response>
                <ns1:message_id>{fn:data($message/ns1:message_id)}</ns1:message_id>
                <ns1:status>sending</ns1:status>
                <ns1:description>mensaje recibido</ns1:description>
            </ns1:message_response>
        }
    </ns1:send_message_response>
};

local:func($req)