xquery version "2004-draft";
(:: pragma bea:schema-type-parameter parameter="$message" type="ns0:message_t" location="../WSDL/outboundAPI_AdvancedWorkflow.xsd" ::)
(:: pragma  parameter="$fault" type="anyType" ::)
(:: pragma bea:global-element-return element="ns0:error_message" location="../WSDL/inboundGateway_InternalMessage.xsd" ::)

declare namespace ns0 = "urn:toatech:agent";
declare namespace xf = "http://tempuri.org/OFSC/InboundGateway/XQuery/errorHandler_to_errorMessage/";

declare function xf:errorHandler_to_errorMessage($message as element(),
    $fault as element(*),
    $status as xs:string,
    $description as xs:string)
    as element(ns0:error_message) {
        <ns0:error_message>
            {
                let $message_t := $message
                return
                    <ns0:message>
                        <ns0:app_host>{ data($message/ns0:app_host) }</ns0:app_host>
                        <ns0:app_port>{ data($message/ns0:app_port) }</ns0:app_port>
                        <ns0:app_url>{ data($message/ns0:app_url) }</ns0:app_url>
                        <ns0:message_id>{ data($message/ns0:message_id) }</ns0:message_id>
                        {
                            for $company_id in $message/ns0:company_id
                            return
                                <ns0:company_id>{ data($company_id) }</ns0:company_id>
                        }
                        {
                            for $address in $message/ns0:address
                            return
                                <ns0:address>{ data($address) }</ns0:address>
                        }
                        {
                            for $send_to in $message/ns0:send_to
                            return
                                <ns0:send_to>{ data($send_to) }</ns0:send_to>
                        }
                        {
                            for $subject in $message/ns0:subject
                            return
                                <ns0:subject>{ data($subject) }</ns0:subject>
                        }
                        {
                            for $body in $message/ns0:body
                            return
                                <ns0:body>{ data($body) }</ns0:body>
                        }
                    </ns0:message>
            }
            <ns0:status>{ $status }</ns0:status>
            <ns0:description>{ $description }</ns0:description>
            <ns0:fault>{ $fault/@* , $fault/node() }</ns0:fault>
        </ns0:error_message>
};

declare variable $message as element() external;
declare variable $fault as element(*) external;
declare variable $status as xs:string external;
declare variable $description as xs:string external;

xf:errorHandler_to_errorMessage($message,
    $fault,
    $status,
    $description)