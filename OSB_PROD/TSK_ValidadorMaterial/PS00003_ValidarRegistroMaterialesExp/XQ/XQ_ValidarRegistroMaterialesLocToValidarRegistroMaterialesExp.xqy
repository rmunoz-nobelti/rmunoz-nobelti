xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.grupogtd.com/wsdl/scm/ValidadorMaterial";
(:: import schema at "../WSDL/ValidadorMaterial.wsdl" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/SP_FGET_VALIDACION_MQH";
(:: import schema at "../../PS00003_ValidarRegistroMaterialesLoc/JCA/Schemas/SP_FGET_VALIDACION_MQH_sp.xsd" ::)

declare namespace val = "http://cl.grupogtd.com/schema/ValidadorMaterial/validarRegistroMateriales";

declare variable $OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::) external;

declare function local:func($OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::)) as element() (:: schema-element(ns2:validarRegistroMaterialesRsp) ::) {
    <ns2:validarRegistroMaterialesRsp>
        <val:resultado>{fn:data($OutputParameters/ns1:FGET_VALIDACION_MQH)}</val:resultado>
        <val:errorCodigo>0</val:errorCodigo>
        <val:errorTipo></val:errorTipo>        
        <val:errorDescripcion></val:errorDescripcion>
    </ns2:validarRegistroMaterialesRsp>
};

local:func($OutputParameters)