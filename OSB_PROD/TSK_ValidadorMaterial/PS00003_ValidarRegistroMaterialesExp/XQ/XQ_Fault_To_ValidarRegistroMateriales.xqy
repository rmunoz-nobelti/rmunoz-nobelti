xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/wsdl/scm/ValidadorMaterial";
(:: import schema at "../WSDL/ValidadorMaterial.wsdl" ::)

declare namespace val = "http://cl.grupogtd.com/schema/ValidadorMaterial/validarRegistroMateriales";

declare variable $reason as xs:string external;

declare function local:func($reason as xs:string) as element() (:: schema-element(ns1:validarRegistroMaterialesRsp) ::) {
    if (fn:contains($reason,"tipoDocumento")) then
    <ns1:validarRegistroMaterialesRsp>
        <val:errorCodigo>V001</val:errorCodigo>
        <val:errorTipo>Validación</val:errorTipo>
        <val:errorDescripcion>Parámetro de entrada "tipo_Documento", debe ser numérico, el cuàl debe contener valores entre 1 y 4.</val:errorDescripcion>
    </ns1:validarRegistroMaterialesRsp>
    else if (fn:contains($reason,"T001")) then
    <ns1:validarRegistroMaterialesRsp>
        <val:errorCodigo>T001</val:errorCodigo>
        <val:errorTipo>Técnico</val:errorTipo>
        <val:errorDescripcion>404 backend no disponible</val:errorDescripcion>
    </ns1:validarRegistroMaterialesRsp>
    else()
};

local:func($reason)