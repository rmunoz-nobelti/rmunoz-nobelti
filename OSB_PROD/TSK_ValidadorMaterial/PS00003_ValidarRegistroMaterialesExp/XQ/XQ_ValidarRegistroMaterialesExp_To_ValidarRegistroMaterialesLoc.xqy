xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/wsdl/scm/ValidadorMaterial";
(:: import schema at "../WSDL/ValidadorMaterial.wsdl" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/SP_FGET_VALIDACION_MQH";
(:: import schema at "../../PS00003_ValidarRegistroMaterialesLoc/JCA/Schemas/SP_FGET_VALIDACION_MQH_sp.xsd" ::)

declare namespace val = "http://cl.grupogtd.com/schema/ValidadorMaterial/validarRegistroMateriales";

declare variable $validarRegistroMaterialesReq as element() (:: schema-element(ns1:validarRegistroMaterialesReq) ::) external;

declare function local:func($validarRegistroMaterialesReq as element() (:: schema-element(ns1:validarRegistroMaterialesReq) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
     <ns2:InputParameters>
        <ns2:VTIPO_DOCUMENTO>{fn:data($validarRegistroMaterialesReq/val:tipoDocumento)}</ns2:VTIPO_DOCUMENTO>
        <ns2:VNRO_ID>{fn:data($validarRegistroMaterialesReq/val:numeroId)}</ns2:VNRO_ID>
    </ns2:InputParameters>
};

local:func($validarRegistroMaterialesReq)