xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/Proveedor/gestionarProveedor";
(:: import schema at "../XSD/gestionarProveedor.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/SP_PPUT_PROVEEDOR";
(:: import schema at "../../PS00001_GestionarProveedorLoc/JCA/Schemas/SP_PPUT_PROVEEDOR_sp.xsd" ::)

declare variable $inputParam as element() (:: schema-element(ns1:gestionarProveedorReq) ::) external;

declare function local:func($inputParam as element() (:: schema-element(ns1:gestionarProveedorReq) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        {
            if ($inputParam/ns1:rutProveedor)
            then <ns2:VRUTPROVEEDOR>{fn:data($inputParam/ns1:rutProveedor)}</ns2:VRUTPROVEEDOR>
            else (<ns2:VRUTPROVEEDOR></ns2:VRUTPROVEEDOR>)
        }
        {
            if ($inputParam/ns1:razonSocial)
            then <ns2:VRAZONSOCIAL>{fn:data($inputParam/ns1:razonSocial)}</ns2:VRAZONSOCIAL>
            else (<ns2:VRAZONSOCIAL></ns2:VRAZONSOCIAL>)
        }
        {
            if ($inputParam/ns1:personalidad)
            then <ns2:VPERSONALIDAD>{fn:data($inputParam/ns1:personalidad)}</ns2:VPERSONALIDAD>
            else (<ns2:VPERSONALIDAD></ns2:VPERSONALIDAD>)
        }
        {
            if ($inputParam/ns1:estado)
            then <ns2:VESTADO>{fn:data($inputParam/ns1:estado)}</ns2:VESTADO>
            else (<ns2:VESTADO></ns2:VESTADO>)
        }
        {
            if ($inputParam/ns1:tipoProveedor)
            then <ns2:VTIPOPROVEEDOR>{fn:data($inputParam/ns1:tipoProveedor)}</ns2:VTIPOPROVEEDOR>
            else (<ns2:VTIPOPROVEEDOR></ns2:VTIPOPROVEEDOR>)
        }
        {
            if ($inputParam/ns1:giroComercial)
            then <ns2:VGIROCOMERCIAL>{fn:data($inputParam/ns1:giroComercial)}</ns2:VGIROCOMERCIAL>
            else (<ns2:VGIROCOMERCIAL></ns2:VGIROCOMERCIAL>)
        }
        <ns2:VGESTION>{fn:string('N')}</ns2:VGESTION>
        <ns2:VCOSTOTRANSPORTE>{fn:string('N')}</ns2:VCOSTOTRANSPORTE>
        <ns2:VDIGITADOR>{fn:string('ADMIINTEGRACION')}</ns2:VDIGITADOR>
        
        </ns2:InputParameters>
};

local:func($inputParam)