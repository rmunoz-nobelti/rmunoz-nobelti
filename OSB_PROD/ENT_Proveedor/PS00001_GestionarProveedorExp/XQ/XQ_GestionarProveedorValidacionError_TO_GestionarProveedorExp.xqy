xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/Proveedor/gestionarProveedor";
(:: import schema at "../XSD/gestionarProveedor.xsd" ::)

declare variable $reason as xs:string external;

declare function local:XQ_GestionarProveedorValidacionError_TO_GestionarProveedorExp($reason as xs:string) as element() (:: schema-element(ns1:gestionarProveedorRsp) ::) {
    if (fn:contains($reason,"rutProveedor")) then
                  <ns1:gestionarProveedorRsp>
                      <ns1:errorCodigo>V001</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                      <ns1:errorDescripcion>Rut debe venir con digito verificador y guion y rellenado con ceros a la izquierda, máximo 12 caracteres.</ns1:errorDescripcion>    
                  </ns1:gestionarProveedorRsp>
            else  if (fn:contains($reason,"razonSocial")) then
                <ns1:gestionarProveedorRsp>
                      <ns1:errorCodigo>V002</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>Largo de la Razón Social debe ser de máximo 50 caracteres.</ns1:errorDescripcion>    
                  </ns1:gestionarProveedorRsp>
            else  if (fn:contains($reason,"personalidad")) then
                <ns1:gestionarProveedorRsp>
                      <ns1:errorCodigo>V003</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>Personalidad válida (‘J’ - Jurídica, ‘N’ - Natural).</ns1:errorDescripcion>    
                  </ns1:gestionarProveedorRsp>
            else  if (fn:contains($reason,"estado")) then
                <ns1:gestionarProveedorRsp>
                      <ns1:errorCodigo>V004</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>Estado enviado válido (‘ACTIVO’, ‘NOACT’).</ns1:errorDescripcion>    
                  </ns1:gestionarProveedorRsp>
            else  if (fn:contains($reason,"tipoProveedor")) then
                <ns1:gestionarProveedorRsp>
                      <ns1:errorCodigo>V005</ns1:errorCodigo>
                      <ns1:errorTipo>Validación</ns1:errorTipo>
                       <ns1:errorDescripcion>Tipo de Proveedor correcto: 1 = Proveedor de Material , 2 = Proveedor de Servicios, 3 = Proveedor de Enlace, 4 = Contratista.</ns1:errorDescripcion>    
                  </ns1:gestionarProveedorRsp>
            else if (fn:contains($reason,"giroComercial")) then
                <ns1:gestionarProveedorRsp>
                     <ns1:errorCodigo>V006</ns1:errorCodigo>
                     <ns1:errorTipo>Validación</ns1:errorTipo>
                     <ns1:errorDescripcion>Giro comercial incorrecto.</ns1:errorDescripcion>    
                </ns1:gestionarProveedorRsp>                
            else ()
};

local:XQ_GestionarProveedorValidacionError_TO_GestionarProveedorExp($reason)