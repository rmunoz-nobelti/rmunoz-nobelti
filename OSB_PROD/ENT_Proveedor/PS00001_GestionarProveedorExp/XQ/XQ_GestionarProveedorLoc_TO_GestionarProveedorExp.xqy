xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.grupogtd.com/schema/Proveedor/gestionarProveedor";
(:: import schema at "../XSD/gestionarProveedor.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/SP_PPUT_PROVEEDOR";
(:: import schema at "../../PS00001_GestionarProveedorLoc/JCA/Schemas/SP_PPUT_PROVEEDOR_sp.xsd" ::)

declare variable $outputParmetrs as element() (:: schema-element(ns1:OutputParameters) ::) external;

declare function local:func($outputParmetrs as element() (:: schema-element(ns1:OutputParameters) ::)) as element() (:: schema-element(ns2:gestionarProveedorRsp) ::) {
    <ns2:gestionarProveedorRsp>
        {
            if ($outputParmetrs/ns1:CODIGO_ERROR_OUT)
            then <ns2:errorCodigo>{fn:data($outputParmetrs/ns1:CODIGO_ERROR_OUT)}</ns2:errorCodigo>
            else (<ns2:errorCodigo></ns2:errorCodigo>)
        }
        <ns2:errorTipo></ns2:errorTipo>
        {
            if ($outputParmetrs/ns1:GLS_ERR_OUT)
            then <ns2:errorDescripcion>{fn:data($outputParmetrs/ns1:GLS_ERR_OUT)}</ns2:errorDescripcion>
            else (<ns2:errorDescripcion></ns2:errorDescripcion>)
        }
    </ns2:gestionarProveedorRsp>
};

local:func($outputParmetrs)