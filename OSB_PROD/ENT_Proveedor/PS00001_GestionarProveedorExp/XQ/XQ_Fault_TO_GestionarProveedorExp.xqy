xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/Proveedor/gestionarProveedor";
(:: import schema at "../XSD/gestionarProveedor.xsd" ::)

declare variable $errorCod as xs:string external;
declare variable $errorTip as xs:string external;
declare variable $errorDesc as xs:string external;

declare function local:XQ_Fault_TO_GestionarProveedorExp($errorCod as xs:string, 
                                                         $errorTip as xs:string, 
                                                         $errorDesc as xs:string) 
                                                         as element() (:: schema-element(ns1:gestionarProveedorRsp) ::) {
    <ns1:gestionarProveedorRsp>
        <ns1:errorCodigo>{fn:data($errorCod)}</ns1:errorCodigo>
        <ns1:errorTipo>{fn:data($errorTip)}</ns1:errorTipo>
        <ns1:errorDescripcion>{fn:data($errorDesc)}</ns1:errorDescripcion>
    </ns1:gestionarProveedorRsp>
};

local:XQ_Fault_TO_GestionarProveedorExp($errorCod, $errorTip, $errorDesc)