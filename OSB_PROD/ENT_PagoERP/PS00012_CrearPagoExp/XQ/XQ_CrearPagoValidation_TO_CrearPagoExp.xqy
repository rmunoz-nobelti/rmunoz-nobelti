xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/PagoERP/crearPago";
(:: import schema at "../XSD/crearPago.xsd" ::)

declare variable $reason as xs:string external;

declare function local:func($reason as xs:string) as element() (:: schema-element(ns1:crearPagoRsp) ::) {
    if (fn:contains($reason,"idEmpresa")) then
          <ns1:crearPagoRsp>
              <ns1:errorCodigo>V001</ns1:errorCodigo>
              <ns1:errorTipo>Validación</ns1:errorTipo>
              <ns1:errorDescripcion>Dato no válido para idEmpresa.</ns1:errorDescripcion>    
          </ns1:crearPagoRsp>
     else  if (fn:contains($reason,"fechaTransaccion")) then
          <ns1:crearPagoRsp>
              <ns1:errorCodigo>V002</ns1:errorCodigo>
              <ns1:errorTipo>Validación</ns1:errorTipo>
              <ns1:errorDescripcion>Dato no válido para fechaTransaccion.</ns1:errorDescripcion>    
          </ns1:crearPagoRsp>
     else  if (fn:contains($reason,"tipoCuenta")) then
          <ns1:crearPagoRsp>
              <ns1:errorCodigo>V003</ns1:errorCodigo>
              <ns1:errorTipo>Validación</ns1:errorTipo>
              <ns1:errorDescripcion>Dato no válido para tipoCuenta.</ns1:errorDescripcion>    
          </ns1:crearPagoRsp>
     else  if (fn:contains($reason,"rutCliente")) then
          <ns1:crearPagoRsp>
              <ns1:errorCodigo>V004</ns1:errorCodigo>
              <ns1:errorTipo>Validación</ns1:errorTipo>
              <ns1:errorDescripcion>Dato no válido para rutCliente.</ns1:errorDescripcion>    
          </ns1:crearPagoRsp>
     else  if (fn:contains($reason,"formaPago")) then
          <ns1:crearPagoRsp>
              <ns1:errorCodigo>V005</ns1:errorCodigo>
              <ns1:errorTipo>Validación</ns1:errorTipo>
              <ns1:errorDescripcion>Dato no válido para formaPago.</ns1:errorDescripcion>    
          </ns1:crearPagoRsp>
     else  if (fn:contains($reason,"empresa")) then
          <ns1:crearPagoRsp>
              <ns1:errorCodigo>V006</ns1:errorCodigo>
              <ns1:errorTipo>Validación</ns1:errorTipo>
              <ns1:errorDescripcion>Dato no válido para empresa.</ns1:errorDescripcion>    
          </ns1:crearPagoRsp>
    else  if (fn:contains($reason,"cuentaFacturacion")) then
          <ns1:crearPagoRsp>
              <ns1:errorCodigo>V007</ns1:errorCodigo>
              <ns1:errorTipo>Validación</ns1:errorTipo>
              <ns1:errorDescripcion>Dato no válido para cuentaFacturacion.</ns1:errorDescripcion>    
          </ns1:crearPagoRsp>
     else  if (fn:contains($reason,"fechaDocumento")) then
          <ns1:crearPagoRsp>
              <ns1:errorCodigo>V008</ns1:errorCodigo>
              <ns1:errorTipo>Validación</ns1:errorTipo>
              <ns1:errorDescripcion>Dato no válido para fechaDocumento.</ns1:errorDescripcion>    
          </ns1:crearPagoRsp>
     else  if (fn:contains($reason,"voucherBSS")) then
          <ns1:crearPagoRsp>
              <ns1:errorCodigo>V009</ns1:errorCodigo>
              <ns1:errorTipo>Validación</ns1:errorTipo>
              <ns1:errorDescripcion>Dato no válido para voucherBSS.</ns1:errorDescripcion>    
          </ns1:crearPagoRsp>
     else  if (fn:contains($reason,"codSubGrupo")) then
          <ns1:crearPagoRsp>
              <ns1:errorCodigo>V010</ns1:errorCodigo>
              <ns1:errorTipo>Validación</ns1:errorTipo>
              <ns1:errorDescripcion>Dato no válido para codSubGrupo.</ns1:errorDescripcion>    
          </ns1:crearPagoRsp>
     else  if (fn:contains($reason,"monto")) then
          <ns1:crearPagoRsp>
              <ns1:errorCodigo>V011</ns1:errorCodigo>
              <ns1:errorTipo>Validación</ns1:errorTipo>
              <ns1:errorDescripcion>Dato no válido para monto.</ns1:errorDescripcion>    
          </ns1:crearPagoRsp>
     else (
          <ns1:crearPagoRsp>
                <ns1:errorCodigo>V012</ns1:errorCodigo>
                <ns1:errorTipo>Validación</ns1:errorTipo>
                 <ns1:errorDescripcion>Error en los datos de entrada.</ns1:errorDescripcion>    
            </ns1:crearPagoRsp>
     )
};

local:func($reason)