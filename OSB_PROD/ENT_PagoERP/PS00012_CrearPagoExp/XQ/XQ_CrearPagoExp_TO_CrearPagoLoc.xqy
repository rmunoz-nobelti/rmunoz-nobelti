xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/PagoERP/crearPago";
(:: import schema at "../XSD/crearPago.xsd" ::)
declare namespace ns2="http://tempuri.org";
(:: import schema at "../../PS00012_CrearPagoLoc/WS/Schemas/XMLSchema_2036091149.xsd" ::)

declare namespace dyn = "http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application";

declare variable $input as element() (:: schema-element(ns1:crearPagoReq) ::) external;

declare function local:func($input as element() (:: schema-element(ns1:crearPagoReq) ::)) 
                            as element() (:: schema-element(ns2:GTDCustPaymentsServiceCreatePaymentsRequest) ::) {
    <ns2:GTDCustPaymentsServiceCreatePaymentsRequest>
        <ns2:_companyId>{fn:data($input/ns1:idEmpresa)}</ns2:_companyId>
        <ns2:_paymentLines>
            <dyn:GTDCustPaymentLineContract>
                <dyn:AccountType>{fn:data("3")}</dyn:AccountType>
                <dyn:Company>{fn:data($input/ns1:empresa)}</dyn:Company>
                <dyn:DocumentDate>{fn:data("1900-01-01")}</dyn:DocumentDate>
                <dyn:LedgerDimension>{fn:data($input/ns1:rutCliente)}</dyn:LedgerDimension>
                <dyn:Lines>
                    {
                        for $detallesLista in $input/ns1:datosDetalle/ns1:detallesLista
                        return 
                        <dyn:GTDCustPaymentOpenTransContract>
                            <dyn:SettleAmount>{fn:data($detallesLista/ns1:monto)}</dyn:SettleAmount>
                            <dyn:SubGroupId>{fn:data($detallesLista/ns1:codSubGrupo)}</dyn:SubGroupId>
                            <dyn:VoucherBSS>{fn:data($detallesLista/ns1:voucherBSS)}</dyn:VoucherBSS>
                        </dyn:GTDCustPaymentOpenTransContract>
                    }
                </dyn:Lines>
                <dyn:OrderAccount>{fn:data($input/ns1:cuentaFacturacion)}</dyn:OrderAccount>
                <dyn:PaymMode>{fn:data($input/ns1:formaPago)}</dyn:PaymMode>
                <dyn:PaymReference></dyn:PaymReference>
                <dyn:TransDate>{fn:data($input/ns1:fechaTransaccion)}</dyn:TransDate>
            </dyn:GTDCustPaymentLineContract>
        </ns2:_paymentLines>
    </ns2:GTDCustPaymentsServiceCreatePaymentsRequest>
};

local:func($input)