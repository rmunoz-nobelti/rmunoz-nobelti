xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/WSDL/NTLMClientService.wsdl" ::)
declare namespace ns2="http://tempuri.org";
(:: import schema at "../WS/Schemas/XMLSchema_2036091149.xsd" ::)

declare namespace dyn = "http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application";

declare variable $input as element() (:: schema-element(ns2:GTDCustPaymentsServiceCreatePaymentsRequest) ::) external;

declare function local:func($input as element() (:: schema-element(ns2:GTDCustPaymentsServiceCreatePaymentsRequest) ::)) as element() (:: schema-element(ns1:reqNTLM) ::) {
    <ns1:reqNTLM>
        <ns1:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDCustIntegrationServices/xppservice.svc</ns1:wsURL>
        <ns1:host>aosaifsrv01</ns1:host>
        <ns1:user>svsbus</ns1:user>
        <ns1:password>cachito</ns1:password>
        <ns1:soapAction>http://tempuri.org/GTDCustPaymentsService/createPayments</ns1:soapAction>
        <ns1:xmlInput>{<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:tem="http://tempuri.org" xmlns:dyn="http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application">
   <soapenv:Header>
      <dat:CallContext>
         <!--Optional:-->
         <dat:Company>tld</dat:Company>
         <!--Optional:-->
         <dat:Language/>
         <!--Optional:-->
         <dat:LogonAsUser/>
         <!--Optional:-->
         <dat:MessageId/>
         <!--Optional:-->
         <dat:PartitionKey/>
         <!--Optional:-->
         <dat:PropertyBag>
            <!--Zero or more repetitions:-->
            <arr:KeyValueOfstringstring>
               <arr:Key/>
               <arr:Value/>
            </arr:KeyValueOfstringstring>
         </dat:PropertyBag>
      </dat:CallContext>
   </soapenv:Header>
   <soapenv:Body>
      <tem:GTDCustPaymentsServiceCreatePaymentsRequest>
         <!--Optional:-->
         <tem:_companyId>{fn:data($input/ns2:_companyId)}</tem:_companyId>
         <!--Optional:-->
         <tem:_paymentLines>
            <!--Zero or more repetitions:-->
            <dyn:GTDCustPaymentLineContract>
               <!--Optional:-->
               <dyn:AccountType>{fn:data($input/ns2:_paymentLines/dyn:GTDCustPaymentLineContract/dyn:AccountType)}</dyn:AccountType>
               <!--Optional:-->
               <dyn:Company>{fn:data($input/ns2:_paymentLines/dyn:GTDCustPaymentLineContract/dyn:Company)}</dyn:Company>
               <!--Optional:-->
               <dyn:DocumentDate>{fn:data($input/ns2:_paymentLines/dyn:GTDCustPaymentLineContract/dyn:DocumentDate)}</dyn:DocumentDate>
               <!--Optional:-->
               <dyn:DocumentNum>2016-01-01</dyn:DocumentNum>
               <!--Optional:-->
               <dyn:LedgerDimension>{fn:data($input/ns2:_paymentLines/dyn:GTDCustPaymentLineContract/dyn:LedgerDimension)}</dyn:LedgerDimension>
               <!--Optional:-->
               <dyn:Lines>
               {
                  for $GTDCustPaymentOpenTransContract in $input/ns2:_paymentLines/dyn:GTDCustPaymentLineContract/dyn:Lines/dyn:GTDCustPaymentOpenTransContract
                  return
                  <dyn:GTDCustPaymentOpenTransContract>
                     <dyn:SettleAmount>{fn:data($GTDCustPaymentOpenTransContract/dyn:SettleAmount)}</dyn:SettleAmount>
                     <dyn:SubGroupId>{fn:data($GTDCustPaymentOpenTransContract/dyn:SubGroupId)}</dyn:SubGroupId>
                     <dyn:VoucherBSS>{fn:data($GTDCustPaymentOpenTransContract/dyn:VoucherBSS)}</dyn:VoucherBSS>
                  </dyn:GTDCustPaymentOpenTransContract>
                }
               </dyn:Lines>
               <!--Optional:-->
               <dyn:OrderAccount>{fn:data($input/ns2:_paymentLines/dyn:GTDCustPaymentLineContract/dyn:OrderAccount)}</dyn:OrderAccount>
               <!--Optional:-->
               <dyn:PaymMode>{fn:data($input/ns2:_paymentLines/dyn:GTDCustPaymentLineContract/dyn:PaymMode)}</dyn:PaymMode>
               <!--Optional:-->
               <dyn:PaymReference>{fn:data($input/ns2:_paymentLines/dyn:GTDCustPaymentLineContract/dyn:PaymReference)}</dyn:PaymReference>
               <!--Optional:-->
               <dyn:TransDate>{fn:data($input/ns2:_paymentLines/dyn:GTDCustPaymentLineContract/dyn:TransDate)}</dyn:TransDate>
            </dyn:GTDCustPaymentLineContract>
         </tem:_paymentLines>
      </tem:GTDCustPaymentsServiceCreatePaymentsRequest>
   </soapenv:Body>
</soapenv:Envelope>
}</ns1:xmlInput>
    </ns1:reqNTLM>
};

local:func($input)