xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/WSDL/NTLMClientService.wsdl" ::)
declare namespace ns1="http://tempuri.org";
(:: import schema at "../WS/Schemas/XMLSchema_-237096527.xsd" ::)

declare namespace dyn = "http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application";

declare variable $input as element() (:: schema-element(ns1:GTDInventTransferJournalPostServCreatingJournalHeaderRequest) ::) external;

declare function local:XQ_GTDTransferJournalLocation_TO_NTLMClient($input as element() (:: schema-element(ns1:GTDInventTransferJournalPostServCreatingJournalHeaderRequest) ::)) as element() (:: schema-element(ns2:reqNTLM) ::) {
    <ns2:reqNTLM>
        <ns2:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDTransferJournalLocation/xppservice.svc</ns2:wsURL>
        <ns2:host>axtest</ns2:host>
        <ns2:user>{fn:data("GTDDevelopment.com\\svsbus")}</ns2:user>
        <ns2:password>cachito</ns2:password>
        <ns2:soapAction>http://tempuri.org/GTDInventTransferJournalPostServ/CreatingJournalHeader</ns2:soapAction>
        <ns2:xmlInput>{
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:tem="http://tempuri.org" xmlns:dyn="http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application">
             <soapenv:Header>
                <dat:CallContext>
                   <!--Optional:-->
                   <dat:Company>TLD</dat:Company>
                   <!--Optional:-->
                   <dat:Language>ES</dat:Language>
                </dat:CallContext>
             </soapenv:Header>
             <soapenv:Body>
                <tem:GTDInventTransferJournalPostServCreatingJournalHeaderRequest>
                   <tem:_journalHeader>
                      <dyn:JournalBLines>
                         <dyn:GTDInventTransJournalLinesContract>
                            <dyn:ItemId>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventTransJournalLinesContract/dyn:ItemId)}</dyn:ItemId>
                            <dyn:Qty>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventTransJournalLinesContract/dyn:Qty)}</dyn:Qty>
                            <dyn:TransDate>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventTransJournalLinesContract/dyn:TransDate)}</dyn:TransDate>
                            <dyn:parmGTDToInventLocation>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventTransJournalLinesContract/dyn:parmGTDToInventLocation)}</dyn:parmGTDToInventLocation>
                            <dyn:parmGTDToInventSiteId>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventTransJournalLinesContract/dyn:parmGTDToInventSiteId)}</dyn:parmGTDToInventSiteId>
                            <dyn:parmGTDToInventStatusId>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventTransJournalLinesContract/dyn:parmGTDToInventStatusId)}</dyn:parmGTDToInventStatusId>
                            <dyn:parmGTDToWMSInventLocationId>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventTransJournalLinesContract/dyn:parmGTDToWMSInventLocationId)}</dyn:parmGTDToWMSInventLocationId>
                            <dyn:parmInventLocationId>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventTransJournalLinesContract/dyn:parmInventLocationId)}</dyn:parmInventLocationId>
                            <dyn:parmInventSiteId>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventTransJournalLinesContract/dyn:parmInventSiteId)}</dyn:parmInventSiteId>
                            <dyn:parmInventStatusId>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventTransJournalLinesContract/dyn:parmInventStatusId)}</dyn:parmInventStatusId>
                            <dyn:parmWMSLocationId>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventTransJournalLinesContract/dyn:parmWMSLocationId)}</dyn:parmWMSLocationId>
                         </dyn:GTDInventTransJournalLinesContract>
                      </dyn:JournalBLines>
                      <dyn:journalATable>
                         <dyn:GTDInventTransJournalHeaderContract/>
                      </dyn:journalATable>
                      <dyn:parmACCIssuePointDescription>{fn:data($input/ns1:_journalHeader/dyn:parmACCIssuePointDescription)}</dyn:parmACCIssuePointDescription>
                      <dyn:parmACCTradeDocumentId>{fn:data($input/ns1:_journalHeader/dyn:parmACCTradeDocumentId)}</dyn:parmACCTradeDocumentId>
                      <dyn:parmACCVendAccount>{fn:data($input/ns1:_journalHeader/dyn:parmACCVendAccount)}</dyn:parmACCVendAccount>
                      <dyn:parmDescription>{fn:data($input/ns1:_journalHeader/dyn:parmDescription)}</dyn:parmDescription>
                      <dyn:parmJournalNameId>{fn:data($input/ns1:_journalHeader/dyn:parmJournalNameId)}</dyn:parmJournalNameId>
                      <dyn:parmNO2CLDeliveryName>{fn:data($input/ns1:_journalHeader/dyn:parmNO2CLDeliveryName)}</dyn:parmNO2CLDeliveryName>
                      <dyn:parmNO2CLPackingSlipAddress>{fn:data($input/ns1:_journalHeader/dyn:parmNO2CLPackingSlipAddress)}</dyn:parmNO2CLPackingSlipAddress>
                   </tem:_journalHeader>
                </tem:GTDInventTransferJournalPostServCreatingJournalHeaderRequest>
             </soapenv:Body>
          </soapenv:Envelope>
        }</ns2:xmlInput>
    </ns2:reqNTLM>
};

local:XQ_GTDTransferJournalLocation_TO_NTLMClient($input)