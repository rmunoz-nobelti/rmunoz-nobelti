xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.grupogtd.com/schema/utility/NTLM/client";
(:: import schema at "../../../UTL_NTLMClient/ProxyServices/WSDL/NTLMClientService.wsdl" ::)
declare namespace ns1="http://tempuri.org";
(:: import schema at "../WS/Schemas/XMLSchema_-1011359736.xsd" ::)

declare namespace dyn = "http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application";

declare variable $input as element() (:: schema-element(ns1:GTDInventJournalPostServiceCreatingJournalHeaderRequest) ::) external;

declare function local:XQ_GTDConsumoAutomatico_TO_NTLMClient($input as element() (:: schema-element(ns1:GTDInventJournalPostServiceCreatingJournalHeaderRequest) ::)) as element() (:: schema-element(ns2:reqNTLM) ::) {
    <ns2:reqNTLM>
        <ns2:wsURL>http://aosaifsrv01/MicrosoftDynamicsAXAif60/GTDConsumoAutomatico/xppservice.svc</ns2:wsURL>
        <ns2:host>axtest</ns2:host>
        <ns2:user>{fn:data("GTDDevelopment.com\\svsbus")}</ns2:user>
        <ns2:password>cachito</ns2:password>
        <ns2:soapAction>http://tempuri.org/GTDInventJournalPostService/CreatingJournalHeader</ns2:soapAction>
        <ns2:xmlInput>
          {
            <soapenv:Envelope xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:dyn="http://schemas.datacontract.org/2004/07/Dynamics.Ax.Application" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org">
	<soapenv:Header>
		<dat:CallContext>
		<dat:Company>TLD</dat:Company>
		<dat:Language>ES</dat:Language>
		</dat:CallContext>
	</soapenv:Header>
	<soapenv:Body>
		<tem:GTDInventJournalPostServiceCreatingJournalHeaderRequest>
		<tem:_journalHeader>
			<dyn:JournalBLines>
				<dyn:GTDInventJournalLinesContract>
					<dyn:ItemId>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventJournalLinesContract/dyn:ItemId)}</dyn:ItemId>
					<dyn:Qty>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventJournalLinesContract/dyn:Qty)}</dyn:Qty>
					<dyn:parmInventLocationId>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventJournalLinesContract/dyn:parmInventLocationId)}</dyn:parmInventLocationId>
					<dyn:parmInventSiteId>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventJournalLinesContract/dyn:parmInventSiteId)}</dyn:parmInventSiteId>
					<dyn:parmInventStatusId>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventJournalLinesContract/dyn:parmInventStatusId)}</dyn:parmInventStatusId>
					<dyn:parmTransDate>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventJournalLinesContract/dyn:parmTransDate)}</dyn:parmTransDate>
					<dyn:parmWMSLocationId>{fn:data($input/ns1:_journalHeader/dyn:JournalBLines/dyn:GTDInventJournalLinesContract/dyn:parmWMSLocationId)}</dyn:parmWMSLocationId>
				</dyn:GTDInventJournalLinesContract>
			</dyn:JournalBLines>
			<dyn:parmACCIssuePointDescription>{fn:data($input/ns1:_journalHeader/dyn:parmACCIssuePointDescription)}</dyn:parmACCIssuePointDescription>
                        <dyn:parmACCTradeDocumentId>{fn:data($input/ns1:_journalHeader/dyn:parmACCTradeDocumentId)}</dyn:parmACCTradeDocumentId>
			<dyn:parmACCVendAccount>{fn:data($input/ns1:_journalHeader/dyn:parmACCVendAccount)}</dyn:parmACCVendAccount>
                        <dyn:parmDescription>{fn:data($input/ns1:_journalHeader/dyn:parmDescription)}</dyn:parmDescription>
                        <dyn:parmGTD_DIMCostCenter>{fn:data($input/ns1:_journalHeader/dyn:parmGTD_DIMCostCenter)}</dyn:parmGTD_DIMCostCenter>
                        <dyn:parmGTD_DimActi_Instal>{fn:data($input/ns1:_journalHeader/dyn:parmGTD_DimActi_Instal)}</dyn:parmGTD_DimActi_Instal>
                        <dyn:parmGTD_DimZona>{fn:data($input/ns1:_journalHeader/dyn:parmGTD_DimZona)}</dyn:parmGTD_DimZona>
			<dyn:parmJournalNameId>{fn:data($input/ns1:_journalHeader/dyn:parmJournalNameId)}</dyn:parmJournalNameId>
			<dyn:parmNO2CLDeliveryName>{fn:data($input/ns1:_journalHeader/dyn:parmNO2CLDeliveryName)}</dyn:parmNO2CLDeliveryName>
			<dyn:parmNO2CLPackingSlipAddress>{fn:data($input/ns1:_journalHeader/dyn:parmNO2CLPackingSlipAddress)}</dyn:parmNO2CLPackingSlipAddress>
		</tem:_journalHeader>
	</tem:GTDInventJournalPostServiceCreatingJournalHeaderRequest>
	</soapenv:Body>
</soapenv:Envelope>

          }
        </ns2:xmlInput>
    </ns2:reqNTLM>
};

local:XQ_GTDConsumoAutomatico_TO_NTLMClient($input)