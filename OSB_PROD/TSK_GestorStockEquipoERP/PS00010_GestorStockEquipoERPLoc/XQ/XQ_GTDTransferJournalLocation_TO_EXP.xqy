xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://tempuri.org";
(:: import schema at "../WS/Schemas/XMLSchema_-237096527.xsd" ::)

declare variable $input as element() (:: schema-element(ns1:GTDInventTransferJournalPostServCreatingJournalHeaderResponse) ::) external;

declare function local:func($input as element() (:: schema-element(ns1:GTDInventTransferJournalPostServCreatingJournalHeaderResponse) ::)) as element() (:: schema-element(ns1:GTDInventTransferJournalPostServCreatingJournalHeaderResponse) ::) {
    <ns1:GTDInventTransferJournalPostServCreatingJournalHeaderResponse>
        {
            if ($input/ns1:response)
            then <ns1:response>{fn:string("0")}</ns1:response>
            else ()
        }
    </ns1:GTDInventTransferJournalPostServCreatingJournalHeaderResponse>
};

local:func($input)