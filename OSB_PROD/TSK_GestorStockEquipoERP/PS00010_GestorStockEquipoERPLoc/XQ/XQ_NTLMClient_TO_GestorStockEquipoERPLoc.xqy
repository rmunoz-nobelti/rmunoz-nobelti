xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/AX/response";
(:: import schema at "../../../UT_Common/Schemas/responseAX.xsd" ::)

declare variable $code as xs:string external;
declare variable $string as xs:string external;

declare function local:func($code as xs:string, 
                            $string as xs:string) 
                            as element() (:: schema-element(ns1:AXResponse) ::) {
    <ns1:AXResponse>
        <ns1:responseCode>{fn:data($code)}</ns1:responseCode>
        <ns1:responseString>{fn:data($string)}</ns1:responseString>
    </ns1:AXResponse>
};

local:func($code, $string)