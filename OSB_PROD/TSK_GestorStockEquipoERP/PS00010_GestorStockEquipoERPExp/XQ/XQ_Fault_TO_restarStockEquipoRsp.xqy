xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/GestorStockEquipoERP/restarStockEquipo";
(:: import schema at "../XSD/restarStockEquipo.xsd" ::)

declare variable $errorCodigo as xs:string external;
declare variable $errorTipo as xs:string external;
declare variable $errorDescripcion as xs:string external;

declare function local:func($errorCodigo as xs:string, 
                            $errorTipo as xs:string, 
                            $errorDescripcion as xs:string) 
                            as element() (:: schema-element(ns1:restarStockEquipoRsp) ::) {
    <ns1:restarStockEquipoRsp>
        <ns1:errorCodigo>{fn:data($errorCodigo)}</ns1:errorCodigo>
        <ns1:errorTipo>{fn:data($errorTipo)}</ns1:errorTipo>
        <ns1:errorDescripcion>{fn:data($errorDescripcion)}</ns1:errorDescripcion>
    </ns1:restarStockEquipoRsp>
};

local:func($errorCodigo, $errorTipo, $errorDescripcion)