xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/AX/response";
(:: import schema at "../../../UT_Common/Schemas/responseAX.xsd" ::)
declare namespace ns2="http://cl.grupogtd.com/schema/GestorStockEquipoERP/restarStockEquipo";
(:: import schema at "../XSD/restarStockEquipo.xsd" ::)

declare variable $input as element() (:: schema-element(ns1:AXResponse) ::) external;

declare function local:func($input as element() (:: schema-element(ns1:AXResponse) ::)) as element() (:: schema-element(ns2:restarStockEquipoRsp) ::) {
    <ns2:restarStockEquipoRsp>
        <ns2:errorCodigo>{fn:data($input/ns1:responseCode)}</ns2:errorCodigo>
        <ns2:errorDescripcion>{fn:data($input/ns1:responseString)}</ns2:errorDescripcion>
    </ns2:restarStockEquipoRsp>
};

local:func($input)