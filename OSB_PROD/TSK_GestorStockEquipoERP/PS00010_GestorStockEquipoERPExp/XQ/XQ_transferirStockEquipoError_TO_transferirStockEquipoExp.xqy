xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/GestorStockEquipoERP/transferirStockEquipo";
(:: import schema at "../XSD/transferirStockEquipo.xsd" ::)

declare variable $reason as xs:string external;

declare function local:func($reason as xs:string) as element() (:: schema-element(ns1:transferirStockEquipoRsp) ::) {
    if (fn:contains($reason,"nonPositiveInteger")) then
        <ns1:transferirStockEquipoRsp>
            <ns1:errorCodigo>V002</ns1:errorCodigo>
            <ns1:errorTipo>Validación</ns1:errorTipo>
            <ns1:errorDescripcion>Error en el dato cantidad.</ns1:errorDescripcion>
        </ns1:transferirStockEquipoRsp>
    else if (fn:contains($reason,"codigoSKU")) then
          <ns1:restarStockEquipoRsp>
              <ns1:errorCodigo>V003</ns1:errorCodigo>
              <ns1:errorTipo>Validación</ns1:errorTipo>
              <ns1:errorDescripcion>Debe ingresar el campo codigoSKU.</ns1:errorDescripcion>
          </ns1:restarStockEquipoRsp>
    else
       <ns1:transferirStockEquipoRsp>
            <ns1:errorCodigo>V001</ns1:errorCodigo>
            <ns1:errorTipo>Validación</ns1:errorTipo>
            <ns1:errorDescripcion>Error en los datos de entrada.</ns1:errorDescripcion>
        </ns1:transferirStockEquipoRsp>
};

local:func($reason)