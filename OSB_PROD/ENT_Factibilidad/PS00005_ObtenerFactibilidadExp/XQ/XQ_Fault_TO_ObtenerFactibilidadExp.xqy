xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/Factibilidad/obtenerFactibilidadRspOp";
(:: import schema at "../XSD/obtenerFactibilidadRsp.xsd" ::)

declare variable $errorCod as xs:string external;
declare variable $errorTip as xs:string external;
declare variable $errorDesc as xs:string external;

declare function local:XQ_Fault_TO_FactibilidadObtenerExp($errorCod as xs:string, 
                                                          $errorTip as xs:string, 
                                                          $errorDesc as xs:string) 
                                                          as element() (:: schema-element(ns1:obtenerFactibilidadRsp) ::) {
    <ns1:factibilidadObtenerRsp>
      <ns1:errorCodigo>{fn:data($errorCod)}</ns1:errorCodigo>
      <ns1:errorTipo>{fn:data($errorTip)}</ns1:errorTipo>
      <ns1:errorDescripcion>{fn:data($errorDesc)}</ns1:errorDescripcion>
      <ns1:MontosFactibilidad></ns1:MontosFactibilidad>
    </ns1:factibilidadObtenerRsp>
};

local:XQ_Fault_TO_FactibilidadObtenerExp($errorCod, $errorTip, $errorDesc)