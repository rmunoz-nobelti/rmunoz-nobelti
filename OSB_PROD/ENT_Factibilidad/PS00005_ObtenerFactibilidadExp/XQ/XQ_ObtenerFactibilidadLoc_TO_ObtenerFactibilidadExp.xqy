xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.grupogtd.com/schema/Factibilidad/obtenerFactibilidadRspOp";
(:: import schema at "../XSD/obtenerFactibilidadRsp.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/SP_CORPORATIVO_TELEDUCTOS_CL_PGET_COSTOS_FACTIBILIDAD";
(:: import schema at "../../PS00005_ObtenerFactibilidadLoc/JCA/Schemas/SP_CORPORATIVO_TELEDUCTOS_CL_PGET_COSTOS_FACTIBILIDAD_sp.xsd" ::)

declare variable $OutputParam as element() (:: schema-element(ns1:OutputParameters) ::) external;

declare function local:XQ_FactibilidadObtenerLoc_TO_FactibilidadObtenerExp($OutputParam as element() (:: schema-element(ns1:OutputParameters) ::)) as element() (:: schema-element(ns2:obtenerFactibilidadRsp) ::) {
    <ns2:factibilidadObtenerRsp>
        {
            if ($OutputParam/ns1:CODIGO_ERROR_OUT)
            then <ns2:errorCodigo>{fn:data($OutputParam/ns1:CODIGO_ERROR_OUT)}</ns2:errorCodigo>
            else ()
        }

        <ns2:errorTipo></ns2:errorTipo>

        {
            if ($OutputParam/ns1:GLS_ERR_OUT)
            then <ns2:errorDescripcion>{fn:data($OutputParam/ns1:GLS_ERR_OUT)}</ns2:errorDescripcion>
            else ()
        }
        {
            for $Row in $OutputParam/ns1:MONTOS_FACTIBILIDAD/ns1:Row
            return 
            <ns2:MontosFactibilidad>
              <ns2:manoObraCables>{fn:data($Row/ns1:Column[@name='MANOOBRACABLE']/text())}</ns2:manoObraCables>
              <ns2:manoObraObrasCiviles>{fn:data($Row/ns1:Column[@name='MANOOBRAOBRACIVIL']/text())}</ns2:manoObraObrasCiviles>
              <ns2:materialesCables>{fn:data($Row/ns1:Column[@name='VALORCABLE']/text())}</ns2:materialesCables>
              <ns2:materialesObrasCiviles>{fn:data($Row/ns1:Column[@name='VALOROBRACIVIL']/text())}</ns2:materialesObrasCiviles>
              <ns2:permisosCables>{fn:data($Row/ns1:Column[@name='PERMISOCABLE']/text())}</ns2:permisosCables>
              <ns2:permisosObrasCiviles>{fn:data($Row/ns1:Column[@name='PERMISOOBRACIVIL']/text())}</ns2:permisosObrasCiviles>
            </ns2:MontosFactibilidad>
        }
    </ns2:factibilidadObtenerRsp>
};

local:XQ_FactibilidadObtenerLoc_TO_FactibilidadObtenerExp($OutputParam)