xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/Factibilidad/obtenerFactibilidadReqOp";
(:: import schema at "../XSD/obtenerFactibilidadReq.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/SP_CORPORATIVO_TELEDUCTOS_CL_PGET_COSTOS_FACTIBILIDAD";
(:: import schema at "../../PS00005_ObtenerFactibilidadLoc/JCA/Schemas/SP_CORPORATIVO_TELEDUCTOS_CL_PGET_COSTOS_FACTIBILIDAD_sp.xsd" ::)

declare variable $inputParam as element() (:: schema-element(ns1:obtenerFactibilidadReq) ::) external;

declare function local:XQ_FactibilidadObtenerExp_TO_FactibilidadObtenerLoc($inputParam as element() (:: schema-element(ns1:obtenerFactibilidadReq) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
      <ns2:VNUMEROFACTIBILIDAD>{fn:data($inputParam/ns1:numeroFactibilidad)}</ns2:VNUMEROFACTIBILIDAD>
    </ns2:InputParameters>
};

local:XQ_FactibilidadObtenerExp_TO_FactibilidadObtenerLoc($inputParam)