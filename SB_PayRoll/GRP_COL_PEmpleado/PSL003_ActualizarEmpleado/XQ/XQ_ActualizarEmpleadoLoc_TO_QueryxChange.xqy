xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.gtd.com/actualizarEmpleadoLocRequest";
(:: import schema at "../XSD/actualizarEmpleadoLocRequest.xsd" ::)
declare namespace ns2="http://cl.gtd.com/empleadoRequest";
(:: import schema at "../../Resources/XSD/empleadoRequest.xsd" ::)

declare variable $actualizarEmpleado as element() (:: schema-element(ns1:actualizarEmpleado) ::) external;

declare function local:func($actualizarEmpleado as element() (:: schema-element(ns1:actualizarEmpleado) ::)) as element() (:: schema-element(ns2:empleadoRequest) ::) {
    <ns2:actualizarEmpleadoRequest>
        <ns2:inInexCod>2</ns2:inInexCod>
        <ns2:clXMLSolic>{fn-bea:serialize($actualizarEmpleado/Interfaz2Solic)}</ns2:clXMLSolic>
    </ns2:actualizarEmpleadoRequest>
};

local:func($actualizarEmpleado)