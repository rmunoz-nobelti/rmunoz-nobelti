xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.gtd.com/retirarEmpleadoLocRequest";
(:: import schema at "../../PSL004_RetirarEmpleado/XSD/retirarEmpleadoLocRequest.xsd" ::)
declare namespace ns1="http://cl.gtd.com/schemas/retirarEmpleadoRequest";
(:: import schema at "../XSD/retirarEmpleadoExpRequest.xsd" ::)

declare variable $retirarEmpleadoReq as element() (:: schema-element(ns1:retirarEmpleadoRequest) ::) external;

declare function local:func($retirarEmpleadoReq as element() (:: schema-element(ns1:retirarEmpleadoRequest) ::)) as element() (:: schema-element(ns2:retirarEmpleado) ::) {
    <ns2:retirarEmpleado>
        <Interfaz3Solic>
            <UNICO>
                <EMP_CODIGO>{fn:data($retirarEmpleadoReq/ns1:codigo)}</EMP_CODIGO>
                <PDEF_FECORTE>{fn-bea:date-to-string-with-format("yyyy-mm-dd",$retirarEmpleadoReq/ns1:pdefFechaCorte)}</PDEF_FECORTE>
                <CAUSA_RETIRO>{fn:data($retirarEmpleadoReq/ns1:causaRetiro)}</CAUSA_RETIRO>
            </UNICO>
        </Interfaz3Solic>
    </ns2:retirarEmpleado>
};

local:func($retirarEmpleadoReq)