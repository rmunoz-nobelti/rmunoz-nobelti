xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.gtd.com/actualizarEmpleadoLocRequest";
(:: import schema at "../../PSL003_ActualizarEmpleado/XSD/actualizarEmpleadoLocRequest.xsd" ::)
declare namespace ns1="http://cl.gtd.com/schemas/actualizarEmpleadoRequest";
(:: import schema at "../XSD/actualizarEmpleadoExpRequest.xsd" ::)

declare variable $actualizarEmpleadoReq as element() (:: schema-element(ns1:actualizarEmpleadoRequest) ::) external;

declare function local:func($actualizarEmpleadoReq as element() (:: schema-element(ns1:actualizarEmpleadoRequest) ::)) as element() (:: schema-element(ns2:actualizarEmpleado) ::) {
    <ns2:actualizarEmpleado>
        <Interfaz2Solic>
            <UNICO>
                <EMP_CODIGO>{fn:data($actualizarEmpleadoReq/ns1:codigo)}</EMP_CODIGO>
                <EMP_APELLIDO1>{fn:substring($actualizarEmpleadoReq/ns1:apellidoPaterno,1,15)}</EMP_APELLIDO1>
                <EMP_APELLIDO2>{fn:substring($actualizarEmpleadoReq/ns1:apellidoMaterno,1,15)}</EMP_APELLIDO2>
                <EMP_NOMBRE>{fn:substring(fn:concat($actualizarEmpleadoReq/ns1:primerNombre," ",$actualizarEmpleadoReq/ns1:segundoNombre),1,30)}</EMP_NOMBRE>
                <EMP_TIPO_IDENTIF>{fn:data($actualizarEmpleadoReq/ns1:tipoIdentificador)}</EMP_TIPO_IDENTIF>
                <EMP_SEXO>{dvmtr:lookup('UT_Common/DVM/Sexo', 'Codigo_AX', fn:data($actualizarEmpleadoReq/ns1:sexo/text()), 'Codigo_Payroll', 'No se encontró valor para campo Sexo.')}</EMP_SEXO>
                <EMP_ESTADO_CIVIL>{dvmtr:lookup('UT_Common/DVM/EstadoCivil', 'Codigo_AX', fn:data($actualizarEmpleadoReq/ns1:estadoCivil/text()), 'Codigo_Payroll', 'No se encontró valor para campo Estado Civil.')}</EMP_ESTADO_CIVIL>
                <UGN1_CODIGO_IDENT>{fn:data($actualizarEmpleadoReq/ns1:codigoPaisIdentificacion)}</UGN1_CODIGO_IDENT>
                <UGN1_CODIGO_NACI>{fn:data($actualizarEmpleadoReq/ns1:codigoPaisNacimiento)}</UGN1_CODIGO_NACI>
                <UGN1_CODIGO_RESID>{fn:data($actualizarEmpleadoReq/ns1:codigoPaisResidencia)}</UGN1_CODIGO_RESID>
                <UGN2_CODIGO_RESID>{fn:data($actualizarEmpleadoReq/ns1:codigoDepartamentoResidencia)}</UGN2_CODIGO_RESID>
                <UGN3_CODIGO_RESID>{concat(data($actualizarEmpleadoReq/ns1:codigoCiudadResidencia),data($actualizarEmpleadoReq/ns1:codigoDepartamentoResidencia))}</UGN3_CODIGO_RESID>
                <EMP_FECHA_NACI>{fn-bea:date-to-string-with-format("yyyy-mm-dd",$actualizarEmpleadoReq/ns1:fechaNacimiento)}</EMP_FECHA_NACI>
                <EMP_LOCALIDAD>{fn:data($actualizarEmpleadoReq/ns1:localidad)}</EMP_LOCALIDAD>
                <EMP_CC_CONTABLE>{fn:data($actualizarEmpleadoReq/ns1:centroCostoContable)}</EMP_CC_CONTABLE>
                <CENCOS_NOMBRE>{fn:data($actualizarEmpleadoReq/ns1:descripcionCentroCosto)}</CENCOS_NOMBRE>
                <EMP_TIPO_NOMBRAMIENTO>{fn:data($actualizarEmpleadoReq/ns1:tipoNombramiento)}</EMP_TIPO_NOMBRAMIENTO>
                <EMP_FUENTE_RECLUTAMIENTO>{fn:data($actualizarEmpleadoReq/ns1:fuenteReclutamiento)}</EMP_FUENTE_RECLUTAMIENTO>
                <EMP_CLASE_EMPLEADO>{fn:data($actualizarEmpleadoReq/ns1:claseEmpleado)}</EMP_CLASE_EMPLEADO>
                <EMP_TIPO_CONTRATO>{fn:data($actualizarEmpleadoReq/ns1:tipoContrato)}</EMP_TIPO_CONTRATO>
                <EMP_FECHA_FIN_CONTRATO>{fn-bea:date-to-string-with-format("yyy-mm-dd",$actualizarEmpleadoReq/ns1:fechaFinContrato)}</EMP_FECHA_FIN_CONTRATO>
                <EMP_CARGO>{fn:data($actualizarEmpleadoReq/ns1:cargo)}</EMP_CARGO>
                <CARGO_NOMBRE>{fn:data($actualizarEmpleadoReq/ns1:cargoNombre)}</CARGO_NOMBRE>
                <EMP_FORMA_PAGO>{dvmtr:lookup('UT_Common/DVM/FormaPago', 'Codigo_AX', fn:data($actualizarEmpleadoReq/ns1:formaPago/text()), 'Codigo_Payroll', 'No se encontró valor para campo Forma Pago.')}</EMP_FORMA_PAGO>
                <EMP_BANCO_CODIGO>{fn:data($actualizarEmpleadoReq/ns1:bancoCodigo)}</EMP_BANCO_CODIGO>
               <EMP_TIPO_CUENTA>{dvmtr:lookup('UT_Common/DVM/TipoCuenta', 'Codigo_AX', fn:data($actualizarEmpleadoReq/ns1:tipoCuenta/text()), 'Codigo_Payroll', 'No se encontró valor para campo Tipo Cuenta.')}</EMP_TIPO_CUENTA>
                <EMP_CTACTE>{fn:data($actualizarEmpleadoReq/ns1:cuentaCorriente)}</EMP_CTACTE>
                <EMP_DIRECCION>{fn:data($actualizarEmpleadoReq/ns1:direccion)}</EMP_DIRECCION>
                <EMP_TELEFONO>{fn:data($actualizarEmpleadoReq/ns1:telefono)}</EMP_TELEFONO>
                <EMP_EMAIL>{fn:data($actualizarEmpleadoReq/ns1:email)}</EMP_EMAIL>
            </UNICO>
        </Interfaz2Solic>
    </ns2:actualizarEmpleado>
};

local:func($actualizarEmpleadoReq)