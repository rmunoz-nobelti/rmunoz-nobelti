xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.gtd.com/existeCedulaLocRequest";
(:: import schema at "../../PSL001_ExisteCedula/XSD/existeCedulaLocRequest.xsd" ::)
declare namespace ns1="http://cl.gtd.com/schemas/existeCedulaRequest";
(:: import schema at "../XSD/existeCedulaExpRequest.xsd" ::)

declare variable $existeCedulaExpRequest as element() (:: schema-element(ns1:existeCedulaRequest) ::) external;

declare function local:func($existeCedulaExpRequest as element() (:: schema-element(ns1:existeCedulaRequest) ::)) as element() (:: schema-element(ns2:existeCedula) ::) {
    <ns2:existeCedula>
        <Interfaz99Solic>
            <Params>
                <CEDULA>{fn:data($existeCedulaExpRequest/ns1:cedula)}</CEDULA>
            </Params>
        </Interfaz99Solic>
    </ns2:existeCedula>
};

local:func($existeCedulaExpRequest)
