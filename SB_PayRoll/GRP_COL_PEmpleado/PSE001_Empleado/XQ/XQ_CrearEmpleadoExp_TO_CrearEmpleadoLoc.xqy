xquery version "1.0" encoding "ISO-8859-15";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://cl.gtd.com/crearEmpleadoLocRequest";
(:: import schema at "../../PSL002_CrearEmpleado/XSD/crearEmpleadoLocRequest.xsd" ::)
declare namespace ns1="http://cl.gtd.com/schemas/crearEmpleadoRequest";
(:: import schema at "../XSD/crearEmpleadoExpRequest.xsd" ::)

declare variable $crearEmpleadoReq as element() (:: schema-element(ns1:crearEmpleadoRequest) ::) external;

declare function local:func($crearEmpleadoReq as element() (:: schema-element(ns1:crearEmpleadoRequest) ::)) as element() (:: schema-element(ns2:crearEmpleado) ::) {
    <ns2:crearEmpleado>
        <Interfaz1Solic>
            <UNICO>
            
                <EMP_CODIGO>{fn:data($crearEmpleadoReq/ns1:codigo)}</EMP_CODIGO>
                <EMP_APELLIDO1>{fn:substring($crearEmpleadoReq/ns1:apellidoPaterno,1,15)}</EMP_APELLIDO1>
                <EMP_APELLIDO2>{fn:substring($crearEmpleadoReq/ns1:apellidoMaterno,1,15)}</EMP_APELLIDO2>
                <EMP_NOMBRE>{fn:substring(fn:concat($crearEmpleadoReq/ns1:primerNombre," ",$crearEmpleadoReq/ns1:segundoNombre),1,30)}</EMP_NOMBRE>
                <EMP_TIPO_IDENTIF>{fn:data($crearEmpleadoReq/ns1:tipoIdentificador)}</EMP_TIPO_IDENTIF>
                <EMP_SEXO>{dvmtr:lookup('UT_Common/DVM/Sexo', 'Codigo_AX', fn:data($crearEmpleadoReq/ns1:sexo/text()), 'Codigo_Payroll', 'No se encontr� valor para campo Sexo.')}</EMP_SEXO>
                <EMP_ESTADO_CIVIL>{dvmtr:lookup('UT_Common/DVM/EstadoCivil', 'Codigo_AX', fn:data($crearEmpleadoReq/ns1:estadoCivil/text()), 'Codigo_Payroll', 'No se encontr� valor para campo Estado Civil.')}</EMP_ESTADO_CIVIL>
                <UGN1_CODIGO_IDENT>{fn:data($crearEmpleadoReq/ns1:codigoPaisIdentificacion)}</UGN1_CODIGO_IDENT>
                <UGN1_CODIGO_NACI>{fn:data($crearEmpleadoReq/ns1:codigoPaisNacimiento)}</UGN1_CODIGO_NACI>
                <UGN1_CODIGO_RESID>{fn:data($crearEmpleadoReq/ns1:codigoPaisResidencia)}</UGN1_CODIGO_RESID>
                <UGN2_CODIGO_RESID>{fn:data($crearEmpleadoReq/ns1:codigoDepartamentoResidencia)}</UGN2_CODIGO_RESID>
                <UGN3_CODIGO_RESID>{concat(data($crearEmpleadoReq/ns1:codigoCiudadResidencia),data($crearEmpleadoReq/ns1:codigoDepartamentoResidencia))}</UGN3_CODIGO_RESID>
                <EMP_FECHA_NACI>{fn-bea:date-to-string-with-format("yyyy-mm-dd",$crearEmpleadoReq/ns1:fechaNacimiento)}</EMP_FECHA_NACI>
                <EMP_SOCIEDAD>{dvmtr:lookup('UT_Common/DVM/Sociedad', 'Codigo_AX', fn:data($crearEmpleadoReq/ns1:sociedad/text()), 'Codigo_Payroll', 'No se encontr� valor para campo Sociedad.')}</EMP_SOCIEDAD>
                <EMP_LOCALIDAD>{fn:data($crearEmpleadoReq/ns1:localidad)}</EMP_LOCALIDAD>
                <EMP_CC_CONTABLE>{fn:data($crearEmpleadoReq/ns1:centroCostoContable)}</EMP_CC_CONTABLE>
                <CENCOS_NOMBRE>{fn:data($crearEmpleadoReq/ns1:descripcionCentroCosto)}</CENCOS_NOMBRE>
                <EMP_TIPO_NOMINA>{fn:data($crearEmpleadoReq/ns1:tipoNomina)}</EMP_TIPO_NOMINA>
                <EMP_TIPO_NOMBRAMIENTO>{fn:data($crearEmpleadoReq/ns1:tipoNombramiento)}</EMP_TIPO_NOMBRAMIENTO>
                <EMP_FUENTE_RECLUTAMIENTO>{fn:data($crearEmpleadoReq/ns1:fuenteReclutamiento)}</EMP_FUENTE_RECLUTAMIENTO>
                <EMP_CLASE_EMPLEADO>{fn:data($crearEmpleadoReq/ns1:claseEmpleado)}</EMP_CLASE_EMPLEADO>
                <EMP_FECHA_INGRESO>{fn-bea:date-to-string-with-format("yyyy-mm-dd",$crearEmpleadoReq/ns1:fechaIngreso)}</EMP_FECHA_INGRESO>
                <EMP_TIPO_CONTRATO>{fn:data($crearEmpleadoReq/ns1:tipoContrato)}</EMP_TIPO_CONTRATO>
                <EMP_FECHA_FIN_CONTRATO>{fn-bea:date-to-string-with-format("yyy-mm-dd",$crearEmpleadoReq/ns1:fechaFinContrato)}</EMP_FECHA_FIN_CONTRATO>
                <EMP_CARGO>{fn:data($crearEmpleadoReq/ns1:cargo)}</EMP_CARGO>
                <CARGO_NOMBRE>{fn:data($crearEmpleadoReq/ns1:cargoNombre)}</CARGO_NOMBRE>
                <EMP_FORMA_PAGO>{dvmtr:lookup('UT_Common/DVM/FormaPago', 'Codigo_AX', fn:data($crearEmpleadoReq/ns1:formaPago/text()), 'Codigo_Payroll', 'No se encontr� valor para campo Forma Pago.')}</EMP_FORMA_PAGO>
                <EMP_BANCO_CODIGO>{fn:data($crearEmpleadoReq/ns1:bancoCodigo)}</EMP_BANCO_CODIGO>
               <EMP_TIPO_CUENTA>{dvmtr:lookup('UT_Common/DVM/TipoCuenta', 'Codigo_AX', fn:data($crearEmpleadoReq/ns1:tipoCuenta/text()), 'Codigo_Payroll', 'No se encontr� valor para campo Tipo Cuenta.')}</EMP_TIPO_CUENTA>
                <EMP_CTACTE>{fn:data($crearEmpleadoReq/ns1:cuentaCorriente)}</EMP_CTACTE>
                <EMP_DIRECCION>{fn:data($crearEmpleadoReq/ns1:direccion)}</EMP_DIRECCION>
                <EMP_TELEFONO>{fn:data($crearEmpleadoReq/ns1:telefono)}</EMP_TELEFONO>
                <EMP_EMAIL>{fn:data($crearEmpleadoReq/ns1:email)}</EMP_EMAIL>
            </UNICO>
        </Interfaz1Solic>
    </ns2:crearEmpleado>
};

local:func($crearEmpleadoReq)