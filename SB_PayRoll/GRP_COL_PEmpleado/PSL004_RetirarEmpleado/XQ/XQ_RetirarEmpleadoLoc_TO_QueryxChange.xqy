xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.gtd.com/retirarEmpleadoLocRequest";
(:: import schema at "../XSD/retirarEmpleadoLocRequest.xsd" ::)
declare namespace ns2="http://cl.gtd.com/empleadoRequest";
(:: import schema at "../../Resources/XSD/empleadoRequest.xsd" ::)

declare variable $retirarEmpleado as element() (:: schema-element(ns1:retirarEmpleado) ::) external;

declare function local:func($retirarEmpleado as element() (:: schema-element(ns1:retirarEmpleado) ::)) as element() (:: schema-element(ns2:empleadoRequest) ::) {
    <ns2:retirarEmpleadoRequest>
        <ns2:inInexCod>3</ns2:inInexCod>
        <ns2:clXMLSolic>{fn-bea:serialize($retirarEmpleado/Interfaz3Solic)}</ns2:clXMLSolic>
    </ns2:retirarEmpleadoRequest>
};

local:func($retirarEmpleado)