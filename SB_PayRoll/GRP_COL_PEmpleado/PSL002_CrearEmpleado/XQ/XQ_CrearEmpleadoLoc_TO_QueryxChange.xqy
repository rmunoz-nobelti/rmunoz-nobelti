xquery version "1.0" encoding "ISO-8859-15";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.gtd.com/crearEmpleadoLocRequest";
(:: import schema at "../XSD/crearEmpleadoLocRequest.xsd" ::)
declare namespace ns2="http://cl.gtd.com/empleadoRequest";
(:: import schema at "../../Resources/XSD/empleadoRequest.xsd" ::)

declare variable $crearEmpleado as element() (:: schema-element(ns1:crearEmpleado) ::) external;

declare function local:func($crearEmpleado as element() (:: schema-element(ns1:crearEmpleado) ::)) as element() (:: schema-element(ns2:empleadoRequest) ::) {
    <ns2:crearEmpleadoRequest>
        <ns2:inInexCod>1</ns2:inInexCod>
        <ns2:clXMLSolic>{fn-bea:serialize($crearEmpleado/Interfaz1Solic)}</ns2:clXMLSolic>
    </ns2:crearEmpleadoRequest>
};

local:func($crearEmpleado)