xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.gtd.com/existeCedulaLocRequest";
(:: import schema at "../XSD/existeCedulaLocRequest.xsd" ::)
declare namespace ns2="http://cl.gtd.com/empleadoRequest";
(:: import schema at "../../Resources/XSD/empleadoRequest.xsd" ::)

declare variable $existeCedulaLocReq as element() (:: schema-element(ns1:existeCedula) ::) external;

declare function local:func($existeCedulaLocReq as element() (:: schema-element(ns1:existeCedula) ::)) as element() (:: schema-element(ns2:empleadoRequest) ::) {
    <ns2:empleadoRequest>
        <ns2:inInexCod>99</ns2:inInexCod>
        <ns2:clXMLSolic>{fn-bea:serialize($existeCedulaLocReq/Interfaz99Solic)}</ns2:clXMLSolic>
        
        </ns2:empleadoRequest>
};

local:func($existeCedulaLocReq)
