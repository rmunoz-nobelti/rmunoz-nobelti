xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://cl.grupogtd.com/schema/common/headerRequest";
(:: import schema at "../Schemas/headerRequest.xsd" ::)
declare namespace ns2="http://cl.grupogtd.com/schema/common/headerResponse";
(:: import schema at "../Schemas/headerResponse.xsd" ::)

declare variable $imput as element() (:: schema-element(ns1:headerRequest) ::) external;

declare function local:XQ_HeaderReq_TO_HeaderRsp($imput as element() (:: schema-element(ns1:headerRequest) ::)) as element() (:: schema-element(ns2:headerResponse) ::) {
    <ns2:headerResponse>
        <ns2:datosCabecera>
            {
                if ($imput/ns1:datosCabecera/ns1:consumerId)
                then <ns2:consumerId>{fn:data($imput/ns1:datosCabecera/ns1:consumerId)}</ns2:consumerId>
                else ()
            }
            {
                if ($imput/ns1:datosCabecera/ns1:idTrx)
                then <ns2:idTrx>{fn:data($imput/ns1:datosCabecera/ns1:idTrx)}</ns2:idTrx>
                else ()
            }
            {
                if ($imput/ns1:datosCabecera/ns1:userId)
                then <ns2:userId>{fn:data($imput/ns1:datosCabecera/ns1:userId)}</ns2:userId>
                else ()
            }
            <ns2:fechaFin>{fn:current-dateTime()}</ns2:fechaFin>
            {
                if ($imput/ns1:datosCabecera/ns1:version)
                then <ns2:version>{fn:data($imput/ns1:datosCabecera/ns1:version)}</ns2:version>
                else ()
            }
        </ns2:datosCabecera>
        <ns2:datosSensibles>
            {
                if ($imput/ns1:datosSensibles/ns1:password)
                then <ns2:password>{fn:data($imput/ns1:datosSensibles/ns1:password)}</ns2:password>
                else ()
            }
        </ns2:datosSensibles>
    </ns2:headerResponse>
};

local:XQ_HeaderReq_TO_HeaderRsp($imput)