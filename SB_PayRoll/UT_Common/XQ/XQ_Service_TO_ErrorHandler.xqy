xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ctx = "http://www.bea.com/wli/sb/context";
declare namespace tp = "http://www.bea.com/wli/sb/transports";
declare namespace http = "http://www.bea.com/wli/sb/transports/http";

declare namespace ns1="http://cl.grupogtd.com/schemas/errorHandler";
(:: import schema at "../../UTL_ErrorHandler/XSD/errorHandlerReq.xsd" ::)

declare variable $soapbody as element(soap-env:Body) external;
declare variable $osbfault as element(ctx:fault) external;
declare variable $osbinbound as element(ctx:endpoint) external;
declare variable $osboutbound as element(ctx:endpoint) external;

declare function local:func($soapbody as element(soap-env:Body),
$osbfault as element(ctx:fault),
$osbinbound as element(ctx:endpoint),
$osboutbound as element(ctx:endpoint)) as element() (:: schema-element(ns1:procesarErrorReq) ::) {
    <ns1:procesarErrorReq>
        <ns1:servicio>string</ns1:servicio>
        <ns1:operación>string</ns1:operación>
        <ns1:codigoError>IN0001</ns1:codigoError>
        <ns1:ubicación>string</ns1:ubicación>
        <ns1:fecha>2008-09-28T21:49:45</ns1:fecha>
        <ns1:soapFault>{fn:data($osbfault)}</ns1:soapFault>
        <ns1:soapBody>{fn:data($soapbody)}</ns1:soapBody>
        <ns1:soapInbound>{fn:data($osbinbound)}</ns1:soapInbound>
        <ns1:soapOutbound>{fn:data($osboutbound)}</ns1:soapOutbound>
    </ns1:procesarErrorReq>
};

local:func($soapbody,
$osbfault,
$osbinbound,
$osboutbound)