package listener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Enumeration;

public class Main {
static String ip ="192.168.18.4";
static String ip2 = "10.0.2.15";
public static void iplocal() throws SocketException {
	Enumeration e = NetworkInterface.getNetworkInterfaces();
	while(e.hasMoreElements()){ NetworkInterface n = (NetworkInterface) e.nextElement();
	Enumeration ee = n.getInetAddresses();
	while (ee.hasMoreElements()) 
	{ InetAddress i = (InetAddress) ee.nextElement();
	if(i.getHostName().equals("enp0s3")) {
		ip2 = i.getHostAddress();
		break;
	}
	}
	}
	
}
	public static void main(String[] args) throws Exception {
		//iplocal();
		llenar();
		llenarListado();
		String tcpDumpCmd = "/usr/sbin/tcpdump -vvvnnls0 -A host 192.168.18.4"
				+ " | egrep --line-buffered \"(GET |PUT |DELETE |PATCH |COPY |OPTIONS |LINK| UNLINK|PURGE |LOCK |UNLOCK |PROPFIND |VIEW |REQUEST|RESPONSE|Request|Response|request|response|HTML|HTTP|html|DOC|POST |HEAD |{|})|^[A-Za-z0-9-]+: \"";
		;

		runTCPDUmp(tcpDumpCmd, true);

	}

	public static String querry(String re, String movimiento, String listado, String ip1, String ip2, String metodo) {
		String consulta = "insert into tablaregistro (movimiento, tipo, metodo, ";
		String values = " values ('" + movimiento + "', '" + re + "', '" + metodo + "', ";
		int pos;

		String texto = listado;
		if (texto.contains("HTTP/1.1 2")) {
			consulta += "HTTP/1.1, ";
			pos = texto.indexOf("HTTP/1.1 2");
			values += "'" + texto.substring(pos + 9, pos + 12) + "', ";
			texto.replaceAll("HTTP/1.1", "");
			System.out.println("200");
		} else if (texto.contains("HTTP/1.1 4")) {
			consulta += "HTTP/1.1, ";
			pos = texto.indexOf("HTTP/1.1 4");
			System.out.println("400");
			values += "'" + texto.substring(pos + 9, pos + 12) + "', ";
		} else if (texto.contains("HTTP/1.1 5")) {
			consulta += "HTTP/1.1, ";
			pos = texto.indexOf("HTTP/1.1 5");
			values += "'" + texto.substring(pos + 9, pos + 12) + "', ";
			System.out.println("500");
		}
		listado = texto;
		for (String tag : listadoTags) {
			if (listado.contains(tag)) {
				consulta += tag + ", ";
				if (tag.equals("Accept-Language:")) {

					values += "'" + buscarPosiciones(listado, tag).substring(0, 9) + "', ";
				} else {
					values += "'" + buscarPosiciones(listado, tag) + "', ";
				}
			}
		}
		consulta = consulta.replaceAll(":", "").replaceAll("/", "").replaceAll("-", "").replaceAll("HTTP1.1", "HTTP");
		if (listado.contains("{\"")) {
			consulta += "Body";
			values += "'" + listado.substring(listado.indexOf("{\""), listado.indexOf("\"}")) + "\"}'";

		} else if (listado.contains("<!DOC")) {
			consulta += "Body";
			values += "'" + listado.substring(listado.indexOf("<!DOC"), listado.indexOf("</html>")) + "</html>'";
		}
		if (consulta.contains("Body")) {
			consulta += ")" + values + ")";
		} else {
			consulta = consulta.substring(0, consulta.length() - 2) + ")" + values.substring(0, values.length() - 2)
					+ ")";
		}
		System.out.print(consulta);
		return consulta;
	}

	static ArrayList<String> listado = new ArrayList<>();

	public static void llenarListado() {
		listado.add("GET");
		listado.add("POST");
		listado.add("PUT");
		listado.add("PATCH");
		listado.add("DELETE");
		listado.add("COPY");
		listado.add("HEAD");
		listado.add("OPTIONS");
		listado.add("LINK");
		listado.add("UNLINK");
		listado.add("PURGE");
		listado.add("LOCK");
		listado.add("UNLOCK");
		listado.add("PROPFIND");
		listado.add("VIEW");
	}

	public static String metodo(String texto) {
		String metodoPaquete = "";
		for (String met : listado) {
			if (texto.contains(met)) {
				metodoPaquete = met;

				break;
			}
		}

		return metodoPaquete;
	}

	public static String ips(String l, String ip1, String ip2) {
		l.replaceAll(ip1, "\r\n" + ip1);
		l.replaceAll(ip2, "\r\n" + ip2);
		return l;
	}

	static ArrayList<String> listadoTags = new ArrayList<>();

	public static void llenar() {

		listadoTags.add("Host:");
		listadoTags.add("Connection:");
		listadoTags.add("Cache-Control:");
		listadoTags.add("Upgrade-Insecure-Requests:");
		listadoTags.add("User-Agent:");
		listadoTags.add("Accept:");
		listadoTags.add("Accept-Encoding:");
		listadoTags.add("Accept-Language:");
		listadoTags.add("Content-Type:");
		listadoTags.add("Content-Languaje:");
		listadoTags.add("Content-Length:");
		listadoTags.add("Date:");
		listadoTags.add("Server:");
		listadoTags.add("Transfer-Encoding:");
		listadoTags.add("Postman-Token:");

	}

	private static String formato(String texto) {

		for (String tipo : listadoTags) {
			texto = texto.replaceAll(tipo, "\r\n" + tipo);
		}
		ip=texto.substring(texto.indexOf("Host: "));
		ip= ip.substring(0, ip.indexOf("\r\n")).replace("Host: ", "");
		texto = texto.replaceAll("GMT", "GMT\r\n");
		texto = texto.replaceAll("0x", "\r\n0x");
		texto = texto.replaceAll("}", "}\r\n");
		ips(texto, "192.168.18.4", "10.0.2.15");
		return texto;

	}

	private static void bdc(String query) {

		Connection con = null;
		try {
			con = conectar();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			Statement st = con.createStatement();
			st.executeUpdate(query);
			con.close();
		} catch (SQLException sqle) {
			System.out.println("Error SQL....." + sqle);
		}
	}

	public static String buscarPosiciones(String buscando, String buscado) {
		String resultado = "-";
		String[] partido = null;
		if (buscando.indexOf(buscado) != -1) {
			resultado = buscando.substring(buscando.indexOf(buscado));
			resultado = resultado.replace(buscado, "");
			if (resultado.indexOf("\r\n") != -1) {
				resultado = resultado.substring(0, resultado.indexOf("\r\n"));
				resultado = resultado.trim();
				if (resultado.contains("\t")) {
					partido = resultado.split("\t");
					resultado = partido[0];
				}

			}
		}

		return resultado;
	}

	// arreglar
	public static String inout(String buscado, String ip) {
		String salida = "";
		if (buscado.indexOf("> " + ip) < buscado.indexOf(ip + " >")) {
			salida = "in";
		} else {
			salida = "out";
		}

		return salida;
	}

	public static Connection conectar() throws Exception {
		Connection con = null;
		String user = "root";
		String pass = "Vasquez1234.";
		String url = "jdbc:mysql://192.168.18.4:3306/sat";
		con = DriverManager.getConnection(url, user, pass);

		return con;
	}

	public static void runTCPDUmp(String cmd, boolean wait) {

		ProcessBuilder processBuilder = null;
		String operatingSystem = System.getProperty("os.name");
		if (operatingSystem.toLowerCase().contains("window")) {
			processBuilder = new ProcessBuilder("cmd", "/c", cmd);
		} else {
			processBuilder = new ProcessBuilder("/bin/bash", "-c", cmd);
		}

		processBuilder.redirectErrorStream(true);

		try {
			Process process = processBuilder.start();
			if (wait) {
				InputStream input = process.getInputStream();
				getStringFromStream(input);
				input.close();
			}

		} catch (Exception e) {
			System.out.println("Error Executing tcpdump command" + e);
		}

	}

	private static void getStringFromStream(InputStream linea) throws IOException {

		if (linea != null) {

			InputStreamReader isReader = new InputStreamReader(linea);
			BufferedReader reader = new BufferedReader(isReader);
			StringBuffer sb = new StringBuffer();
			String str;
			String DBs = "";
			String movimiento = "";
			String metodopaquete = "";
			String[] partes = null;
			
			String guardar;
			int codigopost = 0;
			String acumTxt = "";
			String aux = "";
			String body = "";

			while ((str = reader.readLine()) != null) {

				sb.append(str);

				if (str.contains("\"}") || str.contains("</html>")) {
					DBs = sb.toString();
					if (!aux.equals("")) {
						metodopaquete = aux;
					} else {
						metodopaquete = metodo(DBs);
					}

					DBs = formato(DBs);
					switch (metodopaquete) {
					case "GET":
						
						listadoTags = new ArrayList<>();
						listado = new ArrayList<>();
						llenar();
						llenarListado();
						partes = DBs.split(metodopaquete);
						int conteo = partes.length;
						movimiento = inout(DBs, ip);
						guardar = querry("request", movimiento, partes[1], ip, ip2, metodopaquete);
						if (guardar.contains("Host")) {
							bdc(guardar);
							guardar = "";
						}

						if (movimiento.equals("in")) {
							movimiento = "out";
						} else {
							movimiento = "in";
						}
						if (conteo >= 3) {
							guardar = querry("response", movimiento, partes[2], ip, ip2, metodopaquete);
							if (guardar.contains("Host")) {
								bdc(guardar);
								guardar = "";
							}
						}
						sb = new StringBuffer();
						DBs = "";
						partes = null;

						break;
					case "POST":
						System.out.println("POST");

						switch (codigopost) {

						case 0:
							listadoTags = new ArrayList<>();
							listado = new ArrayList<>();
							llenar();
							llenarListado();
							acumTxt += DBs + " \r\n";
							acumTxt= formato(acumTxt);
							movimiento = inout(acumTxt, ip);
							guardar = querry("request", movimiento, acumTxt, ip, ip2, metodopaquete);
							if (guardar.contains("Host")) {

								bdc(guardar);
								aux = metodopaquete;
								sb = new StringBuffer();
								DBs = "";
								partes = null;

								if (acumTxt.contains("{\"")) {

									body = acumTxt.substring(acumTxt.indexOf("{\""), acumTxt.indexOf("\"}")) + "\"}";

								} else if (listado.contains("<!DOC")) {

									body = acumTxt.substring(acumTxt.indexOf("<!DOC"), acumTxt.indexOf("</html>"))
											+ "</html>";
								}
							}
							acumTxt = "";
							codigopost++;

							break;
						case 1:
							codigopost++;
							break;
						case 2:
							acumTxt = "";
							listadoTags = new ArrayList<>();
							listado = new ArrayList<>();
							llenar();
							llenarListado();
							movimiento = inout(DBs, ip);
							while (DBs.contains(body)) {
								DBs = DBs.replace(body, "");
							}
							DBs = formato(DBs);
							guardar = querry("response", movimiento, DBs, ip, ip2, metodopaquete);

							if (guardar.contains("Host")) {
								bdc(guardar);
								body = "";
								aux = "";
								sb = new StringBuffer();
								DBs = "";
								partes = null;
							}
							codigopost = 0;
							break;

						}
						break;
					case "PUT":
						listadoTags = new ArrayList<>();
						listado = new ArrayList<>();
						llenar();
						llenarListado();
						switch (codigopost) {

						case 0:
							listadoTags = new ArrayList<>();
							listado = new ArrayList<>();
							llenar();
							llenarListado();
							acumTxt += DBs + " \r\n";
							acumTxt= formato(acumTxt);
							movimiento = inout(acumTxt, ip);
							guardar = querry("request", movimiento, formato(acumTxt), ip, ip2, metodopaquete);
							if (guardar.contains("Host")) {

								bdc(guardar);
								aux = metodopaquete;
								sb = new StringBuffer();
								DBs = "";
								partes = null;

								if (acumTxt.contains("{\"")) {

									body = acumTxt.substring(acumTxt.indexOf("{\""), acumTxt.indexOf("\"}")) + "\"}";

								} else if (listado.contains("<!DOC")) {

									body = acumTxt.substring(acumTxt.indexOf("<!DOC"), acumTxt.indexOf("</html>"))
											+ "</html>";
								}
							}
							acumTxt="";
							codigopost++;

							break;
						case 1:
							codigopost++;
							break;
						case 2:
							acumTxt = "";
							listadoTags = new ArrayList<>();
							listado = new ArrayList<>();
							llenar();
							llenarListado();
							movimiento = inout(DBs, ip);
							while (DBs.contains(body)) {
								DBs = DBs.replace(body, "");
							}
							DBs = formato(DBs);
							guardar = querry("response", movimiento, DBs, ip, ip2, metodopaquete);

							if (guardar.contains("Host")) {
								bdc(guardar);
								body = "";
								aux = "";
								sb = new StringBuffer();
								DBs = "";
								partes = null;
							}
							codigopost = 0;
							acumTxt = "";
							break;

						}
						break;
						
					case "DELETE":
						listadoTags = new ArrayList<>();
						listado = new ArrayList<>();
						llenar();
						llenarListado();
						switch (codigopost) {

						case 0:
							listadoTags = new ArrayList<>();
							listado = new ArrayList<>();
							llenar();
							llenarListado();
							
							acumTxt += DBs + " \r\n";
							acumTxt= formato(acumTxt);
							movimiento = inout(acumTxt, ip);
							guardar = querry("request", movimiento, formato(acumTxt), ip, ip2, metodopaquete);
							if (guardar.contains("Host")) {

								bdc(guardar);
								aux = metodopaquete;
								sb = new StringBuffer();
								DBs = "";
								partes = null;

								if (acumTxt.contains("{\"")) {

									body = acumTxt.substring(acumTxt.indexOf("{\""), acumTxt.indexOf("\"}")) + "\"}";

								} else if (listado.contains("<!DOC")) {

									body = acumTxt.substring(acumTxt.indexOf("<!DOC"), acumTxt.indexOf("</html>"))
											+ "</html>";
								}
							}
							codigopost++;

							break;
						case 1:
							codigopost++;
							break;
						case 2:
							acumTxt = "";
							listadoTags = new ArrayList<>();
							listado = new ArrayList<>();
							llenar();
							llenarListado();
							movimiento = inout(DBs, ip);
							while (DBs.contains(body)) {
								DBs = DBs.replace(body, "");
							}
							DBs = formato(DBs);
							guardar = querry("response", movimiento, DBs, ip, ip2, metodopaquete);

							if (guardar.contains("Host")) {
								bdc(guardar);
								body = "";
								aux = "";
								sb = new StringBuffer();
								DBs = "";
								partes = null;
							}
							codigopost = 0;
							break;

						}
						break;
					default:
						break;

					}
				}

			}
		}

	}
}
