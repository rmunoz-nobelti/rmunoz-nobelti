package com.service.readoutbound.endpoint;

import com.service.readoutbound.model.ToaOutbound;
import com.service.readoutbound.repository.ToaOutboundDAO;
import io.spring.guides.gs_producing_web_service.MessageResponseT;
import io.spring.guides.gs_producing_web_service.MessageT;
import io.spring.guides.gs_producing_web_service.SendMessageRequest;
import io.spring.guides.gs_producing_web_service.SendMessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * Created by Manuel Martorell Gonzalez
 * on 2/12/2018 3:44 PM
 */
@Endpoint
public class MiddlewareAdvancedEndpoint {
    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

    private ToaOutboundDAO toaOutboundDAO;

    @Autowired
    public MiddlewareAdvancedEndpoint(ToaOutboundDAO messajeRepository) {
        this.toaOutboundDAO = messajeRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "send_message_request")
    @ResponsePayload
    public SendMessageResponse sendMessage(@RequestPayload SendMessageRequest request) {
        SendMessageResponse response = new SendMessageResponse();
        for (MessageT messageT : request.getMessages().getMessage()) {
            ToaOutbound toaOutbound = new ToaOutbound();
            MessageResponseT messageResponseT = new MessageResponseT();
            try {
                toaOutbound.setAddress(messageT.getAddress());
                toaOutbound.setAppHost(messageT.getAppHost());
                toaOutbound.setAppPort(messageT.getAppPort());
                toaOutbound.setAppUrl(messageT.getAppUrl());
                toaOutbound.setBody(messageT.getBody());
                toaOutbound.setCompanyId(messageT.getCompanyId());
                toaOutbound.setMessageId(messageT.getMessageId());
                toaOutbound.setSendTo(messageT.getSendTo());
                toaOutbound.setSubject(messageT.getSubject());
                toaOutboundDAO.save(toaOutbound);

                messageResponseT.setMessageId(messageT.getMessageId());
                messageResponseT.setStatus("0");
                messageResponseT.setDescription("successful");

            } catch (Exception error) {
                System.out.print(error.getMessage());
                messageResponseT.setMessageId(messageT.getMessageId());
                messageResponseT.setStatus("1");
                messageResponseT.setDescription("internal error");
            }
            response.getMessageResponse().add(messageResponseT);
        }
        return response;
    }
}
