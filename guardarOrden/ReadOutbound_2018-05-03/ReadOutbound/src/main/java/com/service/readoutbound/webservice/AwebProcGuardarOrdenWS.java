package com.service.readoutbound.webservice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

/**
 * Created by Manuel Martorell Gonzalez
 * on 2/12/2018 5:19 PM
 */
@Configuration
public class AwebProcGuardarOrdenWS {

    @Bean(name = "AwebProcGuardarOrden")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema awebProcGuardarOrdendSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setServiceName("agent_service");
        wsdl11Definition.setRequestSuffix("_request");
        wsdl11Definition.setResponseSuffix("_response");
        wsdl11Definition.setPortTypeName("agent_port_type");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace("urn:toatech:agent");
        wsdl11Definition.setSchema(awebProcGuardarOrdendSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema awebProcGuardarOrdendSchema() {
        return new SimpleXsdSchema(new ClassPathResource("AwebProcGuardarOrden.xsd"));
    }
}
