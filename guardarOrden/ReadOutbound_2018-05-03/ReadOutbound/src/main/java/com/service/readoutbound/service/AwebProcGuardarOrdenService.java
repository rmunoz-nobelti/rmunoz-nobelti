package com.service.readoutbound.service;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

/**
 * Created by Manuel Martorell Gonzalez
 * on 3/1/2018 11:18 AM
 */
@Service
public class AwebProcGuardarOrdenService {

    @PersistenceContext
    private EntityManager entityManager;

    public String AwebProcGuardarOrden(String CRM, String APPNUMBER, String ACTIVITYTYPE, String XA_TECHNOLOGY,
                                Integer XA_MOVE, Integer XA_URGENT, Integer XA_COMPLAIN_TYPE, Integer SCHEDULEWEB,
                                String COUNTRY_CODETUS, String XA_REGION, String XA_PROVINCE, String STATEPROVINCE,
                                String XA_COMUNA, String CITY, String XA_NODE, String STATUS, String XA_INSTALL_DETAILS,
                                Integer XA_NR_POINTS_TV, Integer XA_NR_POINT_INTERNET, Integer XA_NR_POINT_PHONE,
                                String XA_STREET_TYPE, String XA_STREET_NR, String STREETADDRESS,
                                String XA_ADDRESS_COMPLEMENT, String XA_ADDRESS_COMPLEMENT_2, String XA_REF_SEC,
                                String POSTALCODE, String COORDSTATUS, String LONGITUDE, String LATITUDE,
                                String SETPOSITIONINROUTE, Integer ACTIVITYID, String POSITION, String RECORDTYPE,
                                String CUSTOMERNUMBER, String XA_RUT, String CUSTOMERNAME, Integer XA_CUSTOMER_TYPE,
                                Integer XA_CUSTORMER_CATEGORY, String CUSTOMERCELL, String CUSTOMERPHONE,
                                String XA_CONTACT_PHONE_2, String CUSTOMEREMAIL, Integer XA_SELL_TYPE, String XA_SELLER,
                                String TIMEZONE, String LANGUAJE, String ADITTIONALDATA, String ADITTIONALSUBDATA
    ) {
        StoredProcedureQuery query = entityManager.createStoredProcedureQuery("AWEB_PROC_GUARDAR_ORDEN");
        query.registerStoredProcedureParameter("CRM", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("APPNUMBER", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("ACTIVITYTYPE", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_TECHNOLOGY", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_MOVE", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_URGENT", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_COMPLAIN_TYPE", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("SCHEDULEWEB", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("COUNTRY_CODETUS", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_REGION", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_PROVINCE", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("STATEPROVINCE", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_COMUNA", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("CITY", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_NODE", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("STATUS", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_INSTALL_DETAILS", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_NR_POINTS_TV", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_NR_POINT_INTERNET", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_NR_POINT_PHONE", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_STREET_TYPE", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_STREET_NR", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("STREETADDRESS", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_ADDRESS_COMPLEMENT", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_ADDRESS_COMPLEMENT_2", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_REF_SEC", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("POSTALCODE", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("COORDSTATUS", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("LONGITUDE", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("LATITUDE", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("SETPOSITIONINROUTE", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("ACTIVITYID", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("POSITION", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("RECORDTYPE", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("CUSTOMERNUMBER", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_RUT", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("CUSTOMERNAME", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_CUSTOMER_TYPE", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_CUSTORMER_CATEGORY", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("CUSTOMERCELL", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("CUSTOMERPHONE", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_CONTACT_PHONE_2", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("CUSTOMEREMAIL", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_SELL_TYPE", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("XA_SELLER", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("TIMEZONE", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("LANGUAJE", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("ADITTIONALDATA", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("ADITTIONALSUBDATA", String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("CRM_APPNUMBER", String.class, ParameterMode.OUT);
        query.registerStoredProcedureParameter("estado", Integer.class, ParameterMode.OUT);
        query.registerStoredProcedureParameter("descripcion", String.class, ParameterMode.OUT);

        query.setParameter("CRM", CRM);
        query.setParameter("APPNUMBER", APPNUMBER);
        query.setParameter("ACTIVITYTYPE", ACTIVITYTYPE);
        query.setParameter("XA_TECHNOLOGY", XA_TECHNOLOGY);
        query.setParameter("XA_MOVE", XA_MOVE);
        query.setParameter("XA_URGENT", XA_URGENT);
        query.setParameter("XA_COMPLAIN_TYPE", XA_COMPLAIN_TYPE);
        query.setParameter("SCHEDULEWEB", SCHEDULEWEB);
        query.setParameter("COUNTRY_CODETUS", COUNTRY_CODETUS);
        query.setParameter("XA_REGION", XA_REGION);
        query.setParameter("XA_PROVINCE", XA_PROVINCE);
        query.setParameter("STATEPROVINCE", STATEPROVINCE);
        query.setParameter("XA_COMUNA", XA_COMUNA);
        query.setParameter("CITY", CITY);
        query.setParameter("XA_NODE", XA_NODE);
        query.setParameter("STATUS", STATUS);
        query.setParameter("XA_INSTALL_DETAILS", XA_INSTALL_DETAILS);
        query.setParameter("XA_NR_POINTS_TV", XA_NR_POINTS_TV);
        query.setParameter("XA_NR_POINT_INTERNET", XA_NR_POINT_INTERNET);
        query.setParameter("XA_NR_POINT_PHONE", XA_NR_POINT_PHONE);
        query.setParameter("XA_STREET_TYPE", XA_STREET_TYPE);
        query.setParameter("XA_STREET_NR", XA_STREET_NR);
        query.setParameter("STREETADDRESS", STREETADDRESS);
        query.setParameter("XA_ADDRESS_COMPLEMENT", XA_ADDRESS_COMPLEMENT);
        query.setParameter("XA_ADDRESS_COMPLEMENT_2", XA_ADDRESS_COMPLEMENT_2);
        query.setParameter("XA_REF_SEC", XA_REF_SEC);
        query.setParameter("POSTALCODE", POSTALCODE);
        query.setParameter("COORDSTATUS", COORDSTATUS);
        query.setParameter("LONGITUDE", LONGITUDE);
        query.setParameter("LATITUDE", LATITUDE);
        query.setParameter("SETPOSITIONINROUTE", SETPOSITIONINROUTE);
        query.setParameter("ACTIVITYID", ACTIVITYID);
        query.setParameter("POSITION", POSITION);
        query.setParameter("RECORDTYPE", RECORDTYPE);
        query.setParameter("CUSTOMERNUMBER", CUSTOMERNUMBER);
        query.setParameter("XA_RUT", XA_RUT);
        query.setParameter("CUSTOMERNAME", CUSTOMERNAME);
        query.setParameter("XA_CUSTOMER_TYPE", XA_CUSTOMER_TYPE);
        query.setParameter("XA_CUSTORMER_CATEGORY", XA_CUSTORMER_CATEGORY);
        query.setParameter("CUSTOMERCELL", CUSTOMERCELL);
        query.setParameter("CUSTOMERPHONE", CUSTOMERPHONE);
        query.setParameter("XA_CONTACT_PHONE_2", XA_CONTACT_PHONE_2);
        query.setParameter("CUSTOMEREMAIL", CUSTOMEREMAIL);
        query.setParameter("XA_SELL_TYPE", XA_SELL_TYPE);
        query.setParameter("XA_SELLER", XA_SELLER);
        query.setParameter("TIMEZONE", TIMEZONE);
        query.setParameter("LANGUAJE", LANGUAJE);
        query.setParameter("ADITTIONALDATA", ADITTIONALDATA);
        query.setParameter("ADITTIONALSUBDATA", ADITTIONALSUBDATA);
        query.execute();
        String CRMAPPNUMBER = (String) query.getOutputParameterValue("CRM_APPNUMBER");
        return CRMAPPNUMBER;
    }
}
