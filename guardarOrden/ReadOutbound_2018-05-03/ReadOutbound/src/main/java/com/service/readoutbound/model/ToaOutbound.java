package com.service.readoutbound.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by Manuel Martorell Gonzalez
 * on 2/13/2018 11:20 AM
 */
@Entity
@Table(name = "TBL_TOA_OUTBOUND")
public class ToaOutbound {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "ID", columnDefinition = "varchar2(36)")
    private String id;

    @Column(name = "APP_HOST", columnDefinition = "varchar2(250)")
    private String appHost;

    @Column(name = "APP_PORT", columnDefinition = "number")
    private Long appPort;

    @Column(name = "APP_URL", columnDefinition = "varchar2(250)")
    private String appUrl;

    @Column(name = "MESSAGE_ID", columnDefinition = "number")
    private Long messageId;

    @Column(name = "COMPANY_ID", columnDefinition = "number")
    private Long companyId;

    @Column(name = "ADDRESS", columnDefinition = "varchar2(250)")
    private String address;

    @Column(name = "SEND_TO", columnDefinition = "varchar2(250)")
    private String sendTo;

    @Column(name = "SUBJECT", columnDefinition = "varchar2(250)")
    private String subject;

    @Column(name = "BODY", columnDefinition = "varchar2(1500)")
    private String body;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAppHost() {
        return appHost;
    }

    public void setAppHost(String appHost) {
        this.appHost = appHost;
    }

    public Long getAppPort() {
        return appPort;
    }

    public void setAppPort(Long appPort) {
        this.appPort = appPort;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSendTo() {
        return sendTo;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
