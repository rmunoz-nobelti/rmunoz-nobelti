package com.service.readoutbound.repository;

import com.service.readoutbound.model.ToaOutbound;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Manuel Martorell Gonzalez
 * on 2/12/2018 3:39 PM
 */
@Repository
@Transactional
public interface ToaOutboundDAO extends JpaRepository<ToaOutbound, String> {
}
