package com.service.readoutbound.endpoint;

import com.service.readoutbound.service.AwebProcGuardarOrdenService;
import io.spring.guides.gs_producing_web_service.GuardarOrdenRequest;
import io.spring.guides.gs_producing_web_service.GuardarOrdenResponse;
import io.spring.guides.gs_producing_web_service.InputT;
import io.spring.guides.gs_producing_web_service.OutputT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * Created by Manuel Martorell Gonzalez
 * on 2/12/2018 3:44 PM
 */
@Endpoint
public class AwebProcGuardarOrdenEndpoint {
    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

    @Autowired
    private AwebProcGuardarOrdenService guardarOrdenService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "guardar_orden_request")
    @ResponsePayload
    public GuardarOrdenResponse guardarOrden(@RequestPayload GuardarOrdenRequest request) {
        InputT input = request.getInput();
        String CRM_APPNUMBER = guardarOrdenService.AwebProcGuardarOrden(input.getCRM(), input.getAPPNUMBER(),
                input.getACTIVITYTYPE(), input.getXATECHNOLOGY(), input.getXAMOVE().intValue(),
                input.getXAURGENT().intValue(), input.getXACOMPLAINTYPE().intValue(), input.getSCHEDULEWEB().intValue(),
                input.getCOUNTRYCODETUS(), input.getXAREGION(), input.getXAPROVINCE(), input.getSTATEPROVINCE(),
                input.getXACOMUNA(), input.getCITY(), input.getXANODE(), input.getSTATUS(), input.getXAINSTALLDETAILS(),
                input.getXANRPOINTSTV().intValue(), input.getXANRPOINTINTERNET().intValue(), input.getXANRPOINTPHONE().intValue(),
                input.getXASTREETTYPE(), input.getXASTREETNR(), input.getSTREETADDRESS(),
                input.getXAADDRESSCOMPLEMENT(), input.getXAADDRESSCOMPLEMENT2(), input.getXAREFSEC(),
                input.getPOSTALCODE(), input.getCOORDSTATUS(), input.getLONGITUDE(), input.getLATITUDE(),
                input.getSETPOSITIONINROUTE(), input.getACTIVITYID().intValue(), input.getPOSITION(), input.getRECORDTYPE(),
                input.getCUSTOMERNUMBER(), input.getXARUT(), input.getCUSTOMERNAME(), input.getXACUSTOMERTYPE().intValue(),
                input.getXACUSTORMERCATEGORY().intValue(), input.getCUSTOMERCELL(), input.getCUSTOMERPHONE(),
                input.getXACONTACTPHONE2(), input.getCUSTOMEREMAIL(), input.getXASELLTYPE().intValue(), input.getXASELLER(),
                input.getTIMEZONE(), input.getLANGUAJE(), input.getADITTIONALDATA(), input.getADITTIONALSUBDATA());
        GuardarOrdenResponse response = new GuardarOrdenResponse();
        OutputT outputT = new OutputT();
        outputT.setCRMAPPNUMBER(CRM_APPNUMBER);
        response.getMessageResponse().add(outputT);
        return response;
    }
}
