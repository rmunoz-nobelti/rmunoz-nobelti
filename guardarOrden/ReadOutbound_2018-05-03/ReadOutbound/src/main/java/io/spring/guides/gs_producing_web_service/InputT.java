//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.03.05 at 05:38:20 PM EST 
//


package io.spring.guides.gs_producing_web_service;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for input_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="input_t">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CRM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="APPNUMBER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ACTIVITYTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XA_TECHNOLOGY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XA_MOVE" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="XA_URGENT" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="XA_COMPLAIN_TYPE" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="SCHEDULEWEB" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="COUNTRY_CODETUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XA_REGION" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XA_PROVINCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="STATEPROVINCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XA_COMUNA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XA_NODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="STATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XA_INSTALL_DETAILS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XA_NR_POINTS_TV" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="XA_NR_POINT_INTERNET" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="XA_NR_POINT_PHONE" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="XA_STREET_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XA_STREET_NR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="STREETADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XA_ADDRESS_COMPLEMENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XA_ADDRESS_COMPLEMENT_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XA_REF_SEC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="POSTALCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="COORDSTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LONGITUDE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LATITUDE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SETPOSITIONINROUTE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ACTIVITYID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="POSITION" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RECORDTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CUSTOMERNUMBER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XA_RUT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CUSTOMERNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XA_CUSTOMER_TYPE" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="XA_CUSTORMER_CATEGORY" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="CUSTOMERCELL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CUSTOMERPHONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XA_CONTACT_PHONE_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CUSTOMEREMAIL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XA_SELL_TYPE" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="XA_SELLER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TIMEZONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LANGUAJE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ADITTIONALDATA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ADITTIONALSUBDATA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "input_t", propOrder = {
    "crm",
    "appnumber",
    "activitytype",
    "xatechnology",
    "xamove",
    "xaurgent",
    "xacomplaintype",
    "scheduleweb",
    "countrycodetus",
    "xaregion",
    "xaprovince",
    "stateprovince",
    "xacomuna",
    "city",
    "xanode",
    "status",
    "xainstalldetails",
    "xanrpointstv",
    "xanrpointinternet",
    "xanrpointphone",
    "xastreettype",
    "xastreetnr",
    "streetaddress",
    "xaaddresscomplement",
    "xaaddresscomplement2",
    "xarefsec",
    "postalcode",
    "coordstatus",
    "longitude",
    "latitude",
    "setpositioninroute",
    "activityid",
    "position",
    "recordtype",
    "customernumber",
    "xarut",
    "customername",
    "xacustomertype",
    "xacustormercategory",
    "customercell",
    "customerphone",
    "xacontactphone2",
    "customeremail",
    "xaselltype",
    "xaseller",
    "timezone",
    "languaje",
    "adittionaldata",
    "adittionalsubdata"
})
public class InputT {

    @XmlElement(name = "CRM", required = true)
    protected String crm;
    @XmlElement(name = "APPNUMBER", required = true)
    protected String appnumber;
    @XmlElement(name = "ACTIVITYTYPE", required = true)
    protected String activitytype;
    @XmlElement(name = "XA_TECHNOLOGY", required = true)
    protected String xatechnology;
    @XmlElement(name = "XA_MOVE", required = true)
    protected BigInteger xamove;
    @XmlElement(name = "XA_URGENT", required = true)
    protected BigInteger xaurgent;
    @XmlElement(name = "XA_COMPLAIN_TYPE", required = true)
    protected BigInteger xacomplaintype;
    @XmlElement(name = "SCHEDULEWEB", required = true, defaultValue = "1")
    protected BigInteger scheduleweb;
    @XmlElement(name = "COUNTRY_CODETUS", required = true)
    protected String countrycodetus;
    @XmlElement(name = "XA_REGION", required = true)
    protected String xaregion;
    @XmlElement(name = "XA_PROVINCE", required = true)
    protected String xaprovince;
    @XmlElement(name = "STATEPROVINCE", required = true)
    protected String stateprovince;
    @XmlElement(name = "XA_COMUNA", required = true)
    protected String xacomuna;
    @XmlElement(name = "CITY", required = true)
    protected String city;
    @XmlElement(name = "XA_NODE", required = true)
    protected String xanode;
    @XmlElement(name = "STATUS", required = true)
    protected String status;
    @XmlElement(name = "XA_INSTALL_DETAILS", required = true)
    protected String xainstalldetails;
    @XmlElement(name = "XA_NR_POINTS_TV", required = true)
    protected BigInteger xanrpointstv;
    @XmlElement(name = "XA_NR_POINT_INTERNET", required = true)
    protected BigInteger xanrpointinternet;
    @XmlElement(name = "XA_NR_POINT_PHONE", required = true)
    protected BigInteger xanrpointphone;
    @XmlElement(name = "XA_STREET_TYPE", required = true)
    protected String xastreettype;
    @XmlElement(name = "XA_STREET_NR", required = true)
    protected String xastreetnr;
    @XmlElement(name = "STREETADDRESS", required = true)
    protected String streetaddress;
    @XmlElement(name = "XA_ADDRESS_COMPLEMENT", required = true)
    protected String xaaddresscomplement;
    @XmlElement(name = "XA_ADDRESS_COMPLEMENT_2", required = true)
    protected String xaaddresscomplement2;
    @XmlElement(name = "XA_REF_SEC", required = true)
    protected String xarefsec;
    @XmlElement(name = "POSTALCODE", required = true)
    protected String postalcode;
    @XmlElement(name = "COORDSTATUS", required = true)
    protected String coordstatus;
    @XmlElement(name = "LONGITUDE", required = true)
    protected String longitude;
    @XmlElement(name = "LATITUDE", required = true)
    protected String latitude;
    @XmlElement(name = "SETPOSITIONINROUTE", required = true)
    protected String setpositioninroute;
    @XmlElement(name = "ACTIVITYID", required = true)
    protected BigInteger activityid;
    @XmlElement(name = "POSITION", required = true)
    protected String position;
    @XmlElement(name = "RECORDTYPE", required = true)
    protected String recordtype;
    @XmlElement(name = "CUSTOMERNUMBER", required = true)
    protected String customernumber;
    @XmlElement(name = "XA_RUT", required = true)
    protected String xarut;
    @XmlElement(name = "CUSTOMERNAME", required = true)
    protected String customername;
    @XmlElement(name = "XA_CUSTOMER_TYPE", required = true)
    protected BigInteger xacustomertype;
    @XmlElement(name = "XA_CUSTORMER_CATEGORY", required = true)
    protected BigInteger xacustormercategory;
    @XmlElement(name = "CUSTOMERCELL", required = true)
    protected String customercell;
    @XmlElement(name = "CUSTOMERPHONE", required = true)
    protected String customerphone;
    @XmlElement(name = "XA_CONTACT_PHONE_2", required = true)
    protected String xacontactphone2;
    @XmlElement(name = "CUSTOMEREMAIL", required = true)
    protected String customeremail;
    @XmlElement(name = "XA_SELL_TYPE", required = true)
    protected BigInteger xaselltype;
    @XmlElement(name = "XA_SELLER", required = true)
    protected String xaseller;
    @XmlElement(name = "TIMEZONE", required = true)
    protected String timezone;
    @XmlElement(name = "LANGUAJE", required = true)
    protected String languaje;
    @XmlElement(name = "ADITTIONALDATA", required = true)
    protected String adittionaldata;
    @XmlElement(name = "ADITTIONALSUBDATA", required = true)
    protected String adittionalsubdata;

    /**
     * Gets the value of the crm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRM() {
        return crm;
    }

    /**
     * Sets the value of the crm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRM(String value) {
        this.crm = value;
    }

    /**
     * Gets the value of the appnumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPPNUMBER() {
        return appnumber;
    }

    /**
     * Sets the value of the appnumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPPNUMBER(String value) {
        this.appnumber = value;
    }

    /**
     * Gets the value of the activitytype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACTIVITYTYPE() {
        return activitytype;
    }

    /**
     * Sets the value of the activitytype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACTIVITYTYPE(String value) {
        this.activitytype = value;
    }

    /**
     * Gets the value of the xatechnology property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXATECHNOLOGY() {
        return xatechnology;
    }

    /**
     * Sets the value of the xatechnology property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXATECHNOLOGY(String value) {
        this.xatechnology = value;
    }

    /**
     * Gets the value of the xamove property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getXAMOVE() {
        return xamove;
    }

    /**
     * Sets the value of the xamove property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setXAMOVE(BigInteger value) {
        this.xamove = value;
    }

    /**
     * Gets the value of the xaurgent property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getXAURGENT() {
        return xaurgent;
    }

    /**
     * Sets the value of the xaurgent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setXAURGENT(BigInteger value) {
        this.xaurgent = value;
    }

    /**
     * Gets the value of the xacomplaintype property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getXACOMPLAINTYPE() {
        return xacomplaintype;
    }

    /**
     * Sets the value of the xacomplaintype property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setXACOMPLAINTYPE(BigInteger value) {
        this.xacomplaintype = value;
    }

    /**
     * Gets the value of the scheduleweb property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSCHEDULEWEB() {
        return scheduleweb;
    }

    /**
     * Sets the value of the scheduleweb property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSCHEDULEWEB(BigInteger value) {
        this.scheduleweb = value;
    }

    /**
     * Gets the value of the countrycodetus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOUNTRYCODETUS() {
        return countrycodetus;
    }

    /**
     * Sets the value of the countrycodetus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOUNTRYCODETUS(String value) {
        this.countrycodetus = value;
    }

    /**
     * Gets the value of the xaregion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXAREGION() {
        return xaregion;
    }

    /**
     * Sets the value of the xaregion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXAREGION(String value) {
        this.xaregion = value;
    }

    /**
     * Gets the value of the xaprovince property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXAPROVINCE() {
        return xaprovince;
    }

    /**
     * Sets the value of the xaprovince property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXAPROVINCE(String value) {
        this.xaprovince = value;
    }

    /**
     * Gets the value of the stateprovince property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTATEPROVINCE() {
        return stateprovince;
    }

    /**
     * Sets the value of the stateprovince property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTATEPROVINCE(String value) {
        this.stateprovince = value;
    }

    /**
     * Gets the value of the xacomuna property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXACOMUNA() {
        return xacomuna;
    }

    /**
     * Sets the value of the xacomuna property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXACOMUNA(String value) {
        this.xacomuna = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCITY() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCITY(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the xanode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXANODE() {
        return xanode;
    }

    /**
     * Sets the value of the xanode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXANODE(String value) {
        this.xanode = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTATUS() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTATUS(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the xainstalldetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXAINSTALLDETAILS() {
        return xainstalldetails;
    }

    /**
     * Sets the value of the xainstalldetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXAINSTALLDETAILS(String value) {
        this.xainstalldetails = value;
    }

    /**
     * Gets the value of the xanrpointstv property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getXANRPOINTSTV() {
        return xanrpointstv;
    }

    /**
     * Sets the value of the xanrpointstv property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setXANRPOINTSTV(BigInteger value) {
        this.xanrpointstv = value;
    }

    /**
     * Gets the value of the xanrpointinternet property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getXANRPOINTINTERNET() {
        return xanrpointinternet;
    }

    /**
     * Sets the value of the xanrpointinternet property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setXANRPOINTINTERNET(BigInteger value) {
        this.xanrpointinternet = value;
    }

    /**
     * Gets the value of the xanrpointphone property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getXANRPOINTPHONE() {
        return xanrpointphone;
    }

    /**
     * Sets the value of the xanrpointphone property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setXANRPOINTPHONE(BigInteger value) {
        this.xanrpointphone = value;
    }

    /**
     * Gets the value of the xastreettype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXASTREETTYPE() {
        return xastreettype;
    }

    /**
     * Sets the value of the xastreettype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXASTREETTYPE(String value) {
        this.xastreettype = value;
    }

    /**
     * Gets the value of the xastreetnr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXASTREETNR() {
        return xastreetnr;
    }

    /**
     * Sets the value of the xastreetnr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXASTREETNR(String value) {
        this.xastreetnr = value;
    }

    /**
     * Gets the value of the streetaddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTREETADDRESS() {
        return streetaddress;
    }

    /**
     * Sets the value of the streetaddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTREETADDRESS(String value) {
        this.streetaddress = value;
    }

    /**
     * Gets the value of the xaaddresscomplement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXAADDRESSCOMPLEMENT() {
        return xaaddresscomplement;
    }

    /**
     * Sets the value of the xaaddresscomplement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXAADDRESSCOMPLEMENT(String value) {
        this.xaaddresscomplement = value;
    }

    /**
     * Gets the value of the xaaddresscomplement2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXAADDRESSCOMPLEMENT2() {
        return xaaddresscomplement2;
    }

    /**
     * Sets the value of the xaaddresscomplement2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXAADDRESSCOMPLEMENT2(String value) {
        this.xaaddresscomplement2 = value;
    }

    /**
     * Gets the value of the xarefsec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXAREFSEC() {
        return xarefsec;
    }

    /**
     * Sets the value of the xarefsec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXAREFSEC(String value) {
        this.xarefsec = value;
    }

    /**
     * Gets the value of the postalcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOSTALCODE() {
        return postalcode;
    }

    /**
     * Sets the value of the postalcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOSTALCODE(String value) {
        this.postalcode = value;
    }

    /**
     * Gets the value of the coordstatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOORDSTATUS() {
        return coordstatus;
    }

    /**
     * Sets the value of the coordstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOORDSTATUS(String value) {
        this.coordstatus = value;
    }

    /**
     * Gets the value of the longitude property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLONGITUDE() {
        return longitude;
    }

    /**
     * Sets the value of the longitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLONGITUDE(String value) {
        this.longitude = value;
    }

    /**
     * Gets the value of the latitude property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLATITUDE() {
        return latitude;
    }

    /**
     * Sets the value of the latitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLATITUDE(String value) {
        this.latitude = value;
    }

    /**
     * Gets the value of the setpositioninroute property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSETPOSITIONINROUTE() {
        return setpositioninroute;
    }

    /**
     * Sets the value of the setpositioninroute property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSETPOSITIONINROUTE(String value) {
        this.setpositioninroute = value;
    }

    /**
     * Gets the value of the activityid property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getACTIVITYID() {
        return activityid;
    }

    /**
     * Sets the value of the activityid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setACTIVITYID(BigInteger value) {
        this.activityid = value;
    }

    /**
     * Gets the value of the position property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOSITION() {
        return position;
    }

    /**
     * Sets the value of the position property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOSITION(String value) {
        this.position = value;
    }

    /**
     * Gets the value of the recordtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRECORDTYPE() {
        return recordtype;
    }

    /**
     * Sets the value of the recordtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRECORDTYPE(String value) {
        this.recordtype = value;
    }

    /**
     * Gets the value of the customernumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTOMERNUMBER() {
        return customernumber;
    }

    /**
     * Sets the value of the customernumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTOMERNUMBER(String value) {
        this.customernumber = value;
    }

    /**
     * Gets the value of the xarut property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXARUT() {
        return xarut;
    }

    /**
     * Sets the value of the xarut property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXARUT(String value) {
        this.xarut = value;
    }

    /**
     * Gets the value of the customername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTOMERNAME() {
        return customername;
    }

    /**
     * Sets the value of the customername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTOMERNAME(String value) {
        this.customername = value;
    }

    /**
     * Gets the value of the xacustomertype property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getXACUSTOMERTYPE() {
        return xacustomertype;
    }

    /**
     * Sets the value of the xacustomertype property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setXACUSTOMERTYPE(BigInteger value) {
        this.xacustomertype = value;
    }

    /**
     * Gets the value of the xacustormercategory property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getXACUSTORMERCATEGORY() {
        return xacustormercategory;
    }

    /**
     * Sets the value of the xacustormercategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setXACUSTORMERCATEGORY(BigInteger value) {
        this.xacustormercategory = value;
    }

    /**
     * Gets the value of the customercell property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTOMERCELL() {
        return customercell;
    }

    /**
     * Sets the value of the customercell property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTOMERCELL(String value) {
        this.customercell = value;
    }

    /**
     * Gets the value of the customerphone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTOMERPHONE() {
        return customerphone;
    }

    /**
     * Sets the value of the customerphone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTOMERPHONE(String value) {
        this.customerphone = value;
    }

    /**
     * Gets the value of the xacontactphone2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXACONTACTPHONE2() {
        return xacontactphone2;
    }

    /**
     * Sets the value of the xacontactphone2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXACONTACTPHONE2(String value) {
        this.xacontactphone2 = value;
    }

    /**
     * Gets the value of the customeremail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTOMEREMAIL() {
        return customeremail;
    }

    /**
     * Sets the value of the customeremail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTOMEREMAIL(String value) {
        this.customeremail = value;
    }

    /**
     * Gets the value of the xaselltype property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getXASELLTYPE() {
        return xaselltype;
    }

    /**
     * Sets the value of the xaselltype property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setXASELLTYPE(BigInteger value) {
        this.xaselltype = value;
    }

    /**
     * Gets the value of the xaseller property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXASELLER() {
        return xaseller;
    }

    /**
     * Sets the value of the xaseller property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXASELLER(String value) {
        this.xaseller = value;
    }

    /**
     * Gets the value of the timezone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIMEZONE() {
        return timezone;
    }

    /**
     * Sets the value of the timezone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIMEZONE(String value) {
        this.timezone = value;
    }

    /**
     * Gets the value of the languaje property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLANGUAJE() {
        return languaje;
    }

    /**
     * Sets the value of the languaje property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLANGUAJE(String value) {
        this.languaje = value;
    }

    /**
     * Gets the value of the adittionaldata property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADITTIONALDATA() {
        return adittionaldata;
    }

    /**
     * Sets the value of the adittionaldata property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADITTIONALDATA(String value) {
        this.adittionaldata = value;
    }

    /**
     * Gets the value of the adittionalsubdata property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADITTIONALSUBDATA() {
        return adittionalsubdata;
    }

    /**
     * Sets the value of the adittionalsubdata property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADITTIONALSUBDATA(String value) {
        this.adittionalsubdata = value;
    }

}
