xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://agendadorejb/";
(:: import schema at "../XSD/ReadOutbound.xsd" ::)
declare namespace ns2="http://spring.io/guides/gs-producing-web-service";
(:: import schema at "../WSDL/ReadOutbound.wsdl" ::)

declare variable $ReadOutbound as element() (:: schema-element(ns1:ReadOutbound) ::) external;

declare function local:func($ReadOutbound as element() (:: schema-element(ns1:ReadOutbound) ::)) as element() (:: schema-element(ns2:send_message_request) ::) {
    <ns2:send_message_request>
        <ns2:user>
            <ns2:now></ns2:now>
            <ns2:company></ns2:company>
            <ns2:login></ns2:login>
            <ns2:auth_string></ns2:auth_string>
        </ns2:user>
        <ns2:messages>
            <ns2:message>
                <ns2:app_host>{fn:data($ReadOutbound/app_host)}</ns2:app_host>
                <ns2:app_port>{fn:data($ReadOutbound/app_port)}</ns2:app_port>
                <ns2:app_url>{fn:data($ReadOutbound/app_url)}</ns2:app_url>
                <ns2:message_id>{fn:data($ReadOutbound/message_id)}</ns2:message_id>
                {
                    if ($ReadOutbound/company_id)
                    then <ns2:company_id>{fn:data($ReadOutbound/company_id)}</ns2:company_id>
                    else ()
                }
                {
                    if ($ReadOutbound/address)
                    then <ns2:address>{fn:data($ReadOutbound/address)}</ns2:address>
                    else ()
                }
                {
                    if ($ReadOutbound/send_to)
                    then <ns2:send_to>{fn:data($ReadOutbound/send_to)}</ns2:send_to>
                    else ()
                }
                {
                    if ($ReadOutbound/subject)
                    then <ns2:subject>{fn:data($ReadOutbound/subject)}</ns2:subject>
                    else ()
                }
                {
                    if ($ReadOutbound/body)
                    then <ns2:body>{fn:data($ReadOutbound/body)}</ns2:body>
                    else ()
                }
            </ns2:message>
        </ns2:messages>
    </ns2:send_message_request>
};

local:func($ReadOutbound)