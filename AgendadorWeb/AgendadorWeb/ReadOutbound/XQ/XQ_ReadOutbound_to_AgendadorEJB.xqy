xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://agendadorejb/";
(:: import schema at "../../Util/AgendadorWebEJB/XSD/AgendadorWebEJB.xsd" ::)

declare variable $GuardarXML as element() (:: schema-element(ns1:GuardarXML) ::) external;

declare function local:func($GuardarXML as element() (:: schema-element(ns1:GuardarXML) ::),  $messageStringFormat as xs:string) as element() (:: schema-element(ns1:GuardarXML) ::) {
    <ns1:GuardarXML>
        <XML>{$messageStringFormat}</XML>
    </ns1:GuardarXML>
};

declare variable $messageStringFormat as xs:string external;
local:func($GuardarXML, $messageStringFormat)