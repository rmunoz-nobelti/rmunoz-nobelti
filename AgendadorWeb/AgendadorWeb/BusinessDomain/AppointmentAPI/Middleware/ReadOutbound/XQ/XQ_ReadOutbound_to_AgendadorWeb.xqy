xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://agendadorejb/";
(:: import schema at "../XSD/ReadOutbound.xsd" ::)
declare namespace ns2="http://spring.io/guides/gs-producing-web-service";
(:: import schema at "../WSDL/ReadOutbound.wsdl" ::)

declare variable $ReadOutbound as element() (:: schema-element(ns1:ReadOutbound) ::) external;

declare function local:func($ReadOutbound as element() (:: schema-element(ns1:ReadOutbound) ::), $body as xs:string, $app_host as xs:string,$app_port as xs:string,$app_url as xs:string,$message_id as xs:string,$company_id as xs:string,$address as xs:string,$send_to as xs:string,$subject as xs:string) as element() (:: schema-element(ns2:send_message_request) ::) {
    <ns2:send_message_request>
        <ns2:user>
            <ns2:now></ns2:now>
            <ns2:company></ns2:company>
            <ns2:login></ns2:login>
            <ns2:auth_string></ns2:auth_string>
        </ns2:user>
        <ns2:messages>
            <ns2:message>
                <ns2:app_host>{$app_host}</ns2:app_host>
                <ns2:app_port>{$app_port}</ns2:app_port>
                <ns2:app_url>{$app_url}</ns2:app_url>
                <ns2:message_id>{$message_id}</ns2:message_id>
                <ns2:company_id>{$company_id}</ns2:company_id>
                <ns2:address>{$address}</ns2:address>
                <ns2:send_to>{$send_to}</ns2:send_to>
                <ns2:subject>{$subject}</ns2:subject>
                <ns2:body>{$body}</ns2:body>
            </ns2:message>
        </ns2:messages>
    </ns2:send_message_request>
};

declare variable $body as xs:string external;
declare variable $app_host as xs:string external;
declare variable $app_port as xs:string external;
declare variable $app_url as xs:string external;
declare variable $message_id as xs:string external;
declare variable $company_id as xs:string external;
declare variable $address as xs:string external;
declare variable $send_to as xs:string external;
declare variable $subject as xs:string external;
local:func($ReadOutbound,$body,$app_host,$app_port,$app_url,$message_id,$company_id,$address,$send_to,$subject)