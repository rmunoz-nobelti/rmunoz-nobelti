xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/AWEB_PROC_FILTRO_PRINCIPAL";
(:: import schema at "../XSD/filtroPrincipalRequest.xsd" ::)

declare variable $filtroPrincipalRequest as element() (:: schema-element(ns1:InputParameters) ::) external;

declare function local:func($filtroPrincipalRequest as element() (:: schema-element(ns1:InputParameters) ::)) as element() (:: schema-element(ns1:InputParameters) ::) {
    <ns1:InputParameters>
        {
            if ($filtroPrincipalRequest/ns1:P_ESTADO)
            then <ns1:P_ESTADO>{fn:data($filtroPrincipalRequest/ns1:P_ESTADO)}</ns1:P_ESTADO>
            else ()
        }
        {
            if ($filtroPrincipalRequest/ns1:P_RUT)
            then <ns1:P_RUT>{fn:data($filtroPrincipalRequest/ns1:P_RUT)}</ns1:P_RUT>
            else ()
        }
        {
            if ($filtroPrincipalRequest/ns1:P_ID_ORDEN)
            then <ns1:P_ID_ORDEN>{fn:data($filtroPrincipalRequest/ns1:P_ID_ORDEN)}</ns1:P_ID_ORDEN>
            else ()
        }
        {
            if ($filtroPrincipalRequest/ns1:P_ACTIVIDAD)
            then <ns1:P_ACTIVIDAD>{fn:data($filtroPrincipalRequest/ns1:P_ACTIVIDAD)}</ns1:P_ACTIVIDAD>
            else ()
        }
        {
            if ($filtroPrincipalRequest/ns1:P_REGION)
            then <ns1:P_REGION>{fn:data($filtroPrincipalRequest/ns1:P_REGION)}</ns1:P_REGION>
            else ()
        }
        {
            if ($filtroPrincipalRequest/ns1:P_FECHA_INI)
            then <ns1:P_FECHA_INI>{fn:data($filtroPrincipalRequest/ns1:P_FECHA_INI)}</ns1:P_FECHA_INI>
            else ()
        }
        {
            if ($filtroPrincipalRequest/ns1:P_FECHA_FIN)
            then <ns1:P_FECHA_FIN>{fn:data($filtroPrincipalRequest/ns1:P_FECHA_FIN)}</ns1:P_FECHA_FIN>
            else ()
        }
    </ns1:InputParameters>
};

local:func($filtroPrincipalRequest)
