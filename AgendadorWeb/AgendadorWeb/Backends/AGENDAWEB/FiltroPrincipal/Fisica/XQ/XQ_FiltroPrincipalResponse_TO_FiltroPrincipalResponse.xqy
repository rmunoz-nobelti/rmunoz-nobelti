xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/AWEB_PROC_FILTRO_PRINCIPAL";
(:: import schema at "../XSD/filtroPrincipalResponse.xsd" ::)

declare variable $filtroPrincipalResponse as element() (:: schema-element(ns1:OutputParameters) ::) external;

declare function local:func($filtroPrincipalResponse as element() (:: schema-element(ns1:OutputParameters) ::)) as element() (:: schema-element(ns1:OutputParameters) ::) {
    <ns1:OutputParameters>
        {
            if ($filtroPrincipalResponse/ns1:P_CURSOR)
            then 
                <ns1:P_CURSOR>
                    {
                        for $P_CURSOR_Row in $filtroPrincipalResponse/ns1:P_CURSOR/ns1:P_CURSOR_Row
                        return 
                        <ns1:P_CURSOR_Row>
                            {
                                if ($P_CURSOR_Row/ns1:APPTNUMBER)
                                then <ns1:APPTNUMBER>{fn:data($P_CURSOR_Row/ns1:APPTNUMBER)}</ns1:APPTNUMBER>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:ID_CRM)
                                then <ns1:ID_CRM>{fn:data($P_CURSOR_Row/ns1:ID_CRM)}</ns1:ID_CRM>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:ACTIVITYTYPE)
                                then <ns1:ACTIVITYTYPE>{fn:data($P_CURSOR_Row/ns1:ACTIVITYTYPE)}</ns1:ACTIVITYTYPE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_TECHNOLOGY)
                                then <ns1:XA_TECHNOLOGY>{fn:data($P_CURSOR_Row/ns1:XA_TECHNOLOGY)}</ns1:XA_TECHNOLOGY>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_RETRIES)
                                then <ns1:XA_RETRIES>{fn:data($P_CURSOR_Row/ns1:XA_RETRIES)}</ns1:XA_RETRIES>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_MOVE)
                                then <ns1:XA_MOVE>{fn:data($P_CURSOR_Row/ns1:XA_MOVE)}</ns1:XA_MOVE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_URGENT)
                                then <ns1:XA_URGENT>{fn:data($P_CURSOR_Row/ns1:XA_URGENT)}</ns1:XA_URGENT>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_COMPLAIN_TYPE)
                                then <ns1:XA_COMPLAIN_TYPE>{fn:data($P_CURSOR_Row/ns1:XA_COMPLAIN_TYPE)}</ns1:XA_COMPLAIN_TYPE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:SCHEDULEWEB)
                                then <ns1:SCHEDULEWEB>{fn:data($P_CURSOR_Row/ns1:SCHEDULEWEB)}</ns1:SCHEDULEWEB>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:FECHA_CC)
                                then <ns1:FECHA_CC>{fn:data($P_CURSOR_Row/ns1:FECHA_CC)}</ns1:FECHA_CC>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:TIMESLOT)
                                then <ns1:TIMESLOT>{fn:data($P_CURSOR_Row/ns1:TIMESLOT)}</ns1:TIMESLOT>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:TRAVELTIME)
                                then <ns1:TRAVELTIME>{fn:data($P_CURSOR_Row/ns1:TRAVELTIME)}</ns1:TRAVELTIME>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:COUNTRY_CODETUS)
                                then <ns1:COUNTRY_CODETUS>{fn:data($P_CURSOR_Row/ns1:COUNTRY_CODETUS)}</ns1:COUNTRY_CODETUS>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_REGION)
                                then <ns1:XA_REGION>{fn:data($P_CURSOR_Row/ns1:XA_REGION)}</ns1:XA_REGION>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_PROVINCE)
                                then <ns1:XA_PROVINCE>{fn:data($P_CURSOR_Row/ns1:XA_PROVINCE)}</ns1:XA_PROVINCE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:STATEPROVINCE)
                                then <ns1:STATEPROVINCE>{fn:data($P_CURSOR_Row/ns1:STATEPROVINCE)}</ns1:STATEPROVINCE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_COMUNA)
                                then <ns1:XA_COMUNA>{fn:data($P_CURSOR_Row/ns1:XA_COMUNA)}</ns1:XA_COMUNA>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:CITY)
                                then <ns1:CITY>{fn:data($P_CURSOR_Row/ns1:CITY)}</ns1:CITY>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_NODE)
                                then <ns1:XA_NODE>{fn:data($P_CURSOR_Row/ns1:XA_NODE)}</ns1:XA_NODE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:ID_ESTADO)
                                then <ns1:ID_ESTADO>{fn:data($P_CURSOR_Row/ns1:ID_ESTADO)}</ns1:ID_ESTADO>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_INSTALL_DETAILS)
                                then <ns1:XA_INSTALL_DETAILS>{fn:data($P_CURSOR_Row/ns1:XA_INSTALL_DETAILS)}</ns1:XA_INSTALL_DETAILS>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:CODIGO)
                                then <ns1:CODIGO>{fn:data($P_CURSOR_Row/ns1:CODIGO)}</ns1:CODIGO>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:PRODUCTO)
                                then <ns1:PRODUCTO>{fn:data($P_CURSOR_Row/ns1:PRODUCTO)}</ns1:PRODUCTO>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:DESCRIPCION)
                                then <ns1:DESCRIPCION>{fn:data($P_CURSOR_Row/ns1:DESCRIPCION)}</ns1:DESCRIPCION>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_NR_POINTS_TV)
                                then <ns1:XA_NR_POINTS_TV>{fn:data($P_CURSOR_Row/ns1:XA_NR_POINTS_TV)}</ns1:XA_NR_POINTS_TV>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_NR_POINT_INTERNET)
                                then <ns1:XA_NR_POINT_INTERNET>{fn:data($P_CURSOR_Row/ns1:XA_NR_POINT_INTERNET)}</ns1:XA_NR_POINT_INTERNET>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_NR_POINT_PHONE)
                                then <ns1:XA_NR_POINT_PHONE>{fn:data($P_CURSOR_Row/ns1:XA_NR_POINT_PHONE)}</ns1:XA_NR_POINT_PHONE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_STREET_TYPE)
                                then <ns1:XA_STREET_TYPE>{fn:data($P_CURSOR_Row/ns1:XA_STREET_TYPE)}</ns1:XA_STREET_TYPE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_STREET_NR)
                                then <ns1:XA_STREET_NR>{fn:data($P_CURSOR_Row/ns1:XA_STREET_NR)}</ns1:XA_STREET_NR>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:STREETADDRESS)
                                then <ns1:STREETADDRESS>{fn:data($P_CURSOR_Row/ns1:STREETADDRESS)}</ns1:STREETADDRESS>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_ADDRESS_COMPLEMENT)
                                then <ns1:XA_ADDRESS_COMPLEMENT>{fn:data($P_CURSOR_Row/ns1:XA_ADDRESS_COMPLEMENT)}</ns1:XA_ADDRESS_COMPLEMENT>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_ADDRESS_COMPLEMENT_2)
                                then <ns1:XA_ADDRESS_COMPLEMENT_2>{fn:data($P_CURSOR_Row/ns1:XA_ADDRESS_COMPLEMENT_2)}</ns1:XA_ADDRESS_COMPLEMENT_2>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_REF_SEC)
                                then <ns1:XA_REF_SEC>{fn:data($P_CURSOR_Row/ns1:XA_REF_SEC)}</ns1:XA_REF_SEC>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_OBSERVATIONS)
                                then <ns1:XA_OBSERVATIONS>{fn:data($P_CURSOR_Row/ns1:XA_OBSERVATIONS)}</ns1:XA_OBSERVATIONS>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:POSTALCODE)
                                then <ns1:POSTALCODE>{fn:data($P_CURSOR_Row/ns1:POSTALCODE)}</ns1:POSTALCODE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:COORDSTATUS)
                                then <ns1:COORDSTATUS>{fn:data($P_CURSOR_Row/ns1:COORDSTATUS)}</ns1:COORDSTATUS>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:LONGITUDE)
                                then <ns1:LONGITUDE>{fn:data($P_CURSOR_Row/ns1:LONGITUDE)}</ns1:LONGITUDE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:LATITUDE)
                                then <ns1:LATITUDE>{fn:data($P_CURSOR_Row/ns1:LATITUDE)}</ns1:LATITUDE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:SETPOSITIONINROUTE)
                                then <ns1:SETPOSITIONINROUTE>{fn:data($P_CURSOR_Row/ns1:SETPOSITIONINROUTE)}</ns1:SETPOSITIONINROUTE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:ACTIVITYID)
                                then <ns1:ACTIVITYID>{fn:data($P_CURSOR_Row/ns1:ACTIVITYID)}</ns1:ACTIVITYID>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:POSITION)
                                then <ns1:POSITION>{fn:data($P_CURSOR_Row/ns1:POSITION)}</ns1:POSITION>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:RECORDTYPE)
                                then <ns1:RECORDTYPE>{fn:data($P_CURSOR_Row/ns1:RECORDTYPE)}</ns1:RECORDTYPE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:ID_CUSTOMERNUMBER)
                                then <ns1:ID_CUSTOMERNUMBER>{fn:data($P_CURSOR_Row/ns1:ID_CUSTOMERNUMBER)}</ns1:ID_CUSTOMERNUMBER>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_RUT)
                                then <ns1:XA_RUT>{fn:data($P_CURSOR_Row/ns1:XA_RUT)}</ns1:XA_RUT>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:CUSTOMERCELL)
                                then <ns1:CUSTOMERCELL>{fn:data($P_CURSOR_Row/ns1:CUSTOMERCELL)}</ns1:CUSTOMERCELL>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:CUSTOMERPHONE)
                                then <ns1:CUSTOMERPHONE>{fn:data($P_CURSOR_Row/ns1:CUSTOMERPHONE)}</ns1:CUSTOMERPHONE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:CUSTOMEREMAIL)
                                then <ns1:CUSTOMEREMAIL>{fn:data($P_CURSOR_Row/ns1:CUSTOMEREMAIL)}</ns1:CUSTOMEREMAIL>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_SELL_TYPE)
                                then <ns1:XA_SELL_TYPE>{fn:data($P_CURSOR_Row/ns1:XA_SELL_TYPE)}</ns1:XA_SELL_TYPE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_SELLER)
                                then <ns1:XA_SELLER>{fn:data($P_CURSOR_Row/ns1:XA_SELLER)}</ns1:XA_SELLER>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:TIMEZONE)
                                then <ns1:TIMEZONE>{fn:data($P_CURSOR_Row/ns1:TIMEZONE)}</ns1:TIMEZONE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:LANGUAJE)
                                then <ns1:LANGUAJE>{fn:data($P_CURSOR_Row/ns1:LANGUAJE)}</ns1:LANGUAJE>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:FILTRO_V)
                                then <ns1:FILTRO_V>{fn:data($P_CURSOR_Row/ns1:FILTRO_V)}</ns1:FILTRO_V>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:ID_CONTACTO)
                                then <ns1:ID_CONTACTO>{fn:data($P_CURSOR_Row/ns1:ID_CONTACTO)}</ns1:ID_CONTACTO>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:PROGRAMADOR)
                                then <ns1:PROGRAMADOR>{fn:data($P_CURSOR_Row/ns1:PROGRAMADOR)}</ns1:PROGRAMADOR>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:AGENDA)
                                then <ns1:AGENDA>{fn:data($P_CURSOR_Row/ns1:AGENDA)}</ns1:AGENDA>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:ALIADO)
                                then <ns1:ALIADO>{fn:data($P_CURSOR_Row/ns1:ALIADO)}</ns1:ALIADO>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:DESASIGNADO)
                                then <ns1:DESASIGNADO>{fn:data($P_CURSOR_Row/ns1:DESASIGNADO)}</ns1:DESASIGNADO>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_COMPLETE_REASON)
                                then <ns1:XA_COMPLETE_REASON>{fn:data($P_CURSOR_Row/ns1:XA_COMPLETE_REASON)}</ns1:XA_COMPLETE_REASON>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:TECNICO)
                                then <ns1:TECNICO>{fn:data($P_CURSOR_Row/ns1:TECNICO)}</ns1:TECNICO>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:CREATE_AT)
                                then <ns1:CREATE_AT>{fn:data($P_CURSOR_Row/ns1:CREATE_AT)}</ns1:CREATE_AT>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:UPDATE_AT)
                                then <ns1:UPDATE_AT>{fn:data($P_CURSOR_Row/ns1:UPDATE_AT)}</ns1:UPDATE_AT>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:RESOURCEID)
                                then <ns1:RESOURCEID>{fn:data($P_CURSOR_Row/ns1:RESOURCEID)}</ns1:RESOURCEID>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:XA_CONTACT_PHONE_2)
                                then <ns1:XA_CONTACT_PHONE_2>{fn:data($P_CURSOR_Row/ns1:XA_CONTACT_PHONE_2)}</ns1:XA_CONTACT_PHONE_2>
                                else ()
                            }
                            {
                                if ($P_CURSOR_Row/ns1:WORKZONE)
                                then <ns1:WORKZONE>{fn:data($P_CURSOR_Row/ns1:WORKZONE)}</ns1:WORKZONE>
                                else ()
                            }
                        </ns1:P_CURSOR_Row>
                    }
                </ns1:P_CURSOR>
            else ()
        }
        {
            if ($filtroPrincipalResponse/ns1:ESTADO)
            then <ns1:ESTADO>{fn:data($filtroPrincipalResponse/ns1:ESTADO)}</ns1:ESTADO>
            else ()
        }
        {
            if ($filtroPrincipalResponse/ns1:DESCRIPCION)
            then <ns1:DESCRIPCION>{fn:data($filtroPrincipalResponse/ns1:DESCRIPCION)}</ns1:DESCRIPCION>
            else ()
        }
    </ns1:OutputParameters>
};

local:func($filtroPrincipalResponse)
