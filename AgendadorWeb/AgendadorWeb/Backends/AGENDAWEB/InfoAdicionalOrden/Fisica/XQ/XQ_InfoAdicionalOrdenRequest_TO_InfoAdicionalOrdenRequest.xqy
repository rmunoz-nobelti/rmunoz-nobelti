xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/dbReference";
(:: import schema at "../XSD/infoAdicionalOrdenRequest.xsd" ::)

declare variable $infoAdicionalOrdenRequest as element() (:: schema-element(ns1:InputParameters) ::) external;

declare function local:func($infoAdicionalOrdenRequest as element() (:: schema-element(ns1:InputParameters) ::)) as element() (:: schema-element(ns1:InputParameters) ::) {
    <ns1:InputParameters>
        {
            if ($infoAdicionalOrdenRequest/ns1:P_ID_ORDER)
            then <ns1:P_ID_ORDER>{fn:data($infoAdicionalOrdenRequest/ns1:P_ID_ORDER)}</ns1:P_ID_ORDER>
            else ()
        }
    </ns1:InputParameters>
};

local:func($infoAdicionalOrdenRequest)