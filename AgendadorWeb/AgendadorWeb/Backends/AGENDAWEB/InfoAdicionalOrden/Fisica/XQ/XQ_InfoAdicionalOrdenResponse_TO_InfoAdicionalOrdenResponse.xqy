xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/dbReference";
(:: import schema at "../XSD/infoAdicionalOrdenResponse.xsd" ::)

declare variable $infoAdicionalOrdenResponse as element() (:: schema-element(ns1:OutputParameters) ::) external;

declare function local:func($infoAdicionalOrdenResponse as element() (:: schema-element(ns1:OutputParameters) ::)) as element() (:: schema-element(ns1:OutputParameters) ::) {
    <ns1:OutputParameters>
        {
            if ($infoAdicionalOrdenResponse/ns1:P_ADITONALDATA)
            then <ns1:P_ADITONALDATA>Completado</ns1:P_ADITONALDATA>
            else ()
        }
        {
            if ($infoAdicionalOrdenResponse/ns1:ESTADO)
            then <ns1:ESTADO>1</ns1:ESTADO>
            else ()
        }
        {
            if ($infoAdicionalOrdenResponse/ns1:DESCRIPCION)
            then <ns1:DESCRIPCION>Descripcion</ns1:DESCRIPCION>
            else ()
        }           
    </ns1:OutputParameters>
};

local:func($infoAdicionalOrdenResponse)