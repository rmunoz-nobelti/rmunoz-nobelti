xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/AWEB_PROC_CONSULTAR_ORDEN";
(:: import schema at "../XSD/consultarOrdenResponse.xsd" ::)

declare variable $consultarOrdenResponse as element() (:: schema-element(ns1:OutputParameters) ::) external;

declare function local:func($consultarOrdenResponse as element() (:: schema-element(ns1:OutputParameters) ::)) as element() (:: schema-element(ns1:OutputParameters) ::) {
    <ns1:OutputParameters>
        {
            if ($consultarOrdenResponse/ns1:P_APPTNUMBER)
            then <ns1:P_APPTNUMBER>{fn:data($consultarOrdenResponse/ns1:P_APPTNUMBER)}</ns1:P_APPTNUMBER>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_ID_CRM)
            then <ns1:P_ID_CRM>{fn:data($consultarOrdenResponse/ns1:P_ID_CRM)}</ns1:P_ID_CRM>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_ACTIVITYTYPE)
            then <ns1:P_ACTIVITYTYPE>{fn:data($consultarOrdenResponse/ns1:P_ACTIVITYTYPE)}</ns1:P_ACTIVITYTYPE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_TECHNOLOGY)
            then <ns1:P_XA_TECHNOLOGY>{fn:data($consultarOrdenResponse/ns1:P_XA_TECHNOLOGY)}</ns1:P_XA_TECHNOLOGY>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_RETRIES)
            then <ns1:P_XA_RETRIES>{fn:data($consultarOrdenResponse/ns1:P_XA_RETRIES)}</ns1:P_XA_RETRIES>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_MOVE)
            then <ns1:P_XA_MOVE>{fn:data($consultarOrdenResponse/ns1:P_XA_MOVE)}</ns1:P_XA_MOVE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_URGENT)
            then <ns1:P_XA_URGENT>{fn:data($consultarOrdenResponse/ns1:P_XA_URGENT)}</ns1:P_XA_URGENT>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_COMPLAIN_TYPE)
            then <ns1:P_XA_COMPLAIN_TYPE>{fn:data($consultarOrdenResponse/ns1:P_XA_COMPLAIN_TYPE)}</ns1:P_XA_COMPLAIN_TYPE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_SCHEDULEWEB)
            then <ns1:P_SCHEDULEWEB>{fn:data($consultarOrdenResponse/ns1:P_SCHEDULEWEB)}</ns1:P_SCHEDULEWEB>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_FECHA_CC)
            then <ns1:P_FECHA_CC>{fn:data($consultarOrdenResponse/ns1:P_FECHA_CC)}</ns1:P_FECHA_CC>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_TIMESLOT)
            then <ns1:P_TIMESLOT>{fn:data($consultarOrdenResponse/ns1:P_TIMESLOT)}</ns1:P_TIMESLOT>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_TRAVELTIME)
            then <ns1:P_TRAVELTIME>{fn:data($consultarOrdenResponse/ns1:P_TRAVELTIME)}</ns1:P_TRAVELTIME>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_COUNTRY_CODETUS)
            then <ns1:P_COUNTRY_CODETUS>{fn:data($consultarOrdenResponse/ns1:P_COUNTRY_CODETUS)}</ns1:P_COUNTRY_CODETUS>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_REGION)
            then <ns1:P_XA_REGION>{fn:data($consultarOrdenResponse/ns1:P_XA_REGION)}</ns1:P_XA_REGION>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_PROVINCE)
            then <ns1:P_XA_PROVINCE>{fn:data($consultarOrdenResponse/ns1:P_XA_PROVINCE)}</ns1:P_XA_PROVINCE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_STATEPROVINCE)
            then <ns1:P_STATEPROVINCE>{fn:data($consultarOrdenResponse/ns1:P_STATEPROVINCE)}</ns1:P_STATEPROVINCE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_COMUNA)
            then <ns1:P_XA_COMUNA>{fn:data($consultarOrdenResponse/ns1:P_XA_COMUNA)}</ns1:P_XA_COMUNA>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_CITY)
            then <ns1:P_CITY>{fn:data($consultarOrdenResponse/ns1:P_CITY)}</ns1:P_CITY>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_NODE)
            then <ns1:P_XA_NODE>{fn:data($consultarOrdenResponse/ns1:P_XA_NODE)}</ns1:P_XA_NODE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_ID_ESTADO)
            then <ns1:P_ID_ESTADO>{fn:data($consultarOrdenResponse/ns1:P_ID_ESTADO)}</ns1:P_ID_ESTADO>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_INSTALL_DETAILS)
            then <ns1:P_XA_INSTALL_DETAILS>{fn:data($consultarOrdenResponse/ns1:P_XA_INSTALL_DETAILS)}</ns1:P_XA_INSTALL_DETAILS>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_CODIGO)
            then <ns1:P_CODIGO>{fn:data($consultarOrdenResponse/ns1:P_CODIGO)}</ns1:P_CODIGO>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_PRODUCTO)
            then <ns1:P_PRODUCTO>{fn:data($consultarOrdenResponse/ns1:P_PRODUCTO)}</ns1:P_PRODUCTO>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_DESCRIPCION)
            then <ns1:P_DESCRIPCION>{fn:data($consultarOrdenResponse/ns1:P_DESCRIPCION)}</ns1:P_DESCRIPCION>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_NR_POINTS_TV)
            then <ns1:P_XA_NR_POINTS_TV>{fn:data($consultarOrdenResponse/ns1:P_XA_NR_POINTS_TV)}</ns1:P_XA_NR_POINTS_TV>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_NR_POINT_INTERNET)
            then <ns1:P_XA_NR_POINT_INTERNET>{fn:data($consultarOrdenResponse/ns1:P_XA_NR_POINT_INTERNET)}</ns1:P_XA_NR_POINT_INTERNET>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_NR_POINT_PHONE)
            then <ns1:P_XA_NR_POINT_PHONE>{fn:data($consultarOrdenResponse/ns1:P_XA_NR_POINT_PHONE)}</ns1:P_XA_NR_POINT_PHONE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_STREET_TYPE)
            then <ns1:P_XA_STREET_TYPE>{fn:data($consultarOrdenResponse/ns1:P_XA_STREET_TYPE)}</ns1:P_XA_STREET_TYPE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_STREET_NR)
            then <ns1:P_XA_STREET_NR>{fn:data($consultarOrdenResponse/ns1:P_XA_STREET_NR)}</ns1:P_XA_STREET_NR>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_STREETADDRESS)
            then <ns1:P_STREETADDRESS>{fn:data($consultarOrdenResponse/ns1:P_STREETADDRESS)}</ns1:P_STREETADDRESS>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_ADDRESS_COMPLEMENT)
            then <ns1:P_XA_ADDRESS_COMPLEMENT>{fn:data($consultarOrdenResponse/ns1:P_XA_ADDRESS_COMPLEMENT)}</ns1:P_XA_ADDRESS_COMPLEMENT>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_ADDRESS_COMPLEMENT_2)
            then <ns1:P_XA_ADDRESS_COMPLEMENT_2>{fn:data($consultarOrdenResponse/ns1:P_XA_ADDRESS_COMPLEMENT_2)}</ns1:P_XA_ADDRESS_COMPLEMENT_2>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_REF_SEC)
            then <ns1:P_XA_REF_SEC>{fn:data($consultarOrdenResponse/ns1:P_XA_REF_SEC)}</ns1:P_XA_REF_SEC>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_OBSERVATIONS)
            then <ns1:P_XA_OBSERVATIONS>{fn:data($consultarOrdenResponse/ns1:P_XA_OBSERVATIONS)}</ns1:P_XA_OBSERVATIONS>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_POSTALCODE)
            then <ns1:P_POSTALCODE>{fn:data($consultarOrdenResponse/ns1:P_POSTALCODE)}</ns1:P_POSTALCODE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_COORDSTATUS)
            then <ns1:P_COORDSTATUS>{fn:data($consultarOrdenResponse/ns1:P_COORDSTATUS)}</ns1:P_COORDSTATUS>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_LONGITUDE)
            then <ns1:P_LONGITUDE>{fn:data($consultarOrdenResponse/ns1:P_LONGITUDE)}</ns1:P_LONGITUDE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_LATITUDE)
            then <ns1:P_LATITUDE>{fn:data($consultarOrdenResponse/ns1:P_LATITUDE)}</ns1:P_LATITUDE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_SETPOSITIONINROUTE)
            then <ns1:P_SETPOSITIONINROUTE>{fn:data($consultarOrdenResponse/ns1:P_SETPOSITIONINROUTE)}</ns1:P_SETPOSITIONINROUTE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_ACTIVITYID)
            then <ns1:P_ACTIVITYID>{fn:data($consultarOrdenResponse/ns1:P_ACTIVITYID)}</ns1:P_ACTIVITYID>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_POSITION)
            then <ns1:P_POSITION>{fn:data($consultarOrdenResponse/ns1:P_POSITION)}</ns1:P_POSITION>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_RECORDTYPE)
            then <ns1:P_RECORDTYPE>{fn:data($consultarOrdenResponse/ns1:P_RECORDTYPE)}</ns1:P_RECORDTYPE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_ID_CUSTOMERNUMBER)
            then <ns1:P_ID_CUSTOMERNUMBER>{fn:data($consultarOrdenResponse/ns1:P_ID_CUSTOMERNUMBER)}</ns1:P_ID_CUSTOMERNUMBER>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_RUT)
            then <ns1:P_XA_RUT>{fn:data($consultarOrdenResponse/ns1:P_XA_RUT)}</ns1:P_XA_RUT>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_CUSTOMERCELL)
            then <ns1:P_CUSTOMERCELL>{fn:data($consultarOrdenResponse/ns1:P_CUSTOMERCELL)}</ns1:P_CUSTOMERCELL>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_CUSTOMERPHONE)
            then <ns1:P_CUSTOMERPHONE>{fn:data($consultarOrdenResponse/ns1:P_CUSTOMERPHONE)}</ns1:P_CUSTOMERPHONE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_CUSTOMEREMAIL)
            then <ns1:P_CUSTOMEREMAIL>{fn:data($consultarOrdenResponse/ns1:P_CUSTOMEREMAIL)}</ns1:P_CUSTOMEREMAIL>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_SELL_TYPE)
            then <ns1:P_XA_SELL_TYPE>{fn:data($consultarOrdenResponse/ns1:P_XA_SELL_TYPE)}</ns1:P_XA_SELL_TYPE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_SELLER)
            then <ns1:P_XA_SELLER>{fn:data($consultarOrdenResponse/ns1:P_XA_SELLER)}</ns1:P_XA_SELLER>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_TIMEZONE)
            then <ns1:P_TIMEZONE>{fn:data($consultarOrdenResponse/ns1:P_TIMEZONE)}</ns1:P_TIMEZONE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_LANGUAJE)
            then <ns1:P_LANGUAJE>{fn:data($consultarOrdenResponse/ns1:P_LANGUAJE)}</ns1:P_LANGUAJE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_FILTRO_V)
            then <ns1:P_FILTRO_V>{fn:data($consultarOrdenResponse/ns1:P_FILTRO_V)}</ns1:P_FILTRO_V>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_ID_CONTACTO)
            then <ns1:P_ID_CONTACTO>{fn:data($consultarOrdenResponse/ns1:P_ID_CONTACTO)}</ns1:P_ID_CONTACTO>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_PROGRAMADOR)
            then <ns1:P_PROGRAMADOR>{fn:data($consultarOrdenResponse/ns1:P_PROGRAMADOR)}</ns1:P_PROGRAMADOR>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_AGENDA)
            then <ns1:P_AGENDA>{fn:data($consultarOrdenResponse/ns1:P_AGENDA)}</ns1:P_AGENDA>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_ALIADO)
            then <ns1:P_ALIADO>{fn:data($consultarOrdenResponse/ns1:P_ALIADO)}</ns1:P_ALIADO>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_DESASIGNADO)
            then <ns1:P_DESASIGNADO>{fn:data($consultarOrdenResponse/ns1:P_DESASIGNADO)}</ns1:P_DESASIGNADO>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_COMPLETE_REASON)
            then <ns1:P_XA_COMPLETE_REASON>{fn:data($consultarOrdenResponse/ns1:P_XA_COMPLETE_REASON)}</ns1:P_XA_COMPLETE_REASON>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_TECNICO)
            then <ns1:P_TECNICO>{fn:data($consultarOrdenResponse/ns1:P_TECNICO)}</ns1:P_TECNICO>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_CREATE_AT)
            then <ns1:P_CREATE_AT>{fn:data($consultarOrdenResponse/ns1:P_CREATE_AT)}</ns1:P_CREATE_AT>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_UPDATE_AT)
            then <ns1:P_UPDATE_AT>{fn:data($consultarOrdenResponse/ns1:P_UPDATE_AT)}</ns1:P_UPDATE_AT>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_RESOURCEID)
            then <ns1:P_RESOURCEID>{fn:data($consultarOrdenResponse/ns1:P_RESOURCEID)}</ns1:P_RESOURCEID>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_XA_CONTACT_PHONE_2)
            then <ns1:P_XA_CONTACT_PHONE_2>{fn:data($consultarOrdenResponse/ns1:P_XA_CONTACT_PHONE_2)}</ns1:P_XA_CONTACT_PHONE_2>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_WORKZONE)
            then <ns1:P_WORKZONE>{fn:data($consultarOrdenResponse/ns1:P_WORKZONE)}</ns1:P_WORKZONE>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_ACTIVO)
            then <ns1:P_ACTIVO>{fn:data($consultarOrdenResponse/ns1:P_ACTIVO)}</ns1:P_ACTIVO>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_TIPO_ESTADO)
            then <ns1:P_TIPO_ESTADO>{fn:data($consultarOrdenResponse/ns1:P_TIPO_ESTADO)}</ns1:P_TIPO_ESTADO>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:ESTADO)
            then <ns1:ESTADO>{fn:data($consultarOrdenResponse/ns1:ESTADO)}</ns1:ESTADO>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:DESCRIPCION)
            then <ns1:DESCRIPCION>{fn:data($consultarOrdenResponse/ns1:DESCRIPCION)}</ns1:DESCRIPCION>
            else ()
        }
        {
            if ($consultarOrdenResponse/ns1:P_LANGUAJE_XXX)
            then <ns1:P_LANGUAJE_XXX>{fn:data($consultarOrdenResponse/ns1:P_LANGUAJE_XXX)}</ns1:P_LANGUAJE_XXX>
            else ()
        }
    </ns1:OutputParameters>
};

local:func($consultarOrdenResponse)
