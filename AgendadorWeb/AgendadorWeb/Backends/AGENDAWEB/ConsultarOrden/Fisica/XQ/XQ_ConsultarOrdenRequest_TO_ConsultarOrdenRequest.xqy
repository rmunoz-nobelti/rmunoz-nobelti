xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/AWEB_PROC_CONSULTAR_ORDEN";
(:: import schema at "../XSD/consultarOrdenRequest.xsd" ::)

declare variable $consultarOrdenRequest as element() (:: schema-element(ns1:InputParameters) ::) external;

declare function local:func($consultarOrdenRequest as element() (:: schema-element(ns1:InputParameters) ::)) as element() (:: schema-element(ns1:InputParameters) ::) {
    <ns1:InputParameters>
        {
            if ($consultarOrdenRequest/ns1:P_ID_ORDER)
            then <ns1:P_ID_ORDER>{fn:data($consultarOrdenRequest/ns1:P_ID_ORDER)}</ns1:P_ID_ORDER>
            else ()
        }
    </ns1:InputParameters>
};

local:func($consultarOrdenRequest)
