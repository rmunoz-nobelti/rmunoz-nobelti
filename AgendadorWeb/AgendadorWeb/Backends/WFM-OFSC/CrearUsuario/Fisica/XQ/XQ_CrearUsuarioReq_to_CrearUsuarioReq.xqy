xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.claro.cl/OFSC/Inbound/CrearUsuarioReq";
(:: import schema at "../XSD/CrearUsuarioReq.xsd" ::)

declare variable $CrearUsuarioReq as element() (:: schema-element(ns1:CrearUsuarioReq) ::) external;

declare function local:func($CrearUsuarioReq as element() (:: schema-element(ns1:CrearUsuarioReq) ::)) as element() (:: schema-element(ns1:CrearUsuarioReq) ::) {
    <ns1:CrearUsuarioReq>
        <ns1:login>{fn:data($CrearUsuarioReq/ns1:login)}</ns1:login>
        <ns1:mainResourceId>{fn:data($CrearUsuarioReq/ns1:mainResourceId)}</ns1:mainResourceId>
        <ns1:name>smunoz</ns1:name>
        <ns1:language>{fn:data($CrearUsuarioReq/ns1:language)}</ns1:language>
        <ns1:timeZone>{fn:data($CrearUsuarioReq/ns1:timeZone)}</ns1:timeZone>
        <ns1:userType>{fn:data($CrearUsuarioReq/ns1:userType)}</ns1:userType>
        <ns1:password>{fn:data($CrearUsuarioReq/ns1:password)}</ns1:password>
        {
            if ($CrearUsuarioReq/ns1:resources)
            then <ns1:resources>{fn:data($CrearUsuarioReq/ns1:resources)}</ns1:resources>
            else ()
        }
    </ns1:CrearUsuarioReq>
};

local:func($CrearUsuarioReq)